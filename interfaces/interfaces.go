package interfaces

import (
	"net/http"
)

//HTTPClient is an http client interface
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}
