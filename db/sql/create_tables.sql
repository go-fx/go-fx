create table if not exists autochartist_signals (
	id int,
    signal_type varchar(20) not null,
    instrument varchar(20) not null,
    completed boolean not null,
    probability decimal(4,2) not null,
    signal_interval int not null,
    direction int not null,
    pattern varchar(50) not null,
    price_low decimal(18,6),
    price_high decimal(18,6),
    time_from datetime,
    time_to datetime,
    raw_data VARCHAR(1000) not null,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id)
);

create table if not exists signal_trades (
    trade_id varchar(50),
    signal_id int not null,
    strategy_name varchar(50) not null,
    instrument varchar(20) not null,
    units int not null,
    entry_price decimal(18,6) not null,
    entry_time datetime not null,
    deadline datetime,
    take_price decimal(18,6),
    stop_price decimal(18,6),
    ts_distance decimal(18,6),
    exit_price decimal(18,6),
    exit_time datetime,
    profit_loss decimal(18,4),
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (trade_id),
    foreign key (signal_id) references autochartist_signals(id)
);

create table if not exists signal_orders (
    order_id varchar(50),
    signal_id int not null,
    strategy_name varchar(50) not null,
    instrument varchar(20) not null,
    units int not null,
    price decimal(18,6) not null,
    deadline datetime,
    take_price decimal(18,6),
    stop_price decimal(18,6),
    ts_distance decimal(18,6),
    filled_at datetime,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (order_id),
    foreign key (signal_id) references autochartist_signals(id)
);

create table if not exists account_snapshots (
    id int auto_increment,
    window_open_at datetime unique,
    balance decimal(18,4),
    NAV decimal(18,4),
    unrealized_pl decimal(18,4),
    margin_used decimal(18,4),
    margin_available decimal(18,4),
    withdrawal_limit decimal(18,4),
    open_trade_count int,
    pending_order_count int,
    financing decimal(12,4),
    last_transaction_id varchar(50),
    account_id varchar(50),
    alias varchar(100),
    currency varchar(5),
    margin_rate decimal(3,2),
    created_by_user_id int,
    raw_data VARCHAR(2000) not null,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id)
);

create table if not exists reports (
    id int auto_increment,
    alias varchar(100),
    account_id varchar(50) not null,
    strategy_name varchar(50) not null,
    window_open_at datetime not null,
    window_close_at datetime not null,
    close_balance decimal(18,4) not null,
    unrealized_pl decimal(18,4) not null,
    pl decimal(18,4) not null,
    active_trades int not null,
    winning_trades int not null,
    losing_trades int not null,
    avg_profit_per_win decimal(18,4) not null,
    avg_loss_per_loss decimal(18,4) not null,
    strategy_config text not null,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id)
);

create table if not exists daily_reports (
    id int auto_increment,
    reportId int unique,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id),
    foreign key (reportId) references reports(id)
);

create table if not exists weekly_reports (
    id int auto_increment,
    reportId int unique,
    created_at datetime not null DEFAULT current_timestamp,
    updated_at datetime not null DEFAULT current_timestamp,
    deleted_at datetime,
    primary key (id),
    foreign key (reportId) references reports(id)
);
