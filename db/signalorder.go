package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gofx/loggers"
	"gofx/model"
	"gofx/model/instruments"
)

//CreateSignalOrderWithSignal creates a new signal order in the db along with its associated signal
func CreateSignalOrderWithSignal(order *model.SignalOrder, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//marshal full signal to JSON
	rawJSON, _ := json.Marshal(order.Signal)

	//optional prediction values
	var priceLow, priceHigh *float64
	var timeFrom, timeTo *time.Time
	if order.Signal.Data.Prediction != nil {
		priceLow = order.Signal.Data.Prediction.PriceLow
		priceHigh = order.Signal.Data.Prediction.PriceHigh
		timeFrom = order.Signal.Data.Prediction.TimeFrom
		timeTo = order.Signal.Data.Prediction.TimeTo
	}

	//insert signal
	_, err := db.Exec(`INSERT INTO autochartist_signals(id, signal_type, instrument, completed, probability, 
		signal_interval, direction, pattern, price_low, price_high, time_from, time_to, raw_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		order.Signal.ID, order.Signal.Type, order.Signal.Instrument.Name(), order.Signal.Meta.Completed, order.Signal.Meta.Probability,
		order.Signal.Meta.Interval, order.Signal.Meta.Direction, order.Signal.Meta.Pattern, priceLow, priceHigh, timeFrom, timeTo, string(rawJSON))
	if err != nil {
		return fmt.Errorf("could not create new signal >> %w", err)
	}

	//insert order
	_, err = db.Exec("INSERT INTO signal_orders(order_id, signal_id, strategy_name, instrument, units, price, deadline, take_price, stop_price, ts_distance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		order.OrderID, order.Signal.ID, order.StrategyName, order.Instrument.Name(), order.Units, order.Price, order.Deadline, order.TakePrice, order.StopPrice, order.TSDistance)
	if err != nil {
		return fmt.Errorf("could not create new order >> %w", err)
	}

	return nil
}

//CreateSignalOrder creates a new signal order in the db
func CreateSignalOrder(order *model.SignalOrder, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//insert order
	_, err := db.Exec("INSERT INTO signal_orders(order_id, signal_id, strategy_name, instrument, units, price, deadline, take_price, stop_price, ts_distance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		order.OrderID, order.Signal.ID, order.StrategyName, order.Instrument.Name(), order.Units, order.Price, order.Deadline, order.TakePrice, order.StopPrice, order.TSDistance)
	if err != nil {
		return fmt.Errorf("could not create new order >> %w", err)
	}

	return nil
}

//GetUnfilledStrategySignalOrders retrieves signal orders which are not yet associated with a trade
func GetUnfilledStrategySignalOrders(strategyName string, db *sql.DB) (map[string]*model.SignalOrder, error) {
	return getStrategySignalOrders(strategyName, true, db)
}

func getStrategySignalOrders(strategyName string, filterUnfilled bool, db *sql.DB) (map[string]*model.SignalOrder, error) {
	if db == nil {
		db = conn
	}

	var query string
	if filterUnfilled {
		query = `
			SELECT order_id, signal_id, strategy_name, orders.instrument, units, price, deadline, take_price, 
				stop_price, ts_distance, orders.created_at, orders.updated_at, raw_data
			FROM signal_orders AS orders
				JOIN autochartist_signals AS signals ON orders.signal_id = signals.id
			WHERE orders.deleted_at IS NULL AND orders.strategy_name = ? AND orders.filled_at IS NULL
			ORDER BY orders.order_id`
	} else {
		query = `
			SELECT order_id, signal_id, strategy_name, orders.instrument, units, price, deadline, take_price, 
				stop_price, ts_distance, orders.created_at, orders.updated_at, raw_data
			FROM signal_orders AS orders
				JOIN autochartist_signals AS signals ON orders.signal_id = signals.id
			WHERE orders.deleted_at IS NULL AND orders.strategy_name = ?
			ORDER BY orders.order_id`
	}

	rows, err := db.Query(query, strategyName)
	if err != nil {
		return nil, fmt.Errorf("could not get signal orders >> %w", err)
	}

	results := make(map[string]*model.SignalOrder)
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var orderID string
		var signalID int
		var strategyName string
		var instrument string
		var units int
		var price float64
		var deadline sql.NullTime
		var takePrice sql.NullFloat64
		var stopPrice sql.NullFloat64
		var tsDistance sql.NullFloat64
		var createdAt time.Time
		var updatedAt time.Time
		var rawData string
		if err := rows.Scan(&orderID, &signalID, &strategyName, &instrument, &units, &price, &deadline, &takePrice,
			&stopPrice, &tsDistance, &createdAt, &updatedAt, &rawData); err != nil {
			return nil, err
		}

		//build order
		order := &model.SignalOrder{}
		order.OrderID = orderID
		order.StrategyName = strategyName
		order.Instrument = instruments.ParseInstrument(instrument)
		order.Units = units
		order.Price = price
		if deadline.Valid {
			order.Deadline = &deadline.Time
		}
		if takePrice.Valid {
			order.TakePrice = &takePrice.Float64
		}
		if stopPrice.Valid {
			order.StopPrice = &stopPrice.Float64
		}
		if tsDistance.Valid {
			order.TSDistance = &tsDistance.Float64
		}
		order.CreatedAt = &createdAt
		order.UpdatedAt = &updatedAt
		order.Signal = &model.AutochartistSignal{}
		if err := json.Unmarshal([]byte(rawData), order.Signal); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal order id: %s", orderID)
		}

		//add to results
		results[order.OrderID] = order
	}

	return results, nil
}

//UpdateSignalOrder updates certain signal order values
func UpdateSignalOrder(order *model.SignalOrder, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//build query
	query := strings.Builder{}
	query.WriteString("UPDATE signal_orders SET updated_at = current_timestamp()")
	query.WriteString(fmt.Sprintf(", units = %d", order.Units))
	query.WriteString(fmt.Sprintf(", price = %.6f", order.Price))
	if order.Deadline != nil {
		query.WriteString(fmt.Sprintf(", deadline = '%s'", order.Deadline.Format("2006-01-02 15:04:05")))
	}
	if order.TakePrice != nil {
		query.WriteString(fmt.Sprintf(", take_price = %.6f", *order.TakePrice))
	}
	if order.StopPrice != nil {
		query.WriteString(fmt.Sprintf(", stop_price = %.6f", *order.StopPrice))
	}
	if order.FilledAt != nil {
		query.WriteString(fmt.Sprintf(", filled_at = '%s'", order.Deadline.Format("2006-01-02 15:04:05")))
	}
	query.WriteString(" WHERE order_id = ?")

	//update order
	_, err := db.Exec(query.String(), order.OrderID)
	if err != nil {
		return fmt.Errorf("could not update order >> %w", err)
	}

	return nil
}
