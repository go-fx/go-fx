package db

import (
	"database/sql"
	"fmt"

	"gofx/model"
)

//CreateDailyReport creates a new daily report entry in the db
func CreateDailyReport(data *model.ReportData, db *sql.DB) (int, error) {
	if db == nil {
		db = conn
	}

	//prepare strategy config JSON/string
	data.SetStrategyConfig()

	//insert data
	result, err := db.Exec(`INSERT INTO reports(alias, account_id, strategy_name, window_open_at, window_close_at, 
		close_balance, unrealized_pl, pl, active_trades, winning_trades, losing_trades, avg_profit_per_win,
		avg_loss_per_loss, strategy_config) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		data.Alias, data.AccountID, data.StrategyName, data.WindowOpenAt, data.WindowCloseAt,
		data.CloseBalance, data.UnrealizedPL, data.PL, data.ActiveTrades, data.WinningTrades, data.LosingTrades,
		data.AvgProfitPerWin, data.AvgLossPerLoss, data.StrategyConfig)
	if err != nil {
		return 0, fmt.Errorf("could not create new daily report >> %w", err)
	}

	//record new report ID
	reportID, err := result.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("could not create new daily report >> %w", err)
	}

	//insert daily report entry
	_, err = db.Exec(`INSERT INTO daily_reports(reportId) VALUES (?)`, reportID)
	if err != nil {
		return 0, fmt.Errorf("could not create new daily report >> %w", err)
	}

	return int(reportID), nil
}

//CreateWeeklyReport creates a new weekly report entry in the db
func CreateWeeklyReport(data *model.ReportData, db *sql.DB) (int, error) {
	if db == nil {
		db = conn
	}

	//prepare strategy config JSON/string
	data.SetStrategyConfig()

	//insert data
	result, err := db.Exec(`INSERT INTO reports(alias, account_id, strategy_name, window_open_at, window_close_at, 
		close_balance, unrealized_pl, pl, active_trades, winning_trades, losing_trades, avg_profit_per_win,
		avg_loss_per_loss, strategy_config) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		data.Alias, data.AccountID, data.StrategyName, data.WindowOpenAt, data.WindowCloseAt,
		data.CloseBalance, data.UnrealizedPL, data.PL, data.ActiveTrades, data.WinningTrades, data.LosingTrades,
		data.AvgProfitPerWin, data.AvgLossPerLoss, data.StrategyConfig)
	if err != nil {
		return 0, fmt.Errorf("could not create new weekly report >> %w", err)
	}

	//record new report ID
	reportID, err := result.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("could not create new weekly report >> %w", err)
	}

	//insert weekly report entry
	_, err = db.Exec(`INSERT INTO weekly_reports(reportId) VALUES (?)`, reportID)
	if err != nil {
		return 0, fmt.Errorf("could not create new weekly report >> %w", err)
	}

	return int(reportID), nil
}
