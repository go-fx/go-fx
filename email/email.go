package email

import (
	"fmt"
	"strconv"

	"gofx/interfaces"
	"gofx/model"
	"gofx/model/oanda"
)

var (
	tradeOpenTemplateFileName   = "email/templates/tradeOpen.email"
	tradeCloseTemplateFileName  = "email/templates/tradeClose.email"
	reportTemplateFileName      = "email/templates/report.email"
	windowAlertTemplateFileName = "email/templates/windowAlert.email"

	//ActiveProvider is the active provider for application emails
	ActiveProvider Provider
)

//Provider is the interface for working with an email provider
type Provider interface {
	SendTXOpenTrade(string, string, OpenTradeTemplateData, interfaces.HTTPClient, *string) (int, error)
	SendTXCloseTrade(string, string, CloseTradeTemplateData, interfaces.HTTPClient, *string) (int, error)
	SendTXReport(string, string, string, ReportTemplateData, interfaces.HTTPClient, *string) (int, error)
	SendTXWindowAlert(string, string, string, WindowAlertTemplateData, interfaces.HTTPClient, *string) (int, error)
}

//BaseTemplateData is common data shared among all email templates
type BaseTemplateData struct {
	AppName       string
	CompanyName   string
	CopyrightYear int
	CompanyLink   string
	StrategyName  string
}

//AccountTemplateData is common account snapshot data
type AccountTemplateData struct {
	AcctAlias       string
	AcctID          string
	AcctBalance     string
	AcctNAV         string
	AcctMarginAvail string
	AcctMarginUsed  string
	AcctMarginUtil  string
}

//NewAccountTemplateData creates an AccountTemplateData struct from an Account object
func NewAccountTemplateData(acct *oanda.Account) *AccountTemplateData {
	data := &AccountTemplateData{}
	data.AcctAlias = acct.Alias
	data.AcctID = acct.ID
	data.AcctBalance = acct.Balance
	data.AcctNAV = acct.NAV
	data.AcctMarginAvail = acct.MarginAvailable
	data.AcctMarginUsed = acct.MarginUsed

	//calculate margin util (%)
	nav, _ := strconv.ParseFloat(acct.NAV, 64)
	mUsed, _ := strconv.ParseFloat(acct.MarginUsed, 64)
	data.AcctMarginUtil = fmt.Sprintf("%.2f%%", 100*mUsed/nav)

	return data
}

//OpenTradeTemplateData wraps data for open trade emails
type OpenTradeTemplateData struct {
	BaseTemplateData
	AccountTemplateData
	Trade      *model.SignalTrade
	MarginUsed string
}

//CloseTradeTemplateData wraps data for close trade emails
type CloseTradeTemplateData struct {
	BaseTemplateData
	AccountTemplateData
	Trade  *model.SignalTrade
	Result string
}

//ReportTemplateData wraps data for daily/weekly report emails
type ReportTemplateData struct {
	BaseTemplateData
	AccountTemplateData
	Period           string
	BeginTimeRFC1123 string
	EndTimeRFC1123   string
	PL               string
	UnrealizedPL     string
	ActiveTrades     int
	WinningTrades    int
	LosingTrades     int
	ClosedTrades     int
	RatioWinLoss     string
	ProfitPerWin     string
	LossPerLoss      string
	RatioPPWLPL      string
	Trades           map[string]*model.SignalTrade
}

//WindowAlertTemplateData wraps data for trading window alert emails
type WindowAlertTemplateData struct {
	BaseTemplateData
	AccountTemplateData
	TypeTitle               string
	TypeLower               string
	WindowTimeRFC1123       string
	DurationUntilWindowTime string
}
