package email

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"gofx/interfaces"
)

//SendGrid is the interface for working with email service SendGrid
type SendGrid struct {
	ProviderEmailEndpointSendTX string
	ProviderEmailAuthHeader     string
}

//getSendTXEmailRequestBodyTemplate
func (sg SendGrid) getSendTXEmailRequestBody(to []string, from, subject, content string) string {
	builder := strings.Builder{}
	builder.WriteString(`{"personalizations": [{ "to": [`)

	for idx, toEmail := range to {
		var sep string
		if idx > 0 {
			sep = ","
		}
		builder.WriteString(fmt.Sprintf(`%s{ "email": "%s" }`, sep, toEmail))
	}

	builder.WriteString(fmt.Sprintf(`]}], "from": { "email": "%s" }, `, from))
	builder.WriteString(fmt.Sprintf(`"subject": "%s", `, subject))
	builder.WriteString(fmt.Sprintf(`"content": [{ "type": "text/html", "value": "%s"`, strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(content, "\"", "\\\""), "\n", ""), "%n", "\\n")))
	builder.WriteString(`}]}`)

	return builder.String()
}

//SendTXOpenTrade is to send tx email to verify trade opening
func (sg SendGrid) SendTXOpenTrade(recEmail, fromEmail string, data OpenTradeTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = tradeOpenTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, "New Trade Opened", renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}

//SendTXCloseTrade is to send tx email to verify trade closing
func (sg SendGrid) SendTXCloseTrade(recEmail, fromEmail string, data CloseTradeTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = tradeCloseTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, "Trade Closed", renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}

//SendTXReport sends tx email of activity report
func (sg SendGrid) SendTXReport(recEmail, fromEmail, subject string, data ReportTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = reportTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, subject, renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}

//SendTXWindowAlert sends tx email of window opening/closing in some time
func (sg SendGrid) SendTXWindowAlert(recEmail, fromEmail, alertType string, data WindowAlertTemplateData, client interfaces.HTTPClient, templatePath *string) (int, error) {
	//read template
	var fileName string
	if templatePath == nil {
		fileName = windowAlertTemplateFileName
	} else {
		fileName = *templatePath
	}
	emailTemplate, err := template.ParseFiles(fileName)
	if err != nil {
		return http.StatusInternalServerError, err
	}

	//render template
	var renderedBytes bytes.Buffer
	emailTemplate.Execute(&renderedBytes, data)

	//prepare email request
	emailReqBody := sg.getSendTXEmailRequestBody([]string{recEmail}, fromEmail, fmt.Sprintf("Trading Window %s Soon", data.TypeTitle), renderedBytes.String())

	// fmt.Println("body:", emailReqBody)

	mailReq, err := http.NewRequest("POST", sg.ProviderEmailEndpointSendTX, strings.NewReader(emailReqBody))
	mailReq.Header.Add("Authorization", sg.ProviderEmailAuthHeader)
	mailReq.Header.Add("Content-Type", "application/json")

	//send request
	res, resErr := client.Do(mailReq)
	if resErr != nil {
		return http.StatusInternalServerError, resErr
	}

	return res.StatusCode, nil
}
