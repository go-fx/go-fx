package email_test

import (
	"fmt"
	"net/http"
	"strings"
	"testing"
	"time"

	"gofx/email"
	"gofx/env"
	"gofx/model"
	"gofx/model/instruments"
)

func init() {
	email.ActiveProvider = email.SendGrid{
		ProviderEmailAuthHeader:     env.GetConfig().ProviderEmailAuthHeader,
		ProviderEmailEndpointSendTX: env.GetConfig().ProviderEmailEndpointSendTX,
	}
}

type testClient struct {
	isFailer bool
}

func (tc testClient) Do(r *http.Request) (*http.Response, error) {
	if tc.isFailer {
		return nil, fmt.Errorf("Generated testing error")
	}
	return &http.Response{StatusCode: http.StatusAccepted}, nil
}

func TestSendTXOpenTrade(t *testing.T) {
	//template path
	templatePath := "templates/tradeOpen.email"

	//test data
	testData := email.OpenTradeTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.StrategyName = "Dummy"
	testData.Trade = &model.SignalTrade{}
	testData.Trade.TradeID = "1234"
	testData.Trade.Signal = &model.AutochartistSignal{}
	testData.Trade.Signal.ID = 5678
	testData.Trade.Signal.Type = "keylevel"
	testData.Trade.Signal.InstrumentName = instruments.EurUsd.Name()
	testData.Trade.Units = 10000
	testData.Trade.EntryTimeRFC1123 = time.Now().Format(time.RFC1123)
	testData.Trade.EntryPrice = 1.1123
	take := 1.1223
	testData.Trade.TakePrice = &take
	stop := 1.1023
	testData.Trade.StopPrice = &stop
	testData.Trade.DeadlineRFC1123 = time.Now().Add(time.Hour).Format(time.RFC1123)
	testData.AcctBalance = "10000"
	testData.AcctNAV = "10100"
	testData.AcctMarginAvail = "10000"
	testData.AcctMarginUsed = "100"
	testData.AcctMarginUtil = "1.00%"

	actualRescode, actualErr := email.ActiveProvider.SendTXOpenTrade("user@gofx.live", env.GetConfig().ProviderEmailFromAddress, testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXOpenTrade >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXOpenTrade >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXOpenTradeClientError(t *testing.T) {
	//template path
	templatePath := "templates/tradeOpen.email"

	//test data
	testData := email.OpenTradeTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.StrategyName = "Dummy"
	testData.Trade = &model.SignalTrade{}
	testData.Trade.TradeID = "1234"
	testData.Trade.Signal = &model.AutochartistSignal{}
	testData.Trade.Signal.ID = 5678
	testData.Trade.Signal.Type = "keylevel"
	testData.Trade.Signal.InstrumentName = instruments.EurUsd.Name()
	testData.Trade.Units = 10000
	testData.Trade.EntryTimeRFC1123 = time.Now().Format(time.RFC1123)
	testData.Trade.EntryPrice = 1.1123
	take := 1.1223
	testData.Trade.TakePrice = &take
	stop := 1.1023
	testData.Trade.StopPrice = &stop
	testData.Trade.DeadlineRFC1123 = time.Now().Add(time.Hour).Format(time.RFC1123)
	testData.AcctBalance = "10000"
	testData.AcctNAV = "10100"
	testData.AcctMarginAvail = "10000"
	testData.AcctMarginUsed = "100"
	testData.AcctMarginUtil = "1.00%"

	actualRescode, actualErr := email.ActiveProvider.SendTXOpenTrade("user@example.com", env.GetConfig().ProviderEmailFromAddress, testData, &testClient{isFailer: true}, &templatePath)
	if actualRescode != http.StatusInternalServerError {
		t.Errorf("TestSendTXOpenTradeClientError >> expected response code: %d, actual: %d", http.StatusInternalServerError, actualRescode)
	}
	if actualErr == nil {
		t.Errorf("TestSendTXOpenTradeClientError >> expected err: %v, actual: %v", fmt.Errorf("Generated testing error"), actualErr)
	}
}

func TestSendTXCloseTrade(t *testing.T) {
	//template path
	templatePath := "templates/tradeClose.email"

	//test data
	testData := email.CloseTradeTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.StrategyName = "Dummy"
	testData.Trade = &model.SignalTrade{}
	testData.Trade.TradeID = "1234"
	testData.Trade.Signal = &model.AutochartistSignal{}
	testData.Trade.Signal.ID = 5678
	testData.Trade.Signal.Type = "keylevel"
	testData.Trade.Signal.InstrumentName = instruments.EurUsd.Name()
	testData.Trade.Units = 10000
	testData.Trade.EntryTimeRFC1123 = time.Now().Add(-time.Hour).Format(time.RFC1123)
	testData.Trade.EntryPrice = 1.1123
	take := 1.1223
	testData.Trade.TakePrice = &take
	stop := 1.1023
	testData.Trade.StopPrice = &stop
	testData.Trade.DeadlineRFC1123 = time.Now().Format(time.RFC1123)
	exitPrice := 1.1223
	testData.Trade.ExitPrice = &exitPrice
	testData.Trade.ExitTimeRFC1123 = time.Now().Format(time.RFC1123)
	pl := 100.
	testData.Trade.ProfitLoss = &pl
	testData.AcctBalance = "10000"
	testData.AcctNAV = "10100"
	testData.AcctMarginAvail = "10000"
	testData.AcctMarginUsed = "100"
	testData.AcctMarginUtil = "1.00%"
	testData.Result = "PROFIT"

	actualRescode, actualErr := email.ActiveProvider.SendTXCloseTrade("user@gofx.live", env.GetConfig().ProviderEmailFromAddress, testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXCloseTrade >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXCloseTrade >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXReport(t *testing.T) {
	//template path
	templatePath := "templates/report.email"

	//test data
	testData := email.ReportTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.Period = "Daily"
	testData.PL = "1000.00"
	testData.UnrealizedPL = "-100.00"
	testData.StrategyName = "Dummy"
	trade := &model.SignalTrade{}
	trade.TradeID = "1"
	trade.Signal = &model.AutochartistSignal{}
	trade.Signal.ID = 12345678
	trade.Signal.Instrument = instruments.EurUsd
	trade.StrategyName = "Dummy"
	trade.Instrument = instruments.EurUsd
	trade.Units = 10000
	trade.EntryPrice = 1.07
	trade.EntryTime = time.Now()
	takePrice := 1.08
	trade.TakePrice = &takePrice
	deadline := time.Now().Add(time.Hour * 24)
	trade.Deadline = &deadline

	actualRescode, actualErr := email.ActiveProvider.SendTXReport("user@gofx.live", env.GetConfig().ProviderEmailFromAddress, fmt.Sprintf("%s Summary Report", testData.Period), testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXReport >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXReport >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXWindowOpeningAlert(t *testing.T) {
	//template path
	templatePath := "templates/windowAlert.email"

	//test data
	testData := email.WindowAlertTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.StrategyName = "Dummy"
	testData.TypeTitle = "Opening"
	testData.TypeLower = "opening"
	windowTime := time.Now().Add(time.Minute * 88).Round(time.Minute)
	testData.WindowTimeRFC1123 = windowTime.Format(time.RFC1123)
	testData.DurationUntilWindowTime = windowTime.Sub(time.Now().Round(time.Minute)).String()
	//cut out seconds
	testData.DurationUntilWindowTime = testData.DurationUntilWindowTime[:strings.Index(testData.DurationUntilWindowTime, "m")+1]
	testData.AcctBalance = "10000"
	testData.AcctNAV = "10100"
	testData.AcctMarginAvail = "10000"
	testData.AcctMarginUsed = "100"
	testData.AcctMarginUtil = "1.00%"

	actualRescode, actualErr := email.ActiveProvider.SendTXWindowAlert("user@gofx.live", env.GetConfig().ProviderEmailFromAddress, fmt.Sprintf("Trading Window %s Soon", testData.TypeTitle), testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXWindowOpeningAlert >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXWindowOpeningAlert >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestSendTXWindowClosingAlert(t *testing.T) {
	//template path
	templatePath := "templates/windowAlert.email"

	//test data
	testData := email.WindowAlertTemplateData{}

	testData.AppName = env.GetConfig().AppName
	testData.CompanyLink = env.GetConfig().AppCompanyLink
	testData.CompanyName = env.GetConfig().AppCompany
	testData.CopyrightYear = time.Now().Year()
	testData.StrategyName = "Dummy"
	testData.TypeTitle = "Closing"
	testData.TypeLower = "closing"
	windowTime := time.Now().Add(time.Minute * 88).Round(time.Minute)
	testData.WindowTimeRFC1123 = windowTime.Format(time.RFC1123)
	testData.DurationUntilWindowTime = windowTime.Sub(time.Now().Round(time.Minute)).String()
	//cut out seconds
	testData.DurationUntilWindowTime = testData.DurationUntilWindowTime[:strings.Index(testData.DurationUntilWindowTime, "m")+1]
	testData.AcctBalance = "10000"
	testData.AcctNAV = "10100"
	testData.AcctMarginAvail = "10000"
	testData.AcctMarginUsed = "100"
	testData.AcctMarginUtil = "1.00%"

	actualRescode, actualErr := email.ActiveProvider.SendTXWindowAlert("user@gofx.live", env.GetConfig().ProviderEmailFromAddress, fmt.Sprintf("Trading Window %s Soon", testData.TypeTitle), testData, &testClient{}, &templatePath)
	if actualRescode != http.StatusAccepted {
		t.Errorf("TestSendTXWindowClosingAlert >> expected response code: %d, actual: %d", http.StatusAccepted, actualRescode)
	}
	if actualErr != nil {
		t.Errorf("TestSendTXWindowClosingAlert >> expected err: %v, actual: %v", nil, actualErr)
	}
}

func TestGetSendTXEmailRequestBody(t *testing.T) {
	//email client
	sendGrid := email.SendGrid{}

	expected := `{"personalizations": [{ "to": [{ "email": "test@example.org" },{ "email": "test2@example.org" }]}], "from": { "email": "gofx@gofx.live" }, "subject": "A Testy Subject", "content": [{ "type": "text/html", "value": "<h1>Simple Header</h1>"}]}`
	actual := email.GetSendTXEmailRequestBody(sendGrid, []string{"test@example.org", "test2@example.org"}, "gofx@gofx.live", "A Testy Subject", "<h1>Simple Header</h1>")
	if actual != expected {
		t.Errorf("TestGetSendTXEmailRequestBody >> expected: %v, actual: %v", expected, actual)
	}
}
