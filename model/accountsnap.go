package model

import (
	"encoding/json"
	"strconv"
	"time"

	"gofx/model/oanda"
)

//AccountSnapshot represents the state of a broker account at the opening of trading window
type AccountSnapshot struct {
	Base
	WindowOpenAt        time.Time `json:"windowOpen"`
	WindowOpenAtRFC1123 string    `json:"-"`
	Balance             float64   `json:"balance"`
	NAV                 float64   `json:"NAV"`
	UnrealizedPL        float64   `json:"unrealizedPL"`
	MarginUsed          float64   `json:"marginUsed"`
	MarginAvailable     float64   `json:"marginAvailable"`
	WithdrawalLimit     float64   `json:"withdrawalLimit"`
	OpenTradeCount      int       `json:"openTradeCount"`
	PendingOrderCount   int       `json:"pendingOrderCount"`
	Financing           float64   `json:"financing"`
	LastTransactionID   *string   `json:"lastTransactionID,omitempty"`
	AccountID           string    `json:"accountID"`
	Alias               string    `json:"alias"`
	Currency            string    `json:"currency"`
	MarginRate          float64   `json:"marginRate"`
	CreatedByUserID     int       `json:"createdByUserID"`
	AcctRawData         string    `json:"-"`
}

func (a *AccountSnapshot) String() string {
	stBytes, _ := json.Marshal(a)
	return string(stBytes)
}

//NewAccountSnapshot produces an AccountSnapshot db model from oanda account model
func NewAccountSnapshot(windowOpen time.Time, brokerAcct *oanda.Account) *AccountSnapshot {
	snapshot := &AccountSnapshot{}
	snapshot.WindowOpenAt = windowOpen
	snapshot.WindowOpenAtRFC1123 = windowOpen.Format(time.RFC1123)
	snapshot.AccountID = brokerAcct.ID
	snapshot.Currency = brokerAcct.Currency
	snapshot.Alias = brokerAcct.Alias
	snapshot.MarginRate, _ = strconv.ParseFloat(brokerAcct.MarginRate, 64)
	snapshot.LastTransactionID = &brokerAcct.LastTransactionID
	snapshot.OpenTradeCount = brokerAcct.OpenTradeCount
	snapshot.PendingOrderCount = brokerAcct.PendingOrderCount
	snapshot.Financing, _ = strconv.ParseFloat(brokerAcct.Financing, 64)
	snapshot.Balance, _ = strconv.ParseFloat(brokerAcct.Balance, 64)
	snapshot.NAV, _ = strconv.ParseFloat(brokerAcct.NAV, 64)
	snapshot.UnrealizedPL, _ = strconv.ParseFloat(brokerAcct.UnrealizedPL, 64)
	snapshot.MarginUsed, _ = strconv.ParseFloat(brokerAcct.MarginUsed, 64)
	snapshot.MarginAvailable, _ = strconv.ParseFloat(brokerAcct.MarginAvailable, 64)
	snapshot.WithdrawalLimit, _ = strconv.ParseFloat(brokerAcct.WithdrawalLimit, 64)
	rawData, _ := json.Marshal(brokerAcct)
	snapshot.AcctRawData = string(rawData)

	return snapshot
}
