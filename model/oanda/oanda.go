package oanda

//Tag is the model for tag
type Tag struct {
	Type string `json:"type"`
	Name string `json:"name"`
}
