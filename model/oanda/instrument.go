package oanda

//Instrument oanda model
type Instrument struct {
	Name                        string                `json:"name"`
	Type                        string                `json:"type"`
	DisplayName                 string                `json:"displayName"`
	PipLocation                 int                   `json:"pipLocation"`
	DisplayPrecision            int                   `json:"displayPrecision"`
	TradeUnitsPrecision         int                   `json:"tradeUnitsPrecision"`
	MinimumTradeSize            string                `json:"minimumTradeSize"`
	MaximumTrailingStopDistance string                `json:"maximumTrailingStopDistance"`
	MinimumTrailingStopDistance string                `json:"minimumTrailingStopDistance"`
	MaximumPositionSize         string                `json:"maximumPositionSize"`
	MaximumOrderUnits           string                `json:"maximumOrderUnits"`
	MarginRate                  string                `json:"marginRate"`
	Commission                  *InstrumentCommission `json:"commission"`
	Financing                   *InstrumentFinancing  `json:"financing"`
	Tags                        []Tag                 `json:"tags"`
}

//InstrumentCommission oanda model
type InstrumentCommission struct {
	Commission        string `json:"commission"`
	UnitsTraded       string `json:"unitsTraded"`
	MinimumCommission string `json:"minimumCommission"`
}

//InstrumentFinancing oanda model
type InstrumentFinancing struct {
	LongRate            string      `json:"longRate"`
	ShortRate           string      `json:"shortRate"`
	FinancingDaysOfWeek []DayOfWeek `json:"financingDaysOfWeek"`
}

//DayOfWeek oanda model
type DayOfWeek struct {
	DayOfWeek   string `json:"dayOfWeek"`
	DaysCharged int    `json:"daysCharged"`
}
