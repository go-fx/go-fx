package oanda

import (
	"time"
)

//ClientPrice is the model for an account-specific price
type ClientPrice struct {
	Type            string         `json:"type"`
	Instrument      string         `json:"instrument"`
	TimeRFC3339Nano string         `json:"time"`
	Time            time.Time      `json:"-"`
	Tradeable       bool           `json:"tradeable"`
	Bids            []*PriceBucket `json:"bids"`
	Asks            []*PriceBucket `json:"asks"`
	CloseoutBid     string         `json:"closeoutBid"`
	CloseoutAsk     string         `json:"closeoutAsk"`
}

//PriceBucket is the model for available price/liquidity
type PriceBucket struct {
	Price     string `json:"price"`
	Liquidity int    `json:"liquidity"`
}

//HomeConversions is the model that contains factors to convert quantities of instrument to account home currency
type HomeConversions struct {
	Currency      string `json:"currency"`
	AccountGain   string `json:"accountGain"`
	AccountLoss   string `json:"accountLoss"`
	PositionValue string `json:"positionValue"`
}
