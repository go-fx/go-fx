package oanda

import (
	"time"
)

//Trade is the trade model
type Trade struct {
	ID                    string                 `json:"id"`
	Instrument            string                 `json:"instrument"`
	Price                 string                 `json:"price"`
	OpenTimeRFC3339Nano   string                 `json:"openTime"`
	OpenTime              time.Time              `json:"-"`
	StateName             string                 `json:"state"`
	InitialUnits          string                 `json:"initialUnits"`
	InitialMarginRequired string                 `json:"initialMarginRequired"`
	CurrentUnits          string                 `json:"currentUnits"`
	RealizedPL            string                 `json:"realizedPL"`
	UnrealizedPL          string                 `json:"unrealizedPL"`
	MarginUsed            string                 `json:"marginUsed"`
	AverageClosePrice     string                 `json:"averageClosePrice"`
	ClosingTransactionIDs []string               `json:"closingTransactionIDs"`
	Financing             string                 `json:"financing"`
	DividendAdjustment    string                 `json:"dividendAdjustment"`
	CloseTimeRFC3339Nano  *string                `json:"closeTime"`
	CloseTime             *time.Time             `json:"-"`
	ClientExtensions      map[string]string      `json:"clientExtensions,omitempty"`
	TakeProfitOrder       *TakeProfitOrder       `json:"takeProfitOrder"`
	StopLossOrder         *StopLossOrder         `json:"stopLossOrder"`
	TrailingStopLossOrder *TrailingStopLossOrder `json:"trailingStopLossOrder"`
}

//TradeOpened is the trade open model
type TradeOpened struct {
	TradeID                string            `json:"tradeID"`
	Units                  string            `json:"units"`
	Price                  string            `json:"price"`
	GuaranteedExecutionFee string            `json:"guaranteedExecutionFee"`
	ClientExtensions       map[string]string `json:"clientExtensions,omitempty"`
	HalfSpreadCost         string            `json:"halfSpreadCost"`
	InitialMarginRequired  string            `json:"initialMarginRequired"`
}

//TradeReduce is the trade reduce model
type TradeReduce struct {
	TradeID                string `json:"tradeID"`
	Units                  string `json:"units"`
	Price                  string `json:"price"`
	RealizedPL             string `json:"realizedPL"`
	Financing              string `json:"financing"`
	GuaranteedExecutionFee string `json:"guaranteedExecutionFee"`
	HalfSpreadCost         string `json:"halfSpreadCost"`
}
