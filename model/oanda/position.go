package oanda

//Position is the position model
type Position struct {
	Instrument              string        `json:"instrument"`
	Long                    *PositionSide `json:"long"`
	Short                   *PositionSide `json:"short"`
	PL                      string        `json:"pl"`
	ResettablePL            string        `json:"resettablePL"`
	Financing               string        `json:"financing"`
	Commission              string        `json:"commission"`
	DividendAdjustment      string        `json:"dividendAdjustment"`
	GuaranteedExecutionFees string        `json:"guaranteedExecutionFees"`
	UnrealizedPL            string        `json:"unrealizedPL"`
	MarginUsed              string        `json:"marginUsed"`
}

//PositionSide is the position side model
type PositionSide struct {
	Units                   string   `json:"units"`
	PL                      string   `json:"pl"`
	ResettablePL            string   `json:"resettablePL"`
	TradeIDs                []string `json:"tradeIDs"`
	Financing               string   `json:"financing"`
	DividendAdjustment      string   `json:"dividendAdjustment"`
	GuaranteedExecutionFees string   `json:"guaranteedExecutionFees"`
	UnrealizedPL            string   `json:"unrealizedPL"`
}
