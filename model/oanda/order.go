package oanda

import (
	"time"
)

//Order is the order model
type Order struct {
	ID                    string            `json:"id"`
	CreateTimeRFC3339Nano string            `json:"createTime,omitempty"`
	StateName             string            `json:"state,omitempty"`
	ClientExtensions      map[string]string `json:"clientExtensions,omitempty"`
	Type                  string            `json:"type,omitempty"`
}

//MarketOrder is the market order model
type MarketOrder struct {
	Order
	Instrument               string                        `json:"instrument"`
	Units                    string                        `json:"units"`
	TimeInForceName          string                        `json:"timeInForce,omitempty"`
	PriceBound               string                        `json:"priceBound,omitempty"`
	PositionFillName         string                        `json:"positionFill"`
	TradeClose               *MarketOrderTradeClose        `json:"tradeClose,omitempty"`
	LongPositionCloseout     *MarketOrderPositionCloseout  `json:"longPositionCloseout,omitempty"`
	ShortPositionCloseout    *MarketOrderPositionCloseout  `json:"shortPositionCloseout,omitempty"`
	MarginCloseout           *MarketOrderMarginCloseout    `json:"marginCloseout,omitempty"`
	DelayedTradeClose        *MarketOrderDelayedTradeClose `json:"delayedTradeClose,omitempty"`
	TakeProfitOnFill         *TakeProfitDetails            `json:"takeProfitOnFill,omitempty"`
	StopLossOnFill           *StopLossDetails              `json:"stopLossOnFill,omitempty"`
	TrailingStopLossOnFill   *TrailingStopLossDetails      `json:"trailingStopLossOnFill,omitempty"`
	TradeClientExtensions    map[string]string             `json:"tradeClientExtensions,omitempty"`
	FillingTransactionID     string                        `json:"fillingTransactionID,omitempty"`
	FilledTimeRFC3339Nano    string                        `json:"filledTime,omitempty"`
	FilledTime               time.Time                     `json:"-"`
	TradeOpenedID            string                        `json:"tradeOpenedID,omitempty"`
	TradeReducedID           string                        `json:"tradeReducedID,omitempty"`
	TradeClosedIDs           []string                      `json:"tradeClosedIDs,omitempty"`
	CancellingTransactionID  string                        `json:"cancellingTransactionID,omitempty"`
	CancelledTimeRFC3339Nano string                        `json:"cancelledTime,omitempty"`
	CancelledTime            time.Time                     `json:"-"`
}

//LimitStopOrder is the common model for limit/stop orders
type LimitStopOrder struct {
	Order
	Instrument               string                   `json:"instrument"`
	Units                    string                   `json:"units"`
	Price                    string                   `json:"price"`
	TimeInForceName          string                   `json:"timeInForce"`
	GTDTimeRFC3339Nano       string                   `json:"gtdTime,omitempty"`
	GTDTime                  time.Time                `json:"-"`
	PositionFillName         string                   `json:"positionFill,omitempty"`
	TriggerCondition         string                   `json:"triggerCondition,omitempty"`
	TakeProfitOnFill         *TakeProfitDetails       `json:"takeProfitOnFill,omitempty"`
	StopLossOnFill           *StopLossDetails         `json:"stopLossOnFill,omitempty"`
	TrailingStopLossOnFill   *TrailingStopLossDetails `json:"trailingStopLossOnFill,omitempty"`
	TradeClientExtensions    map[string]string        `json:"tradeClientExtensions,omitempty"`
	FillingTransactionID     string                   `json:"fillingTransactionID,omitempty"`
	FilledTimeRFC3339Nano    string                   `json:"filledTime,omitempty"`
	FilledTime               time.Time                `json:"-"`
	TradeOpenedID            string                   `json:"tradeOpenedID,omitempty"`
	TradeReducedID           string                   `json:"tradeReducedID,omitempty"`
	TradeClosedIDs           []string                 `json:"tradeClosedIDs,omitempty"`
	CancellingTransactionID  string                   `json:"cancellingTransactionID,omitempty"`
	CancelledTimeRFC3339Nano string                   `json:"cancelledTime,omitempty"`
	CancelledTime            time.Time                `json:"-"`
	ReplacesOrderID          string                   `json:"replacesOrderID,omitempty"`
	ReplacedByOrderID        string                   `json:"replacedByOrderID,omitempty"`
}

//LimitOrder is the limit order model
type LimitOrder struct {
	LimitStopOrder
}

//StopOrder is the stop order model
type StopOrder struct {
	LimitStopOrder
	PriceBound string `json:"priceBound"`
}

//ExitTradeOrder is the common model for tp/sl/tsl
type ExitTradeOrder struct {
	Order
	TradeID                  string    `json:"tradeID"`
	ClientTradeID            string    `json:"clientTradeID"`
	TimeInForceName          string    `json:"timeInForce,omitempty"`
	GTDTimeRFC3339Nano       string    `json:"gtdTime,omitempty"`
	GTDTime                  time.Time `json:"-"`
	TriggerCondition         string    `json:"triggerCondition,omitempty"`
	FillingTransactionID     string    `json:"fillingTransactionID,omitempty"`
	FilledTimeRFC3339Nano    string    `json:"filledTime,omitempty"`
	FilledTime               time.Time `json:"-"`
	TradeOpenedID            string    `json:"tradeOpenedID,omitempty"`
	TradeReducedID           string    `json:"tradeReducedID,omitempty"`
	TradeClosedIDs           []string  `json:"tradeClosedIDs,omitempty"`
	CancellingTransactionID  string    `json:"cancellingTransactionID,omitempty"`
	CancelledTimeRFC3339Nano string    `json:"cancelledTime,omitempty"`
	CancelledTime            time.Time `json:"-"`
	ReplacesOrderID          string    `json:"replacesOrderID,omitempty"`
	ReplacedByOrderID        string    `json:"replacedByOrderID,omitempty"`
}

//TakeProfitOrder is the tp order model
type TakeProfitOrder struct {
	ExitTradeOrder
	Price string `json:"price"`
}

//StopLossOrder is the tp order model
type StopLossOrder struct {
	ExitTradeOrder
	Price    string `json:"price"`
	Distance string `json:"distance"`
}

//TrailingStopLossOrder is the tp order model
type TrailingStopLossOrder struct {
	ExitTradeOrder
	Distance          string `json:"distance"`
	TrailingStopValue string `json:"trailingStopValue"`
}

//MarketOrderTradeClose is the model when market order used to close trade
type MarketOrderTradeClose struct {
	TradeID       string `json:"tradeID"`
	ClientTradeID string `json:"clientTradeID"`
	Units         string `json:"units"`
}

//MarketOrderPositionCloseout is the model for requested closeout
type MarketOrderPositionCloseout struct {
	Instrument string `json:"instrument"`
	Units      string `json:"units"`
}

//MarketOrderMarginCloseout is the model for requested closeout
type MarketOrderMarginCloseout struct {
	ReasonName string `json:"reason"`
}

//MarketOrderDelayedTradeClose is the model for requested closeout
type MarketOrderDelayedTradeClose struct {
	TradeID             string `json:"tradeID"`
	ClientTradeID       string `json:"clientTradeID"`
	SourceTransactionID string `json:"sourceTransactionID"`
}

//TakeProfitDetails is the model for tp details
type TakeProfitDetails struct {
	Price              string            `json:"price"`
	TimeInForceName    string            `json:"timeInForce,omitempty"`
	GTDTimeRFC3339Nano string            `json:"gtdTime,omitempty"`
	GTDTime            time.Time         `json:"-"`
	ClientExtensions   map[string]string `json:"clientExtensions,omitempty"`
}

//StopLossDetails is the model for sl details
type StopLossDetails struct {
	Price              string            `json:"price,omitempty"`
	Distance           string            `json:"distance,omitempty"`
	TimeInForceName    string            `json:"timeInForce,omitempty"`
	GTDTimeRFC3339Nano string            `json:"gtdTime,omitempty"`
	GTDTime            time.Time         `json:"-"`
	ClientExtensions   map[string]string `json:"clientExtensions,omitempty"`
}

//TrailingStopLossDetails is the model for tsl details
type TrailingStopLossDetails struct {
	Distance           string            `json:"distance"`
	TimeInForceName    string            `json:"timeInForce,omitempty"`
	GTDTimeRFC3339Nano string            `json:"gtdTime,omitempty"`
	GTDTime            time.Time         `json:"-"`
	ClientExtensions   map[string]string `json:"clientExtensions,omitempty"`
}

//AccountOrder is the model for orders as they're returned from list request
type AccountOrder struct {
	Order
	Instrument             string                   `json:"instrument,omitempty"`
	Units                  string                   `json:"units,omitempty"`
	Price                  string                   `json:"price,omitempty"`
	Distance               string                   `json:"distance,omitempty"`
	TrailingStopValue      string                   `json:"trailingStopValue,omitempty"`
	TriggerCondition       string                   `json:"triggerCondition,omitempty"`
	TakeProfitOnFill       *TakeProfitDetails       `json:"takeProfitOnFill,omitempty"`
	StopLossOnFill         *StopLossDetails         `json:"stopLossOnFill,omitempty"`
	TrailingStopLossOnFill *TrailingStopLossDetails `json:"trailingStopLossOnFill,omitempty"`
	FillingTransactionID   *string                  `json:"fillingTransactionID,omitempty"`
	FilledTimeRFC3339Nano  *string                  `json:"filledTime,omitempty"`
	FilledTime             *time.Time               `json:"-"`
	TradeOpenedID          *string                  `json:"tradeOpenedID,omitempty"`
	ReplacesOrderID        string                   `json:"replacesOrderID,omitempty"`
	TimeInForceName        string                   `json:"timeInForce,omitempty"`
	PartialFill            string                   `json:"partialFill,omitempty"`
	PositionFill           string                   `json:"positionFill,omitempty"`
	GTDTimeRFC3339Nano     string                   `json:"gtdTime,omitempty"`
	GTDTime                time.Time                `json:"-"`
}
