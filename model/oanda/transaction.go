package oanda

import (
	"time"
)

//Transaction is the base transaction model
type Transaction struct {
	ID              string     `json:"id"`
	TimeRFC3339Nano string     `json:"time"`
	Time            *time.Time `json:"-"`
	UserID          int        `json:"userID"`
	AccountID       string     `json:"accountID"`
	BatchID         string     `json:"batchID"`
	RequestID       string     `json:"requestID"`
}

//OrderFillTransaction is the model for a tx where order was filled
type OrderFillTransaction struct {
	Transaction
	Type                          string         `json:"type"`
	OrderID                       string         `json:"orderID"`
	ClientOrderID                 string         `json:"clientOrderID"`
	Instrument                    string         `json:"instrument"`
	Units                         string         `json:"units"`
	GainQuoteHomeConversionFactor string         `json:"gainQuoteHomeConversionFactor"`
	LossQuoteHomeConversionFactor string         `json:"lossQuoteHomeConversionFactor"`
	FullVWAP                      string         `json:"fullVWAP"`
	FullPrice                     *FullPrice     `json:"fullPrice"`
	Reason                        string         `json:"reason"`
	PL                            string         `json:"pl"`
	Financing                     string         `json:"financing"`
	Commission                    string         `json:"commission"`
	GuaranteedExecutionFee        string         `json:"guaranteedExecutionFee"`
	AccountBalance                string         `json:"accountBalance"`
	TradeOpened                   *TradeOpened   `json:"tradeOpened"`
	TradesClosed                  []*TradeReduce `json:"tradesClosed"`
	TradeReduced                  *TradeReduce   `json:"tradeReduced"`
	HalfSpreadCost                string         `json:"halfSpreadCost"`
}

//FullPrice is the model for an order fill response full price
type FullPrice struct {
	Type            string             `json:"type"`
	Instrument      string             `json:"instrument"`
	TimeRFC3339Nano string             `json:"time"`
	Tradeable       bool               `json:"tradeable"`
	Bids            []*FullPriceBucket `json:"bids"`
	Asks            []*FullPriceBucket `json:"asks"`
	CloseoutBid     string             `json:"closeoutBid"`
	CloseoutAsk     string             `json:"closeoutAsk"`
}

//FullPriceBucket is the model for available price/liquidity
type FullPriceBucket struct {
	Price     string `json:"price"`
	Liquidity string `json:"liquidity"`
}

//OrderCancelTransaction is the model for a tx where order was cancelled
type OrderCancelTransaction struct {
	Transaction
	Type              string `json:"type"`
	OrderID           string `json:"orderID"`
	ClientOrderID     string `json:"clientOrderID"`
	Reason            string `json:"reason"`
	ReplacedByOrderID string `json:"replacedByOrderID"`
}

//OrderCancelRejectTransaction is the model for when cancelling order is rejected
type OrderCancelRejectTransaction struct {
	Transaction
	Type          string `json:"type"`
	OrderID       string `json:"orderID"`
	ClientOrderID string `json:"clientOrderID"`
	RejectReason  string `json:"rejectReason"`
}

//MarketOrderTransaction is the model for tx for order filled at market
type MarketOrderTransaction struct {
	Transaction
	Type                   string                        `json:"type"`
	Instrument             string                        `json:"instrument"`
	Units                  string                        `json:"units"`
	TimeInForce            string                        `json:"timeInForce"`
	PriceBound             string                        `json:"priceBound"`
	PositionFill           string                        `json:"positionFill"`
	TradeClose             *MarketOrderTradeClose        `json:"tradeClose"`
	LongPositionCloseout   *MarketOrderPositionCloseout  `json:"longPositionCloseout"`
	ShortPositionCloseout  *MarketOrderPositionCloseout  `json:"shortPositionCloseout"`
	MarginCloseout         *MarketOrderMarginCloseout    `json:"marginCloseout"`
	DelayedTradeClose      *MarketOrderDelayedTradeClose `json:"delayedTradeClose"`
	Reason                 string                        `json:"reason"`
	ClientExtensions       map[string]string             `json:"clientExtensions,omitempty"`
	TakeProfitOnFill       *TakeProfitDetails            `json:"takeProfitOnFill"`
	StopLossOnFill         *StopLossDetails              `json:"stopLossOnFill"`
	TrailingStopLossOnFill *TrailingStopLossDetails      `json:"trailingStopLossOnFill"`
	TradeClientExtensions  map[string]string             `json:"tradeClientExtensions,omitempty"`
}

//TakeProfitOrderTransaction is the model for tp order filled tx
type TakeProfitOrderTransaction struct {
	Transaction
	Type                    string            `json:"type"`
	TradeID                 string            `json:"tradeID"`
	ClientTradeID           string            `json:"clientTradeID"`
	Price                   string            `json:"price"`
	TimeInForce             string            `json:"timeInForce"`
	GTDTimeRFC3339Nano      string            `json:"gtdTime"`
	TriggerCondition        string            `json:"triggerCondition"`
	Reason                  string            `json:"reason"`
	ClientExtensions        map[string]string `json:"clientExtensions,omitempty"`
	OrderFillTransactionID  string            `json:"orderFillTransactionID"`
	ReplacesOrderID         string            `json:"replacesOrderID"`
	CancellingTransactionID string            `json:"cancellingTransactionID"`
}

//StopLossOrderTransaction is the model for sl order filled tx
type StopLossOrderTransaction struct {
	Transaction
	Type                    string            `json:"type"`
	TradeID                 string            `json:"tradeID"`
	ClientTradeID           string            `json:"clientTradeID"`
	Price                   string            `json:"price"`
	Distance                string            `json:"distance"`
	TimeInForce             string            `json:"timeInForce"`
	GTDTimeRFC3339Nano      string            `json:"gtdTime"`
	TriggerCondition        string            `json:"triggerCondition"`
	Reason                  string            `json:"reason"`
	ClientExtensions        map[string]string `json:"clientExtensions,omitempty"`
	OrderFillTransactionID  string            `json:"orderFillTransactionID"`
	ReplacesOrderID         string            `json:"replacesOrderID"`
	CancellingTransactionID string            `json:"cancellingTransactionID"`
}

//TrailingStopLossOrderTransaction is the model for tsl filled tx
type TrailingStopLossOrderTransaction struct {
	Transaction
	Type                    string            `json:"type"`
	TradeID                 string            `json:"tradeID"`
	ClientTradeID           string            `json:"clientTradeID"`
	Distance                string            `json:"distance"`
	TimeInForce             string            `json:"timeInForce"`
	GTDTimeRFC3339Nano      string            `json:"gtdTime"`
	TriggerCondition        string            `json:"triggerCondition"`
	Reason                  string            `json:"reason"`
	ClientExtensions        map[string]string `json:"clientExtensions,omitempty"`
	OrderFillTransactionID  string            `json:"orderFillTransactionID"`
	ReplacesOrderID         string            `json:"replacesOrderID"`
	CancellingTransactionID string            `json:"cancellingTransactionID"`
}
