package oanda

import (
	"time"
)

//Account is the full account model
type Account struct {
	ID                          string      `json:"id"`
	CreatedTimeRFC3339Nano      string      `json:"createdTime"`
	CreatedTime                 time.Time   `json:"-"`
	CreatedByUserID             int         `json:"createdByUserID"`
	Currency                    string      `json:"currency"`
	Alias                       string      `json:"alias"`
	MarginRate                  string      `json:"marginRate"`
	GuaranteedStopLossOrderMode string      `json:"guaranteedStopLossOrderMode"`
	HedgingEnabled              bool        `json:"hedgingEnabled"`
	LastTransactionID           string      `json:"lastTransactionID"`
	Balance                     string      `json:"balance"`
	OpenTradeCount              int         `json:"openTradeCount"`
	OpenPositionCount           int         `json:"openPositionCount"`
	PendingOrderCount           int         `json:"pendingOrderCount"`
	PL                          string      `json:"pl"`
	ResettablePL                string      `json:"resettablePL"`
	ResettablePLTimeRFC3339Nano string      `json:"resettablePLTime"`
	ResettablePLTime            time.Time   `json:"-"`
	Financing                   string      `json:"financing"`
	Commission                  string      `json:"commission"`
	DividendAdjustment          string      `json:"dividendAdjustment"`
	GuaranteedExecutionFees     string      `json:"guaranteedExecutionFees"`
	Orders                      []*Order    `json:"orders"`
	Positions                   []*Position `json:"positions"`
	Trades                      []*Trade    `json:"trades"`
	UnrealizedPL                string      `json:"unrealizedPL"`
	NAV                         string      `json:"NAV"`
	MarginUsed                  string      `json:"marginUsed"`
	MarginAvailable             string      `json:"marginAvailable"`
	PositionValue               string      `json:"positionValue"`
	MarginCloseoutUnrealizedPL  string      `json:"marginCloseoutUnrealizedPL"`
	MarginCloseoutNAV           string      `json:"marginCloseoutNAV"`
	MarginCloseoutMarginUsed    string      `json:"marginCloseoutMarginUsed"`
	MarginCloseoutPositionValue string      `json:"marginCloseoutPositionValue"`
	MarginCloseoutPercent       string      `json:"marginCloseoutPercent"`
	WithdrawalLimit             string      `json:"withdrawalLimit"`
	MarginCallMarginUsed        string      `json:"marginCallMarginUsed"`
	MarginCallPercent           string      `json:"marginCallPercent"`
}
