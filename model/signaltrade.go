package model

import (
	"encoding/json"
	"time"

	"gofx/model/instruments"
)

//SignalTrade represents a trade associated with a signal
type SignalTrade struct {
	BaseNoID
	TradeID          string                 `json:"tradeID"`
	Signal           *AutochartistSignal    `json:"signal"`
	StrategyName     string                 `json:"strategyName"`
	Instrument       instruments.Instrument `json:"instrument"`
	Units            int                    `json:"units"`
	EntryPrice       float64                `json:"entryPrice"`
	EntryTime        time.Time              `json:"entryTime"`
	EntryTimeRFC1123 string                 `json:"-"`
	TakePrice        *float64               `json:"takePrice,omitempty"`
	Deadline         *time.Time             `json:"deadline,omitempty"`
	DeadlineRFC1123  string                 `json:"-"`
	StopPrice        *float64               `json:"stopPrice,omitempty"`
	TSDistance       *float64               `json:"tsDistance,omitempty"`
	ExitPrice        *float64               `json:"exitPrice,omitempty"`
	ExitTime         *time.Time             `json:"exitTime,omitempty"`
	ExitTimeRFC1123  string                 `json:"-"`
	ProfitLoss       *float64               `json:"pl,omitempty"`
}

func (st *SignalTrade) String() string {
	stBytes, _ := json.Marshal(st)
	return string(stBytes)
}
