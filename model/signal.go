package model

import (
	"encoding/json"
	"time"

	"gofx/loggers"
	"gofx/model/instruments"
	"gofx/model/oanda"
)

//AutochartistSignal represents an autochartist signal
type AutochartistSignal struct {
	Base
	Type           string                 `json:"type"`
	InstrumentName string                 `json:"instrument"`
	Instrument     instruments.Instrument `json:"-"`
	Data           SignalData             `json:"data"`
	Meta           SignalMetaData         `json:"meta"`
}

//NewAutochartistSignal produces an AutochartistSignal db model from oanda model
func NewAutochartistSignal(inSignal *oanda.AutochartistSignal) *AutochartistSignal {
	//output object
	outSignal := &AutochartistSignal{}

	//transfer via JSON initially
	inSignalJSON, _ := json.MarshalIndent(inSignal, "", "  ")
	if err := json.Unmarshal(inSignalJSON, outSignal); err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not convert oanda signal to db gofx signal")
	}

	//transform fields
	outSignal.Instrument = instruments.ParseInstrument(inSignal.Instrument)

	pEndTime := time.Unix(outSignal.Data.PatternEndTimeUnix, 0).In(time.Local)
	outSignal.Data.PatternEndTime = &pEndTime

	if outSignal.Data.Prediction != nil && outSignal.Data.Prediction.TimeFromUnix > 0 {
		pFrom := time.Unix(outSignal.Data.Prediction.TimeFromUnix, 0).In(time.Local)
		outSignal.Data.Prediction.TimeFrom = &pFrom
	}
	if outSignal.Data.Prediction != nil && outSignal.Data.Prediction.TimeToUnix > 0 {
		pTo := time.Unix(outSignal.Data.Prediction.TimeToUnix, 0).In(time.Local)
		outSignal.Data.Prediction.TimeTo = &pTo
	}
	if outSignal.Data.Points.KeyTimeUnix != nil {
		outSignal.Data.Points.KeyTime = make(map[int]time.Time)
		for k := 1; k <= len(outSignal.Data.Points.KeyTimeUnix); k++ {
			if outSignal.Data.Points.KeyTimeUnix[k] > 0 {
				outSignal.Data.Points.KeyTime[k] = time.Unix(outSignal.Data.Points.KeyTimeUnix[k], 0).In(time.Local)
			}
		}
	}
	if outSignal.Data.Points.Support != nil {
		if outSignal.Data.Points.Support.X0Unix > 0 {
			sX0 := time.Unix(outSignal.Data.Points.Support.X0Unix, 0).In(time.Local)
			outSignal.Data.Points.Support.X0 = &sX0
		}
		if outSignal.Data.Points.Support.X1Unix > 0 {
			sX1 := time.Unix(outSignal.Data.Points.Support.X1Unix, 0).In(time.Local)
			outSignal.Data.Points.Support.X1 = &sX1
		}
	}
	if outSignal.Data.Points.Resistance != nil {
		if outSignal.Data.Points.Resistance.X0Unix > 0 {
			rX0 := time.Unix(outSignal.Data.Points.Resistance.X0Unix, 0).In(time.Local)
			outSignal.Data.Points.Resistance.X0 = &rX0
		}
		if outSignal.Data.Points.Resistance.X1Unix > 0 {
			rX1 := time.Unix(outSignal.Data.Points.Resistance.X1Unix, 0).In(time.Local)
			outSignal.Data.Points.Resistance.X1 = &rX1
		}
	}

	return outSignal
}

//SignalData struct
type SignalData struct {
	Prediction         *SignalPrediction `json:"prediction,omitempty"`
	Points             SignalPoints      `json:"points"`
	PatternEndTimeUnix int64             `json:"patternendtime"`
	PatternEndTime     *time.Time        `json:"-"`
	Price              float64           `json:"price,omitempty"`
}

//SignalPrediction struct
type SignalPrediction struct {
	PriceLow     *float64   `json:"pricelow"`
	PriceHigh    *float64   `json:"pricehigh"`
	TimeFromUnix int64      `json:"timefrom"`
	TimeFrom     *time.Time `json:"-"`
	TimeToUnix   int64      `json:"timeto"`
	TimeTo       *time.Time `json:"-"`
	TimeBars     int        `json:"timebars,omitempty"`
}

//SignalPoints struct
type SignalPoints struct {
	Support     *SignalPointsCoordinates `json:"support,omitempty"`
	Resistance  *SignalPointsCoordinates `json:"resistance,omitempty"`
	KeyTimeUnix map[int]int64            `json:"keytime,omitempty"`
	KeyTime     map[int]time.Time        `json:"-"`
}

//SignalPointsCoordinates struct
type SignalPointsCoordinates struct {
	Y1     float64    `json:"y1"`
	Y0     float64    `json:"y0"`
	X1Unix int64      `json:"x1"`
	X1     *time.Time `json:"-"`
	X0Unix int64      `json:"x0"`
	X0     *time.Time `json:"-"`
}

//SignalMetaData struct
type SignalMetaData struct {
	Scores          *MetaScores   `json:"scores"`
	PatternType     string        `json:"patterntype,omitempty"`
	Pattern         string        `json:"pattern"`
	Probability     float64       `json:"probability"`
	Interval        int           `json:"interval"`
	Direction       int           `json:"direction"`
	Length          int           `json:"length"`
	HistoricalStats MetaHistStats `json:"historicalstats"`
	TrendType       string        `json:"trendtype,omitempty"`
	Completed       int           `json:"completed"`
}

//MetaScores struct
type MetaScores struct {
	Clarity      *int `json:"clarity,omitempty"`
	InitialTrend *int `json:"initialtrend,omitempty"`
	Breakout     *int `json:"breakout,omitempty"`
	Quality      *int `json:"quality,omitempty"`
	Uniformity   *int `json:"uniformity,omitempty"`
}

//MetaHistStats struct
type MetaHistStats struct {
	Symbol    MetaHistStatData `json:"symbol"`
	Pattern   MetaHistStatData `json:"pattern"`
	HourOfDay MetaHistStatData `json:"hourofday"`
}

//MetaHistStatData struct
type MetaHistStatData struct {
	Correct int     `json:"correct"`
	Percent float64 `json:"percent"`
	Total   int     `json:"total"`
}
