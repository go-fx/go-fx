package client

import (
	"strconv"
	"time"

	gofxhttp "gofx/client/http"
	"gofx/client/http/model/req"
	"gofx/client/http/model/res"
	"gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model"
	"gofx/model/oanda"
)

func createSignalTradeFromMarketOrder(trade *model.SignalTrade, client interfaces.HTTPClient) (*model.SignalTrade, error) {
	//prepare request
	order := &req.MarketOrder{}
	order.Type = "MARKET"
	order.Instrument = trade.Instrument.Name()
	order.Units = strconv.Itoa(trade.Units)
	if trade.TakePrice != nil {
		order.TakeProfitOnFill = &req.TakeProfitOrder{
			Price: strconv.FormatFloat(*trade.TakePrice, 'f', -1, 64),
		}
	}
	if trade.StopPrice != nil {
		order.StopLossOnFill = &req.StopLossOrder{
			Price: strconv.FormatFloat(*trade.StopPrice, 'f', -1, 64),
		}
	}
	if trade.TSDistance != nil {
		order.TrailingStopLossOnFill = &req.TrailingStopLossOrder{
			Distance: strconv.FormatFloat(*trade.TSDistance, 'f', -1, 64),
		}
	}

	//send request
	res, err := gofxhttp.CreateMarketOrder(order, client)
	if err != nil {
		return nil, err
	}

	//update signal trade with response data
	trade.TradeID = res.OrderFillTransaction.TradeOpened.TradeID
	trade.StrategyName = ActiveStrategy.Name()
	trade.Units, _ = strconv.Atoi(res.OrderFillTransaction.TradeOpened.Units)
	trade.EntryPrice, _ = strconv.ParseFloat(res.OrderFillTransaction.TradeOpened.Price, 64)
	trade.EntryTime = *res.OrderFillTransaction.Time
	trade.EntryTimeRFC1123 = trade.EntryTime.Format(time.RFC1123)

	//create signal trade with signal in database
	if err := db.CreateSignalTradeWithSignal(trade, nil); err != nil {
		if err := db.CreateSignalTrade(trade, nil); err != nil {
			return nil, err
		}
	}

	//log
	ts := loggers.GetTimestamp()
	loggers.GetJSONLogger().WithField("ts", ts).WithField("openedTrade", trade).Tracef("Opened trade ID '%s' details", trade.TradeID)
	loggers.GetJSONLogger().WithField("ts", ts).Infof("BROKER UPDATE! ... Opened trade ID '%s' in db", trade.TradeID)

	//send email alert
	emailData := email.OpenTradeTemplateData{}
	emailData.AppName = env.GetConfig().AppName
	emailData.CompanyLink = env.GetConfig().AppCompanyLink
	emailData.CompanyName = env.GetConfig().AppCompany
	emailData.CopyrightYear = time.Now().Year()
	emailData.StrategyName = trade.StrategyName
	emailData.Trade = trade
	if emailData.Trade.Deadline != nil {
		emailData.Trade.DeadlineRFC1123 = trade.Deadline.Format(time.RFC1123)
	}

	//get account email data
	resAcct, err := gofxhttp.GetAccount(client)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not send open trade alert email")
		return trade, err
	}
	emailData.AccountTemplateData = *email.NewAccountTemplateData(resAcct)

	_, err = email.ActiveProvider.SendTXOpenTrade(env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, emailData, client, nil)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not send open trade alert email")
	}
	ts = loggers.GetTimestamp()
	loggers.GetJSONLogger().WithField("ts", ts).Infof("successfully sent open trade alert email for trade ID '%s", trade.TradeID)

	//success
	return trade, nil
}

func createUpdateDeleteTradeOrders(trade *model.SignalTrade, client interfaces.HTTPClient) (*res.PutUpdateTradeOrders, error) {
	//prepare request
	orders := &req.PutUpdateTradeOrders{}
	if trade.TakePrice != nil {
		orders.TakeProfit = &oanda.TakeProfitDetails{
			Price: strconv.FormatFloat(*trade.TakePrice, 'f', -1, 64),
		}
	}
	if trade.StopPrice != nil {
		orders.StopLoss = &oanda.StopLossDetails{
			Price: strconv.FormatFloat(*trade.StopPrice, 'f', -1, 64),
		}
	}
	if trade.TSDistance != nil {
		orders.TrailingStopLoss = &oanda.TrailingStopLossDetails{
			Distance: strconv.FormatFloat(*trade.TSDistance, 'f', -1, 64),
		}
	}

	//send request
	res, err := gofxhttp.UpdateTradeOrders(trade.TradeID, orders, client)
	if err != nil {
		return nil, err
	}

	//update trade with response data
	// if res.TakeProfitOrderTransaction != nil {
	// 	takePrice, _ := strconv.ParseFloat(res.TakeProfitOrderTransaction.Price, 64)
	// 	trade.TakePrice = &takePrice
	// }
	// if res.StopLossOrderTransaction != nil {
	// 	stopPrice, _ := strconv.ParseFloat(res.StopLossOrderTransaction.Price, 64)
	// 	trade.StopPrice = &stopPrice
	// }
	// if res.TrailingStopLossOrderTransaction != nil {
	// 	tsDistance, _ := strconv.ParseFloat(res.TrailingStopLossOrderTransaction.Distance, 64)
	// 	trade.TSDistance = &tsDistance
	// }

	//update trade in database
	// if err := db.UpdateSignalTrade(trade, nil); err != nil {
	// 	return nil, err
	// }

	//success
	return res, nil
}

func closeSignalTradeUnits(tradeSpecifier string, units string, client interfaces.HTTPClient) (*res.PutCloseTrade, error) {
	//prepare request
	closeReq := &req.PutCloseTrade{
		Units: units,
	}

	//send request
	res, err := gofxhttp.CloseTrade(tradeSpecifier, closeReq, client)
	if err != nil {
		return nil, err
	}

	//success
	return res, nil
}

func createLimitSignalOrder(order *model.SignalOrder, client interfaces.HTTPClient) (*model.SignalOrder, error) {
	//prepare request
	orderReq := &req.LimitOrder{}
	orderReq.Type = "LIMIT"
	orderReq.Instrument = order.Instrument.Name()
	orderReq.Units = strconv.Itoa(order.Units)
	if order.TakePrice != nil {
		orderReq.TakeProfitOnFill = &req.TakeProfitOrder{
			Price: strconv.FormatFloat(*order.TakePrice, 'f', -1, 64),
		}
	}
	if order.StopPrice != nil {
		orderReq.StopLossOnFill = &req.StopLossOrder{
			Price: strconv.FormatFloat(*order.StopPrice, 'f', -1, 64),
		}
	}
	if order.TSDistance != nil {
		orderReq.TrailingStopLossOnFill = &req.TrailingStopLossOrder{
			Distance: strconv.FormatFloat(*order.TSDistance, 'f', -1, 64),
		}
	}

	//send request
	res, err := gofxhttp.CreateLimitOrder(orderReq, client)
	if err != nil {
		return nil, err
	}

	//update order with response data
	order.CreatedAt = res.OrderCreateTransaction.Time
	order.OrderID = res.OrderCreateTransaction.ID

	//create order in database
	if err := db.CreateSignalOrder(order, nil); err != nil {
		if err := db.CreateSignalOrder(order, nil); err != nil {
			return nil, err
		}
	}

	//success
	return order, nil
}

func createStopSignalOrder(order *model.SignalOrder, client interfaces.HTTPClient) (*model.SignalOrder, error) {
	//prepare request
	orderReq := &req.StopOrder{}
	orderReq.Type = "STOP"
	orderReq.Instrument = order.Instrument.Name()
	orderReq.Units = strconv.Itoa(order.Units)
	if order.TakePrice != nil {
		orderReq.TakeProfitOnFill = &req.TakeProfitOrder{
			Price: strconv.FormatFloat(*order.TakePrice, 'f', -1, 64),
		}
	}
	if order.StopPrice != nil {
		orderReq.StopLossOnFill = &req.StopLossOrder{
			Price: strconv.FormatFloat(*order.StopPrice, 'f', -1, 64),
		}
	}
	if order.TSDistance != nil {
		orderReq.TrailingStopLossOnFill = &req.TrailingStopLossOrder{
			Distance: strconv.FormatFloat(*order.TSDistance, 'f', -1, 64),
		}
	}

	//send request
	res, err := gofxhttp.CreateStopOrder(orderReq, client)
	if err != nil {
		return nil, err
	}

	//update order with response data
	order.CreatedAt = res.OrderCreateTransaction.Time
	order.OrderID = res.OrderCreateTransaction.ID

	//create order in database
	if err := db.CreateSignalOrderWithSignal(order, nil); err != nil {
		if err := db.CreateSignalOrder(order, nil); err != nil {
			return nil, err
		}
	}

	//success
	return order, nil
}
