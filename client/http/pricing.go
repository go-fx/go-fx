package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/instruments"
)

//GetPricing retrieves pricing info for a list of instruments
func GetPricing(instruments []instruments.Instrument, includeHomeConv bool, since *time.Time, client interfaces.HTTPClient) (*res.GetPricing, error) {
	if instruments == nil || len(instruments) < 1 {
		return nil, fmt.Errorf("missing instruments")
	}

	urlParams := url.Values{}
	instrumentNames := make([]string, 0)
	for _, inst := range instruments {
		instrumentNames = append(instrumentNames, inst.Name())
	}
	urlParams.Set("instruments", strings.Join(instrumentNames, ","))

	if since != nil {
		urlParams.Set("since", since.Format(time.RFC3339Nano))
	}
	if includeHomeConv {
		urlParams.Set("includeHomeConversions", "true")
	}

	reqURL := fmt.Sprintf("%s?%s", strings.Replace(env.GetConfig().TradeAPIEndpointGetPricing, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), urlParams.Encode())

	pricingReq, err := http.NewRequest("GET", reqURL, nil)
	pricingReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	pricingReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(pricingReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve pricing")
		return nil, err
	}

	//parse response body
	var resPricing res.GetPricing
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resPricing); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get pricing response")
			return nil, err
		}

		if resPricing.Prices == nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithField("res", string(resJSON)).Error("pricing not received")
		}

		//parse times
		for _, price := range resPricing.Prices {
			price.Time, _ = time.Parse(time.RFC3339Nano, price.TimeRFC3339Nano)
			price.Time = price.Time.In(time.Local)
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve pricing")
		return nil, err
	}

	return &resPricing, nil
}
