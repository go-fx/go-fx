package res

import (
	"gofx/model/oanda"
)

//GetTrade is the response model for get trade details request
type GetTrade struct {
	Trade             *oanda.Trade `json:"trade"`
	LastTransactionID string       `json:"lastTransactionID"`
}

//GetTrades is the response model for list trade requests
type GetTrades struct {
	Trades            []*oanda.Trade `json:"trades"`
	LastTransactionID string         `json:"lastTransactionID"`
}

//PutCloseTrade is the response model for closing a trade
type PutCloseTrade struct {
	Units                  string                        `json:"units"`
	OrderCreateTransaction *oanda.MarketOrderTransaction `json:"orderCreateTransaction"`
	OrderFillTransaction   *oanda.OrderFillTransaction   `json:"orderFillTransaction"`
	OrderCancelTransaction *oanda.OrderCancelTransaction `json:"orderCancelTransaction"`
	RelatedTransactionIDs  []string                      `json:"relatedTransactionIDs"`
	LastTransactionID      string                        `json:"lastTransactionID"`
}

//PutUpdateTradeOrders is the response model for updating a trade's dependent orders
type PutUpdateTradeOrders struct {
	TakeProfitOrderCancelTransaction        *oanda.OrderCancelTransaction           `json:"takeProfitOrderCancelTransaction"`
	TakeProfitOrderTransaction              *oanda.TakeProfitOrderTransaction       `json:"takeProfitOrderTransaction"`
	TakeProfitOrderFillTransaction          *oanda.OrderFillTransaction             `json:"takeProfitOrderFillTransaction"`
	TakeProfitOrderCreatedCancelTransaction *oanda.OrderCancelTransaction           `json:"takeProfitOrderCreatedCancelTransaction"`
	StopLossOrderCancelTransaction          *oanda.OrderCancelTransaction           `json:"stopLossOrderCancelTransaction"`
	StopLossOrderTransaction                *oanda.StopLossOrderTransaction         `json:"stopLossOrderTransaction"`
	StopLossOrderFillTransaction            *oanda.OrderFillTransaction             `json:"stopLossOrderFillTransaction"`
	StopLossOrderCreatedCancelTransaction   *oanda.OrderCancelTransaction           `json:"stopLossOrderCreatedCancelTransaction"`
	TrailingStopLossOrderCancelTransaction  *oanda.OrderCancelTransaction           `json:"trailingStopLossOrderCancelTransaction"`
	TrailingStopLossOrderTransaction        *oanda.TrailingStopLossOrderTransaction `json:"trailingStopLossOrderTransaction"`
	RelatedTransactionIDs                   []string                                `json:"relatedTransactionIDs"`
	LastTransactionID                       string                                  `json:"lastTransactionID"`
}
