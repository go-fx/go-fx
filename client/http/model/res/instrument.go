package res

import (
	"gofx/model/oanda"
)

//GetInstruments is the response model for the get instruments request
type GetInstruments struct {
	Instruments       []*oanda.Instrument `json:"instruments"`
	LastTransactionID string              `json:"lastTransactionID"`
}
