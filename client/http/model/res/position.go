package res

import (
	"gofx/model/oanda"
)

//GetOpenPositions is the response model for getting open positions
type GetOpenPositions struct {
	Positions         []*oanda.Position `json:"positions"`
	LastTransactionID string            `json:"lastTransactionID"`
}

//PutClosePosition is the response model for put request to close position
type PutClosePosition struct {
	LongOrderCreateTransaction  *oanda.MarketOrderTransaction `json:"longOrderCreateTransaction"`
	LongOrderFillTransaction    *oanda.OrderFillTransaction   `json:"longOrderFillTransaction"`
	LongOrderCancelTransaction  *oanda.OrderCancelTransaction `json:"longOrderCancelTransaction"`
	ShortOrderCreateTransaction *oanda.MarketOrderTransaction `json:"shortOrderCreateTransaction"`
	ShortOrderFillTransaction   *oanda.OrderFillTransaction   `json:"shortOrderFillTransaction"`
	ShortOrderCancelTransaction *oanda.OrderCancelTransaction `json:"shortOrderCancelTransaction"`
	RelatedTransactionIDs       []string                      `json:"relatedTransactionIDs"`
	LastTransactionID           string                        `json:"lastTransactionID"`
}
