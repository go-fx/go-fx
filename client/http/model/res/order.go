package res

import (
	"gofx/model/oanda"
)

//PostOrder is the response body from an order post request
type PostOrder struct {
	OrderCreateTransaction        *oanda.Transaction            `json:"orderCreateTransaction"`
	OrderFillTransaction          *oanda.OrderFillTransaction   `json:"orderFillTransaction"`
	OrderCancelTransaction        *oanda.OrderCancelTransaction `json:"orderCancelTransaction"`
	OrderReissueTransaction       *oanda.Transaction            `json:"orderReissueTransaction"`
	OrderReissueRejectTransaction *oanda.Transaction            `json:"orderReissueRejectTransaction"`
	RelatedTransactionIDs         []string                      `json:"relatedTransactionIDs"`
	LastTransactionID             string                        `json:"lastTransactionID"`
}

//PutUpdateOrder is the response body from an order put update request
type PutUpdateOrder struct {
	OrderCreateTransaction          *oanda.Transaction            `json:"orderCreateTransaction"`
	OrderFillTransaction            *oanda.OrderFillTransaction   `json:"orderFillTransaction"`
	OrderCancelTransaction          *oanda.OrderCancelTransaction `json:"orderCancelTransaction"`
	OrderReissueTransaction         *oanda.Transaction            `json:"orderReissueTransaction"`
	OrderReissueRejectTransaction   *oanda.Transaction            `json:"orderReissueRejectTransaction"`
	ReplacingOrderCancelTransaction *oanda.OrderCancelTransaction `json:"replacingOrderCancelTransaction"`
	RelatedTransactionIDs           []string                      `json:"relatedTransactionIDs"`
	LastTransactionID               string                        `json:"lastTransactionID"`
}

//PutCancelOrder is the response body from an order put cancel request
type PutCancelOrder struct {
	OrderCancelTransaction *oanda.OrderCancelTransaction `json:"orderCancelTransaction"`
	RelatedTransactionIDs  []string                      `json:"relatedTransactionIDs"`
	LastTransactionID      string                        `json:"lastTransactionID"`
}

//GetOrders is the response body from a get orders request
type GetOrders struct {
	Orders            []*oanda.AccountOrder `json:"orders"`
	LastTransactionID string                `json:"lastTransactionID"`
}
