package req

//PostPutOrder is the request body for order post or put requests
type PostPutOrder struct {
	Order interface{} `json:"order"`
}

//GetOrder is the request body for get order requests
type GetOrder struct {
	Order             *Order `json:"order"`
	LastTransactionID string `json:"lastTransactionID"`
}

//GetOrders is the request body for list orders requests
type GetOrders struct {
	Order             []*Order `json:"orders"`
	LastTransactionID string   `json:"lastTransactionID"`
}

//Order is the common request model orders
type Order struct {
	Type                   string                 `json:"type"`
	Instrument             string                 `json:"instrument,omitempty"`
	Units                  string                 `json:"units,omitempty"`
	TimeInForceName        string                 `json:"timeInForce,omitempty"`
	PositionFillName       string                 `json:"positionFill,omitempty"`
	ClientExtensions       map[string]string      `json:"clientExtensions,omitempty"`
	TakeProfitOnFill       *TakeProfitOrder       `json:"takeProfitOnFill,omitempty"`
	StopLossOnFill         *StopLossOrder         `json:"stopLossOnFill,omitempty"`
	TrailingStopLossOnFill *TrailingStopLossOrder `json:"trailingStopLossOnFill,omitempty"`
	TradeClientExtensions  map[string]string      `json:"tradeClientExtensions,omitempty"`
}

//MarketOrder is the request model for create market order
type MarketOrder struct {
	Order
	PriceBound string `json:"priceBound,omitempty"`
}

//LimitOrder is the request model for create limit order
type LimitOrder struct {
	Order
	Price              string `json:"price"`
	GTDTimeRFC3339Nano string `json:"gtdTime,omitempty"`
	TriggerCondition   string `json:"triggerCondition,omitempty"`
}

//StopOrder is the request model for create stop order
type StopOrder struct {
	Order
	Price              string `json:"price"`
	PriceBound         string `json:"priceBound,omitempty"`
	GTDTimeRFC3339Nano string `json:"gtdTime,omitempty"`
	TriggerCondition   string `json:"triggerCondition,omitempty"`
}

//TakeProfitOrder is the request model for create tp order
type TakeProfitOrder struct {
	Order
	TradeID            string `json:"tradeID,omitempty"`
	ClientTradeID      string `json:"clientTradeID,omitempty"`
	Price              string `json:"price"`
	PriceBound         string `json:"priceBound,omitempty"`
	GTDTimeRFC3339Nano string `json:"gtdTime,omitempty"`
	TriggerCondition   string `json:"triggerCondition,omitempty"`
}

//StopLossOrder is the request model for create sl order
type StopLossOrder struct {
	Order
	TradeID            string `json:"tradeID,omitempty"`
	ClientTradeID      string `json:"clientTradeID,omitempty"`
	Price              string `json:"price,omitempty"`
	Distance           string `json:"distance,omitempty"`
	GTDTimeRFC3339Nano string `json:"gtdTime,omitempty"`
	TriggerCondition   string `json:"triggerCondition,omitempty"`
}

//TrailingStopLossOrder is the request model for create tsl order
type TrailingStopLossOrder struct {
	Order
	TradeID            string `json:"tradeID,omitempty"`
	ClientTradeID      string `json:"clientTradeID,omitempty"`
	Distance           string `json:"distance"`
	GTDTimeRFC3339Nano string `json:"gtdTime,omitempty"`
	TriggerCondition   string `json:"triggerCondition,omitempty"`
}
