package req

import (
	"gofx/model/oanda"
)

//PutCloseTrade is the request model for closing a trade
type PutCloseTrade struct {
	Units string `json:"units"`
}

//PutUpdateTradeOrders is the request model for updating a trade's dependent orders (WILL MOD IF EMPTY)
type PutUpdateTradeOrders struct {
	TakeProfit       *oanda.TakeProfitDetails       `json:"takeProfit"`
	StopLoss         *oanda.StopLossDetails         `json:"stopLoss"`
	TrailingStopLoss *oanda.TrailingStopLossDetails `json:"trailingStopLoss"`
}
