package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/oanda"
)

//GetAccount retrieves full account details
func GetAccount(client interfaces.HTTPClient) (*oanda.Account, error) {
	acctReq, err := http.NewRequest("GET", strings.Replace(env.GetConfig().TradeAPIEndpointGetAccount, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), nil)
	acctReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	acctReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(acctReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve account")
		return nil, err
	}

	//parse response body
	var resAccount res.GetAccount
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resAccount); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get account response")
			return nil, err
		}

		if resAccount.Account == nil {
			err := fmt.Errorf("account not received")
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).WithField("res", string(resJSON)).Error(err.Error())
			return nil, err
		}

		//parse account times
		resAccount.Account.CreatedTime, _ = time.Parse(time.RFC3339Nano, resAccount.Account.CreatedTimeRFC3339Nano)
		resAccount.Account.CreatedTime = resAccount.Account.CreatedTime.In(time.Local)
		resAccount.Account.ResettablePLTime, _ = time.Parse(time.RFC3339Nano, resAccount.Account.ResettablePLTimeRFC3339Nano)
		resAccount.Account.ResettablePLTime = resAccount.Account.ResettablePLTime.In(time.Local)
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve account")
		return nil, err
	}

	return resAccount.Account, nil
}
