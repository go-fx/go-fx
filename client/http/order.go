package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gofx/client/http/model/req"
	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/oanda"
)

//CreateMarketOrder creates a new market order
func CreateMarketOrder(order *req.MarketOrder, client interfaces.HTTPClient) (*res.PostOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	resOrder, err := createOrder(reqBodyReader, client)
	if err != nil {
		return resOrder, err
	}
	if resOrder.OrderFillTransaction == nil {
		orderBytes, _ := json.Marshal(order)
		resBytes, _ := json.Marshal(resOrder)
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithField("failedOrder", string(orderBytes)).WithField("response", string(resBytes)).Error("market order not filled")
		return nil, fmt.Errorf("market order created but not filled")
	}

	//parse times
	orderFillTime, _ := time.Parse(time.RFC3339Nano, resOrder.OrderFillTransaction.TimeRFC3339Nano)
	orderFillTime = orderFillTime.In(time.Local)
	resOrder.OrderFillTransaction.Time = &orderFillTime

	return resOrder, nil
}

//CreateLimitOrder creates a new limit order
func CreateLimitOrder(order *req.LimitOrder, client interfaces.HTTPClient) (*res.PostOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return createOrder(reqBodyReader, client)
}

//CreateStopOrder creates a new stop order
func CreateStopOrder(order *req.StopOrder, client interfaces.HTTPClient) (*res.PostOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return createOrder(reqBodyReader, client)
}

//CreateTakeProfitOrder creates a new tp order
func CreateTakeProfitOrder(order *req.TakeProfitOrder, client interfaces.HTTPClient) (*res.PostOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return createOrder(reqBodyReader, client)
}

//CreateStopLossOrder creates a new sl order
func CreateStopLossOrder(order *req.StopLossOrder, client interfaces.HTTPClient) (*res.PostOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return createOrder(reqBodyReader, client)
}

//createOrder creates a new order
func createOrder(reqBodyReader *strings.Reader, client interfaces.HTTPClient) (*res.PostOrder, error) {
	orderReq, err := http.NewRequest("POST", strings.Replace(env.GetConfig().TradeAPIEndpointCreateOrder, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), reqBodyReader)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not create order")
		return nil, err
	}

	//parse response body
	resJSON, err := ioutil.ReadAll(response.Body)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not create order")
		return nil, err
	}

	var resOrder res.PostOrder
	if err := json.Unmarshal(resJSON, &resOrder); err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse create order response")
		return nil, err
	}

	if resOrder.OrderCreateTransaction == nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithField("res", string(resJSON)).Error("order not created")
		return &resOrder, fmt.Errorf("order not created")
	}

	//parse time
	createTime, _ := time.Parse(time.RFC3339Nano, resOrder.OrderCreateTransaction.TimeRFC3339Nano)
	createTime = createTime.In(time.Local)
	resOrder.OrderCreateTransaction.Time = &createTime

	if resOrder.OrderFillTransaction != nil {
		//parse time
		fillTime, _ := time.Parse(time.RFC3339Nano, resOrder.OrderFillTransaction.TimeRFC3339Nano)
		fillTime = fillTime.In(time.Local)
		resOrder.OrderFillTransaction.Time = &fillTime
	}

	return &resOrder, nil
}

//UpdateMarketOrder updates an existing market order
func UpdateMarketOrder(orderSpecifier string, order *req.MarketOrder, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return updateOrder(orderSpecifier, reqBodyReader, client)
}

//UpdateLimitOrder updates an existing limit order
func UpdateLimitOrder(orderSpecifier string, order *req.LimitOrder, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return updateOrder(orderSpecifier, reqBodyReader, client)
}

//UpdateStopOrder updates an existing stop order
func UpdateStopOrder(orderSpecifier string, order *req.StopOrder, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return updateOrder(orderSpecifier, reqBodyReader, client)
}

//UpdateTakeProfitOrder updates an existing tp order
func UpdateTakeProfitOrder(orderSpecifier string, order *req.TakeProfitOrder, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return updateOrder(orderSpecifier, reqBodyReader, client)
}

//UpdateStopLossOrder updates an existing sl order
func UpdateStopLossOrder(orderSpecifier string, order *req.StopLossOrder, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	//prepare req body
	reqBodyBytes, _ := json.Marshal(req.PostPutOrder{Order: order})
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	return updateOrder(orderSpecifier, reqBodyReader, client)
}

//updateOrder creates a new order
func updateOrder(orderSpecifier string, reqBodyReader *strings.Reader, client interfaces.HTTPClient) (*res.PutUpdateOrder, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointUpdateOrder, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{orderSpecifier}", orderSpecifier, 1)

	orderReq, err := http.NewRequest("PUT", reqURL, reqBodyReader)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not update order")
		return nil, err
	}

	//parse response body
	var resUpdateOrder res.PutUpdateOrder
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resUpdateOrder); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse update order response")
			return nil, err
		}

		if resUpdateOrder.OrderCreateTransaction == nil {
			err := fmt.Errorf("order not updated")
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).WithField("res", string(resJSON)).Error(err.Error())
			return nil, err
		}

		//parse times
		orderCreateTime, _ := time.Parse(time.RFC3339Nano, resUpdateOrder.OrderCreateTransaction.TimeRFC3339Nano)
		orderCreateTime = orderCreateTime.In(time.Local)
		resUpdateOrder.OrderCreateTransaction.Time = &orderCreateTime
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not update order")
		return nil, err
	}

	return &resUpdateOrder, nil
}

//CancelOrder cancels an existing order
func CancelOrder(orderSpecifier string, client interfaces.HTTPClient) (*res.PutCancelOrder, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointCancelOrder, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{orderSpecifier}", orderSpecifier, 1)

	orderReq, err := http.NewRequest("PUT", reqURL, nil)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not cancel order")
		return nil, err
	}

	//parse response body
	var resCancelOrder res.PutCancelOrder
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resCancelOrder); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse cancel order response")
			return nil, err
		}

		if resCancelOrder.OrderCancelTransaction == nil {
			err := fmt.Errorf("order not cancelled")
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).WithField("res", string(resJSON)).Error(err.Error())
			return nil, err
		}

		//parse times
		timeCancelled, _ := time.Parse(time.RFC3339Nano, resCancelOrder.OrderCancelTransaction.TimeRFC3339Nano)
		timeCancelled = timeCancelled.In(time.Local)
		resCancelOrder.OrderCancelTransaction.Time = &timeCancelled
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not cancel order")
		return nil, err
	}

	return &resCancelOrder, nil
}

//GetOrders retrieves orders for account
func GetOrders(params url.Values, client interfaces.HTTPClient) ([]*oanda.AccountOrder, error) {
	//prepare url
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointGetOrders, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = fmt.Sprintf("%s?%s", reqURL, params.Encode())

	//prepare request
	orderReq, err := http.NewRequest("GET", reqURL, nil)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve orders")
		return nil, err
	}

	//parse response body
	var resOrders res.GetOrders
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resOrders); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get orders response")
			return nil, err
		}

		if resOrders.Orders == nil {
			err := fmt.Errorf("orders not received")
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).WithField("res", string(resJSON)).Error(err.Error())
			return nil, err
		}

		//parse times
		for _, order := range resOrders.Orders {
			if order.FilledTimeRFC3339Nano != nil {
				orderFillTime, _ := time.Parse(time.RFC3339Nano, *order.FilledTimeRFC3339Nano)
				orderFillTime = orderFillTime.In(time.Local)
				order.FilledTime = &orderFillTime
			}
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve orders")
		return nil, err
	}

	return resOrders.Orders, nil
}
