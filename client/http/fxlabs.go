package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"

	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/oanda"
)

//GetAutochartistSignals retrieves signals from Oanda provided by Autochartist
func GetAutochartistSignals(params []url.Values, client interfaces.HTTPClient) []*oanda.AutochartistSignal {
	//final results channel
	resultsChan := make(chan []*oanda.AutochartistSignal)

	//response retriever for concurrent requests
	responseChan := make(chan []*oanda.AutochartistSignal)
	//collect signals (listener)
	go func() {
		signals := make([]*oanda.AutochartistSignal, 0)
		for i := 0; i < len(params); i++ {
			signals = append(signals, (<-responseChan)...)
		}
		resultsChan <- signals
	}()

	//spawn go routines to retrieve chart patterns and key levels
	wg := &sync.WaitGroup{}
	wg.Add(len(params))
	for idx, urlParams := range params {
		go func(reqNum int, queryParams url.Values) {
			signalReq, err := http.NewRequest("GET", fmt.Sprintf("%s?%s", env.GetConfig().TradeAPIEndpointGetSignals, queryParams.Encode()), nil)
			signalReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
			signalReq.Header.Add("Content-Type", "application/json")

			//send request
			response, err := client.Do(signalReq)
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithField("params", queryParams).WithError(err).Error("could not retrieve signal")
				wg.Done()
				return
			}

			//parse response body
			var resSignals res.GetAutoChartistSignals
			if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
				if err := json.Unmarshal(resJSON, &resSignals); err != nil {
					ts := loggers.GetTimestamp()
					loggers.GetJSONLogger().WithField("ts", ts).WithField("params", queryParams).WithError(err).Error("could not retrieve signal")
					wg.Done()
					return
				}
			} else {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithField("params", queryParams).WithError(err).Error("could not retrieve signal")
				wg.Done()
				return
			}

			responseChan <- resSignals.Signals
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Tracef("signal request %d complete", reqNum)
			wg.Done()
		}(idx+1, urlParams)
	}

	wg.Wait()

	close(responseChan)

	return <-resultsChan
}
