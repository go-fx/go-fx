package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gofx/client/http/model/req"
	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/instruments"
	"gofx/model/oanda"
)

//GetOpenPositions retrieves all open trades for account
func GetOpenPositions(client interfaces.HTTPClient) ([]*oanda.Position, error) {
	posReq, err := http.NewRequest("GET", strings.Replace(env.GetConfig().TradeAPIEndpointGetOpenPositions, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), nil)
	posReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	posReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(posReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve open positions")
		return nil, err
	}

	//parse response body
	var resPositions res.GetOpenPositions
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resPositions); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get open positions response")
			return nil, err
		}

		if resPositions.Positions == nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithField("res", string(resJSON)).Error("open positions not received")
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve open positions")
		return nil, err
	}

	return resPositions.Positions, nil
}

//ClosePosition closes units of an open position
func ClosePosition(instrument instruments.Instrument, closeUnitsReq *req.PutClosePosition, client interfaces.HTTPClient) (*res.PutClosePosition, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointClosePosition, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{instrument}", instrument.Name(), 1)

	//prepare req body
	reqBodyBytes, _ := json.Marshal(closeUnitsReq)
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	orderReq, err := http.NewRequest("PUT", reqURL, reqBodyReader)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close position units")
		return nil, err
	}

	//parse response body
	var resPositionClose res.PutClosePosition
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resPositionClose); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse close position response")
			return nil, err
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close position units")
		return nil, err
	}

	return &resPositionClose, nil
}
