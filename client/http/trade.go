package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gofx/client/http/model/req"
	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/oanda"
)

//GetOpenTrades retrieves all open trades for account
func GetOpenTrades(client interfaces.HTTPClient) ([]*oanda.Trade, error) {
	orderReq, err := http.NewRequest("GET", strings.Replace(env.GetConfig().TradeAPIEndpointGetOpenTrades, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), nil)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve open trades")
		return nil, err
	}

	//parse response body
	var resTrades res.GetTrades
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resTrades); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get open trades response")
			return nil, err
		}

		if resTrades.Trades == nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithField("res", string(resJSON)).Error("open trades not received")
		}

		//parse times
		for _, trade := range resTrades.Trades {
			trade.OpenTime, _ = time.Parse(time.RFC3339Nano, trade.OpenTimeRFC3339Nano)
			trade.OpenTime = trade.OpenTime.In(time.Local)
			if trade.CloseTimeRFC3339Nano != nil {
				closeTime, _ := time.Parse(time.RFC3339Nano, *trade.CloseTimeRFC3339Nano)
				closeTime = closeTime.In(time.Local)
				trade.CloseTime = &closeTime
			}
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve open trades")
		return nil, err
	}

	return resTrades.Trades, nil
}

//GetTradeByID retrieves a trade by ID
func GetTradeByID(tradeSpecifier string, client interfaces.HTTPClient) (*res.GetTrade, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointGetTradeByID, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{tradeSpecifier}", tradeSpecifier, 1)

	tradeReq, err := http.NewRequest("GET", reqURL, nil)
	tradeReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	tradeReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(tradeReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get trade")
		return nil, err
	}

	//parse response body
	var resTrade res.GetTrade
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resTrade); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get trade response")
			return nil, err
		}

		if resTrade.Trade == nil {
			err := fmt.Errorf("trade not found in broker w/ID: %s", tradeSpecifier)
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error(err.Error())
			return nil, err
		}

		//parse times
		resTrade.Trade.OpenTime, _ = time.Parse(time.RFC3339Nano, resTrade.Trade.OpenTimeRFC3339Nano)
		resTrade.Trade.OpenTime = resTrade.Trade.OpenTime.In(time.Local)
		if resTrade.Trade.CloseTimeRFC3339Nano != nil {
			closeTime, _ := time.Parse(time.RFC3339Nano, *resTrade.Trade.CloseTimeRFC3339Nano)
			closeTime = closeTime.In(time.Local)
			resTrade.Trade.CloseTime = &closeTime
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get trade")
		return nil, err
	}

	return &resTrade, nil
}

//UpdateTradeOrders updates trade order(s)
func UpdateTradeOrders(tradeSpecifier string, updateTradeOrderReq *req.PutUpdateTradeOrders, client interfaces.HTTPClient) (*res.PutUpdateTradeOrders, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointUpdateTradeOrders, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{tradeSpecifier}", tradeSpecifier, 1)

	//prepare req body
	reqBodyBytes, _ := json.Marshal(updateTradeOrderReq)
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	orderReq, err := http.NewRequest("PUT", reqURL, reqBodyReader)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not update trade orders")
		return nil, err
	}

	//parse response body
	var resTradeOrders res.PutUpdateTradeOrders
	var tradeOrderErr error
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resTradeOrders); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse update trade orders response")
			return nil, err
		}

		// -- validate successful updates --
		if updateTradeOrderReq.TakeProfit != nil && resTradeOrders.TakeProfitOrderTransaction == nil {
			tradeOrderErr = fmt.Errorf("could not update take profit order for trade ID %s", tradeSpecifier)
		}

		if updateTradeOrderReq.StopLoss != nil && resTradeOrders.StopLossOrderTransaction == nil {
			tradeOrderErr = fmt.Errorf("%w :: could not update stop loss order for trade ID %s", tradeOrderErr, tradeSpecifier)
		}

		if updateTradeOrderReq.TrailingStopLoss != nil && resTradeOrders.TrailingStopLossOrderTransaction == nil {
			tradeOrderErr = fmt.Errorf("%w :: could not update trailing stop order for trade ID %s", tradeOrderErr, tradeSpecifier)
		}

		if tradeOrderErr != nil {
			tradeOrderErr = fmt.Errorf("%w :: %s", tradeOrderErr, string(resJSON))
			return nil, tradeOrderErr
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not update trade orders")
		return nil, err
	}

	return &resTradeOrders, nil
}

//CloseTrade closes units of an open trade
func CloseTrade(tradeSpecifier string, closeUnitsReq *req.PutCloseTrade, client interfaces.HTTPClient) (*res.PutCloseTrade, error) {
	reqURL := strings.Replace(env.GetConfig().TradeAPIEndpointCloseTrade, "{accountID}", env.GetConfig().TradeAPIAccountID, 1)
	reqURL = strings.Replace(reqURL, "{tradeSpecifier}", tradeSpecifier, 1)

	//prepare req body
	reqBodyBytes, _ := json.Marshal(closeUnitsReq)
	reqBodyReader := strings.NewReader(string(reqBodyBytes))

	orderReq, err := http.NewRequest("PUT", reqURL, reqBodyReader)
	orderReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	orderReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(orderReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close trade units")
		return nil, err
	}

	//parse response body
	var resTradeClose res.PutCloseTrade
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resTradeClose); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse close trade response")
			return nil, err
		}

		if resTradeClose.OrderFillTransaction == nil {
			return nil, fmt.Errorf("could not fill close trade order for trade ID %s", tradeSpecifier)
		}

		//parse times
		closeTime, _ := time.Parse(time.RFC3339Nano, resTradeClose.OrderFillTransaction.TimeRFC3339Nano)
		closeTime = closeTime.In(time.Local)
		resTradeClose.OrderFillTransaction.Time = &closeTime
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close trade units")
		return nil, err
	}

	return &resTradeClose, nil
}
