package client

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/loggers"
	"gofx/strategy/basic"
)

//DisplayTitle shows the ASCII art title
func DisplayTitle() {
	fmt.Println(`
	 
       /$$$$$$                  /$$$$$$$$ /$$   /$$
      /$$__  $$                | $$_____/| $$  / $$
     | $$  \__/  /$$$$$$       | $$      |  $$/ $$/
     | $$ /$$$$ /$$__  $$      | $$$$$    \  $$$$/ 
     | $$|_  $$| $$  \ $$      | $$__/     >$$  $$ 
     | $$  \ $$| $$  | $$      | $$       /$$/\  $$
     |  $$$$$$/|  $$$$$$/      | $$      | $$  \ $$
      \______/  \______/       |__/      |__/  |__/
	
	  
      © GoFX.Live 2020
	
	`)
}

//StartupTasks executes all tasks to perfor before server starts listening for requests
func StartupTasks() {
	logger.Println("App Startup")
	logger.Println("===========")
	logger.Printf("TZ: %s\n", time.Local)
	logger.Printf("ENV: %s\n\n", env.GetConfig().ServerEnv.Name())
	fmt.Printf(">> Running startup tasks...\n\n")

	// -- define all possible tasks
	task1 := dbConnect
	task2 := dbCreateSchemaTables
	task3 := dbScripts
	task4 := initProviders
	task5 := initLogger
	task6 := initStrategy

	// -- load required tasks --
	tasks := []*func(int, int){
		&task1,
		&task2,
		&task4,
		&task5,
		&task6,
	}

	// -- schedule optional tasks
	if len(env.GetConfig().DBScripts) > 0 {
		//schedule task 3rd
		tasks = append(tasks, nil)
		copy(tasks[2:], append([]*func(int, int){&task3}, tasks[2:]...))
	}

	// -- run tasks --
	for i, task := range tasks {
		switch task {
		case nil:
			continue
		default:
			(*task)(i+1, len(tasks))
		}
	}

	//success
	logger.Printf("Startup tasks complete! <<\n\n")
}

// -- DB -- connecting or creating db if not exists
func dbConnect(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- connecting or creating db if not exists...\n", currentTask, totalTasks)
	if err := db.CreateSchema(nil); err == nil {
		logger.Printf("...SUCCESS!\n\n")
	} else {
		logger.Println("...FAIL!")
		panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
	}
}

// -- DB -- creating tables if not exists
func dbCreateSchemaTables(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- creating tables if not exist...", currentTask, totalTasks)
	if _, err := db.ExecScript("db", "sql", "create_tables.sql"); err == nil {
		logger.Printf("...SUCCESS!\n\n")
	} else {
		logger.Println("...FAIL!")
		panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
	}
}

// -- DB -- run startup scripts
func dbScripts(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) DB -- run startup scripts...", currentTask, totalTasks)
	csep := regexp.MustCompile(`[\s+,]+`)
	scripts := csep.Split(env.GetConfig().DBScripts, -1)
	for _, scriptName := range scripts {
		if len(scriptName) == 0 {
			continue
		}
		if _, err := db.ExecScript("db", "sql", scriptName); err != nil {
			logger.Println("...FAIL!")
			panic(fmt.Sprintf("Startup task %d failed! >> %s", currentTask, err))
		}
	}
	logger.Printf("...SUCCESS!\n\n")
}

// -- Providers -- initialize providers
func initProviders(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) WS -- initialize service providers...", currentTask, totalTasks)

	//email
	switch env.GetConfig().ProviderEmailName {
	case "sendgrid":
		email.ActiveProvider = email.SendGrid{
			ProviderEmailAuthHeader:     env.GetConfig().ProviderEmailAuthHeader,
			ProviderEmailEndpointSendTX: env.GetConfig().ProviderEmailEndpointSendTX,
		}
	}

	logger.Printf("...SUCCESS!\n\n")
}

// -- Logger -- initialize logger
func initLogger(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) Logger -- initialize logger...", currentTask, totalTasks)

	//logrus JSON logger
	loggers.InitLoggers(env.GetConfig().ServerLogLevel)

	logger.Printf("...SUCCESS!\n\n")
}

// -- Strategy -- initialize strategy from env name
func initStrategy(currentTask, totalTasks int) {
	logger.Printf("(%d of %d) Strategy -- initialize strategy...", currentTask, totalTasks)

	//choose active strategy from env value
	switch strings.ToLower(env.GetConfig().StrategyName) {
	default:
		ActiveStrategy = &basic.Basic{}
	}

	//set strategy alias if defined
	if len(env.GetConfig().StrategyAlias) > 0 {
		ActiveStrategy.SetName(env.GetConfig().StrategyAlias)
	}

	//init strategy
	ActiveStrategy.Init()

	logger.Printf("...SUCCESS!\n\n")
}
