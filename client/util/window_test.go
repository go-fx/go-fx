package util_test

import (
	"testing"
	"time"

	"gofx/client/util"
	"gofx/env"
)

var getCurrentTradingWindowTests = []struct {
	current              time.Time
	expectedOpen         time.Time
	expectedClose        time.Time
	expectedWithinWindow bool
}{
	{
		current:              time.Date(2020, time.Month(4), 30, 18, 30, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 1, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: true,
	},
	{
		current:              time.Date(2020, time.Month(5), 1, 13, 30, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 1, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: true,
	},
	{
		current:              time.Date(2020, time.Month(5), 1, 18, 30, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 8, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: false,
	},
	{
		current:              time.Date(2020, time.Month(5), 2, 12, 0, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 8, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: false,
	},
	{
		current:              time.Date(2020, time.Month(5), 3, 12, 0, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 8, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: false,
	},
	{
		current:              time.Date(2020, time.Month(5), 3, 18, 0, 0, 1, time.Local),
		expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 8, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: true,
	},
	{
		current:              time.Date(2020, time.Month(5), 4, 9, 0, 0, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(5), 8, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: true,
	},
	{
		current:              time.Date(2020, time.Month(6), 26, 18, 24, 2, 0, time.Local),
		expectedOpen:         time.Date(2020, time.Month(6), 28, 18, 0, 0, 0, time.Local),
		expectedClose:        time.Date(2020, time.Month(7), 3, 17, 59, 59, 0, time.Local),
		expectedWithinWindow: false,
	},
}

func TestGetCurrentTradingWindow(t *testing.T) {
	//set env for testing
	env.DefaultConfig.TradeWindowOpenDay = 0
	env.DefaultConfig.TradeWindowOpenHour = 18
	env.DefaultConfig.TradeWindowCloseDay = 5
	env.DefaultConfig.TradeWindowCloseHour = 18
	env.Configure()

	for idx, tt := range getCurrentTradingWindowTests {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentTradingWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentTradingWindow(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentTradingWindow(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinWindow {
			t.Errorf("TestGetCurrentTradingWindow(%d): expected within window %v, actual %v", idx, tt.expectedWithinWindow, actualWithinWindow)
		}
	}
}

func getCurrentNormalizedTradingWindowBothOutOfBoundsTests() []struct {
	current              time.Time
	expectedOpen         time.Time
	expectedClose        time.Time
	expectedWithinWindow bool
} {
	tzLA, _ := time.LoadLocation("America/Los_Angeles")
	tzNY, _ := time.LoadLocation("America/New_York")
	// tzBG, _ := time.LoadLocation("America/Lima")

	return []struct {
		current              time.Time
		expectedOpen         time.Time
		expectedClose        time.Time
		expectedWithinWindow bool
	}{
		{
			current:              time.Date(2020, time.Month(4), 30, 18, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(4), 26, env.GetMarketHours().MarketHoursOpenHour-3, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 1, env.GetMarketHours().MarketHoursCloseHour-4, 59, 59, 0, tzLA),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(4), 26, 14, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(4), 26, env.GetMarketHours().MarketHoursOpenHour-3, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 1, env.GetMarketHours().MarketHoursCloseHour-4, 59, 59, 0, tzLA),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(5), 3, 11, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour-3, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursCloseHour-4, 59, 59, 0, tzLA),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 17, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 20, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 2, 2, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 3, 17, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, tzNY),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(5), 17, 0, 35, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 17, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 22, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
	}
}

func TestGetCurrentNormalizedTradingWindowBothOutOfBounds(t *testing.T) {
	//set env for testing
	env.DefaultConfig.TradeWindowOpenDay = 0
	env.DefaultConfig.TradeWindowOpenHour = 11
	env.DefaultConfig.TradeWindowCloseDay = 5
	env.DefaultConfig.TradeWindowCloseHour = 23
	env.Configure()

	for idx, tt := range getCurrentNormalizedTradingWindowBothOutOfBoundsTests() {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentNormalizedTradingWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinWindow {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected within window %v, actual %v", idx, tt.expectedWithinWindow, actualWithinWindow)
		}
	}
}

func TestGetCurrentNormalizedTradingWindowBothOutOfBoundsWeekend(t *testing.T) {
	//set env for testing
	env.DefaultConfig.TradeWindowOpenDay = 5
	env.DefaultConfig.TradeWindowOpenHour = 23
	env.DefaultConfig.TradeWindowCloseDay = 5
	env.DefaultConfig.TradeWindowCloseHour = 22
	env.Configure()

	for idx, tt := range getCurrentNormalizedTradingWindowBothOutOfBoundsTests() {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentNormalizedTradingWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinWindow {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected within window %v, actual %v", idx, tt.expectedWithinWindow, actualWithinWindow)
		}
	}
}

func getCurrentNormalizedTradingWindowBothInBoundsTests() []struct {
	current              time.Time
	expectedOpen         time.Time
	expectedClose        time.Time
	expectedWithinWindow bool
} {
	tzLA, _ := time.LoadLocation("America/Los_Angeles")
	tzNY, _ := time.LoadLocation("America/New_York")
	// tzBG, _ := time.LoadLocation("America/Lima")

	return []struct {
		current              time.Time
		expectedOpen         time.Time
		expectedClose        time.Time
		expectedWithinWindow bool
	}{
		{
			current:              time.Date(2020, time.Month(4), 30, 18, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 1, 12, 59, 59, 0, tzLA),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(4), 26, 14, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 1, 12, 59, 59, 0, tzLA),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(4), 26, 18, 30, 0, 0, tzLA),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzLA),
			expectedClose:        time.Date(2020, time.Month(5), 1, 12, 59, 59, 0, tzLA),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 17, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, 12, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 2, 2, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, 12, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 3, 17, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, 12, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 20, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, 12, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(6), 26, 16, 24, 2, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(6), 28, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(7), 3, 12, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
	}
}

func TestGetCurrentNormalizedTradingWindowBothInBounds(t *testing.T) {
	//set env for testing
	env.DefaultConfig.TradeWindowOpenDay = 0
	env.DefaultConfig.TradeWindowOpenHour = 18
	env.DefaultConfig.TradeWindowCloseDay = 5
	env.DefaultConfig.TradeWindowCloseHour = 13
	env.Configure()

	for idx, tt := range getCurrentNormalizedTradingWindowBothInBoundsTests() {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentNormalizedTradingWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinWindow {
			t.Errorf("TestGetCurrentNormalizedTradingWindowBothOutOfBounds(%d): expected within window %v, actual %v", idx, tt.expectedWithinWindow, actualWithinWindow)
		}
	}
}

func getCurrentNormalizedTradingWindowCloseOutOfBoundsTests() []struct {
	current              time.Time
	expectedOpen         time.Time
	expectedClose        time.Time
	expectedWithinWindow bool
} {
	// tzLA, _ := time.LoadLocation("America/Los_Angeles")
	tzNY, _ := time.LoadLocation("America/New_York")
	// tzBG, _ := time.LoadLocation("America/Lima")

	return []struct {
		current              time.Time
		expectedOpen         time.Time
		expectedClose        time.Time
		expectedWithinWindow bool
	}{
		{
			current:              time.Date(2020, time.Month(4), 30, 18, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 1, 16, 59, 59, 0, tzNY),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(4), 26, 11, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 1, 16, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 11, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(4), 26, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 1, 16, 59, 59, 0, tzNY),
			expectedWithinWindow: true,
		},
		{
			current:              time.Date(2020, time.Month(5), 1, 17, 30, 0, 0, tzNY),
			expectedOpen:         time.Date(2020, time.Month(5), 3, 18, 0, 0, 0, tzNY),
			expectedClose:        time.Date(2020, time.Month(5), 8, 16, 59, 59, 0, tzNY),
			expectedWithinWindow: false,
		},
	}
}

func TestGetCurrentNormalizedTradingWindowCloseOutOfBounds(t *testing.T) {
	//set env for testing
	env.DefaultConfig.TradeWindowOpenDay = 0
	env.DefaultConfig.TradeWindowOpenHour = 18
	env.DefaultConfig.TradeWindowCloseDay = 5
	env.DefaultConfig.TradeWindowCloseHour = 18
	env.Configure()

	for idx, tt := range getCurrentNormalizedTradingWindowCloseOutOfBoundsTests() {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentNormalizedTradingWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowCloseOutOfBounds(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentNormalizedTradingWindowCloseOutOfBounds(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinWindow {
			t.Errorf("TestGetCurrentNormalizedTradingWindowCloseOutOfBounds(%d): expected within window %v, actual %v", idx, tt.expectedWithinWindow, actualWithinWindow)
		}
	}
}

func getCurrentMarketHoursWindowTests() []struct {
	current                   time.Time
	expectedOpen              time.Time
	expectedClose             time.Time
	expectedWithinMarketHours bool
} {
	tzLA, _ := time.LoadLocation("America/Los_Angeles")
	tzNY, _ := time.LoadLocation("America/New_York")
	tzBG, _ := time.LoadLocation("America/Lima")

	return []struct {
		current                   time.Time
		expectedOpen              time.Time
		expectedClose             time.Time
		expectedWithinMarketHours bool
	}{
		{
			current:                   time.Date(2020, time.Month(4), 30, 18, 30, 0, 0, tzLA),
			expectedOpen:              time.Date(2020, time.Month(4), 26, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(5), 1, env.GetMarketHours().MarketHoursCloseHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: true,
		},
		{
			current:                   time.Date(2020, time.Month(5), 1, 18, 30, 0, 0, tzNY),
			expectedOpen:              time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursOpenHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: false,
		},
		{
			current:                   time.Date(2020, time.Month(6), 26, 16, 30, 0, 0, tzNY),
			expectedOpen:              time.Date(2020, time.Month(6), 21, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(6), 26, env.GetMarketHours().MarketHoursOpenHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: true,
		},
		{
			current:                   time.Date(2020, time.Month(5), 2, 12, 0, 0, 0, tzBG),
			expectedOpen:              time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursOpenHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: false,
		},
		{
			current:                   time.Date(2020, time.Month(5), 3, 12, 0, 0, 0, tzBG),
			expectedOpen:              time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursOpenHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: false,
		},
		{
			current:                   time.Date(2020, time.Month(5), 3, 18, 0, 0, 1, tzBG),
			expectedOpen:              time.Date(2020, time.Month(5), 3, env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation),
			expectedClose:             time.Date(2020, time.Month(5), 8, env.GetMarketHours().MarketHoursOpenHour-1, 59, 59, 0, env.GetMarketHours().MarketHoursLocation),
			expectedWithinMarketHours: true,
		},
	}
}

func TestGetCurrentMarketHoursWindow(t *testing.T) {
	for idx, tt := range getCurrentMarketHoursWindowTests() {
		actualOpen, actualClose, actualWithinWindow := util.GetCurrentMarketHoursWindow(tt.current)
		if !actualOpen.Equal(tt.expectedOpen) {
			t.Errorf("TestGetCurrentMarketHoursWindow(%d): expected open %v, actual %v", idx, tt.expectedOpen.Format(time.RFC1123), actualOpen.Format(time.RFC1123))
		}
		if !actualClose.Equal(tt.expectedClose) {
			t.Errorf("TestGetCurrentMarketHoursWindow(%d): expected close %v, actual %v", idx, tt.expectedClose.Format(time.RFC1123), actualClose.Format(time.RFC1123))
		}
		if !actualWithinWindow == tt.expectedWithinMarketHours {
			t.Errorf("TestGetCurrentMarketHoursWindow(%d): expected within market hours %v, actual %v", idx, tt.expectedWithinMarketHours, actualWithinWindow)
		}
	}
}
