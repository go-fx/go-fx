package util

import (
	"math"
	"strconv"
	"strings"
	"sync"

	gofxhttp "gofx/client/http"
	"gofx/interfaces"
	"gofx/model/instruments"
)

//GetAvailableMarginUnits calculates tradable units from available margin for an instrument
func GetAvailableMarginUnits(inst instruments.Instrument, client interfaces.HTTPClient) (int, error) {
	return getMarginUnits(false, inst, client)
}

//GetTotalMarginUnits calculates tradable units from total margin for an instrument
func GetTotalMarginUnits(inst instruments.Instrument, client interfaces.HTTPClient) (int, error) {
	return getMarginUnits(true, inst, client)
}

func getMarginUnits(useTotalMargin bool, inst instruments.Instrument, client interfaces.HTTPClient) (int, error) {
	marginChan, marginRateChan, baseToHomeRateChan := make(chan float64, 1), make(chan float64, 1), make(chan float64, 1)
	errChan := make(chan error, 3)
	wg := &sync.WaitGroup{}
	wg.Add(3)

	//get margin from account
	go func() {
		resAccount, err := gofxhttp.GetAccount(client)
		if err != nil {
			errChan <- err
			wg.Done()
			return
		}
		var margin float64
		if useTotalMargin {
			margin, _ = strconv.ParseFloat(resAccount.Balance, 64)
		} else {
			margin, _ = strconv.ParseFloat(resAccount.MarginAvailable, 64)
		}
		marginChan <- margin
		wg.Done()
	}()

	//get margin rate from instrument
	go func() {
		resInst, err := gofxhttp.GetInstruments([]instruments.Instrument{inst}, client)
		if err != nil {
			errChan <- err
			wg.Done()
			return
		}
		marginRate, _ := strconv.ParseFloat(resInst.Instruments[0].MarginRate, 64)
		marginRateChan <- marginRate
		wg.Done()
	}()

	//get base to home conversion rate
	go func() {
		baseCurrency := strings.Split(inst.Name(), "_")[0]
		var baseToHomeRate float64
		resPrice, err := gofxhttp.GetPricing([]instruments.Instrument{inst}, true, nil, client)
		if err != nil {
			errChan <- err
			wg.Done()
			return
		}

		for _, rate := range resPrice.HomeConversions {
			if rate.Currency == baseCurrency {
				baseToHomeRate, _ = strconv.ParseFloat(rate.AccountGain, 64)
				break
			}
		}
		baseToHomeRateChan <- baseToHomeRate
		wg.Done()
	}()

	wg.Wait()
	if len(errChan) > 0 {
		err := <-errChan
		close(errChan)
		close(marginChan)
		close(marginRateChan)
		close(baseToHomeRateChan)
		return 0, err
	}
	close(errChan)

	margin, marginRate, baseToHomeRate := <-marginChan, <-marginRateChan, <-baseToHomeRateChan
	close(marginChan)
	close(marginRateChan)
	close(baseToHomeRateChan)

	return int(margin / marginRate / baseToHomeRate), nil
}

//GetPipGain calculates the profit per PIP for a trade
func GetPipGain(units int, inst instruments.Instrument, client interfaces.HTTPClient) (float64, error) {
	//get pip location from instruments
	instRes, err := gofxhttp.GetInstruments([]instruments.Instrument{inst}, client)
	if err != nil {
		return 0., err
	}
	pipLocation := instRes.Instruments[0].PipLocation

	//get home to quote rate from pricing
	pricingRes, err := gofxhttp.GetPricing([]instruments.Instrument{inst}, true, nil, client)
	if err != nil {
		return 0., err
	}

	//get quote currency
	quoteCurrency := strings.Split(inst.Name(), "_")[1]

	//find quote conversion
	var quoteToHomeRate float64
	for _, conv := range pricingRes.HomeConversions {
		if conv.Currency == quoteCurrency {
			quoteToHomeRate, _ = strconv.ParseFloat(conv.AccountGain, 64)
		}
	}

	return math.Round((float64(units)*math.Pow10(pipLocation)*quoteToHomeRate)*1e6) / 1e6, nil
}
