package util_test

import (
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"testing"

	"gofx/client/util"
	"gofx/model/instruments"
)

type testEurUsdClient struct {
	lock sync.Mutex
}

func (tc *testEurUsdClient) Do(req *http.Request) (*http.Response, error) {
	tc.lock.Lock()

	res := &http.Response{}
	switch {
	case strings.Contains(req.URL.String(), "/instruments"):
		resWriter := strings.NewReader(`{
			"instruments": [
				{
					"name": "EUR_USD",
					"type": "CURRENCY",
					"displayName": "EUR/USD",
					"pipLocation": -4,
					"displayPrecision": 5,
					"tradeUnitsPrecision": 0,
					"minimumTradeSize": "1",
					"maximumTrailingStopDistance": "1.00000",
					"minimumTrailingStopDistance": "0.00050",
					"maximumPositionSize": "0",
					"maximumOrderUnits": "100000000",
					"marginRate": "0.02",
					"guaranteedStopLossOrderMode": "DISABLED",
					"tags": [
						{
							"type": "ASSET_CLASS",
							"name": "CURRENCY"
						}
					],
					"financing": {
						"longRate": "-0.0502",
						"shortRate": "-0.0118",
						"financingDaysOfWeek": [
							{
								"dayOfWeek": "MONDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "TUESDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "WEDNESDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "THURSDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "FRIDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "SATURDAY",
								"daysCharged": 0
							},
							{
								"dayOfWeek": "SUNDAY",
								"daysCharged": 0
							}
						]
					}
				}
			],
			"lastTransactionID": "5555"
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	case strings.Contains(req.URL.String(), "/pricing"):
		resWriter := strings.NewReader(`{
			"time": "2020-05-14T03:05:38.730982830Z",
			"prices": [
				{
					"type": "PRICE",
					"time": "2020-05-14T03:05:21.930363759Z",
					"bids": [
						{
							"price": "1.08124",
							"liquidity": 10000000
						}
					],
					"asks": [
						{
							"price": "1.08139",
							"liquidity": 10000000
						}
					],
					"closeoutBid": "1.08124",
					"closeoutAsk": "1.08139",
					"status": "tradeable",
					"tradeable": true,
					"unitsAvailable": {
						"default": {
							"long": "2261199",
							"short": "2261199"
						},
						"openOnly": {
							"long": "2261199",
							"short": "2261199"
						},
						"reduceFirst": {
							"long": "2261199",
							"short": "2261199"
						},
						"reduceOnly": {
							"long": "0",
							"short": "0"
						}
					},
					"instrument": "EUR_USD"
				}
			],
			"homeConversions": [
				{
					"currency": "EUR",
					"accountGain": "1.08124",
					"accountLoss": "1.08139",
					"positionValue": "1.08132"
				},
				{
					"currency": "USD",
					"accountGain": "1",
					"accountLoss": "1",
					"positionValue": "1"
				}
			]
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	case strings.Contains(req.URL.String(), "/accounts"):
		resWriter := strings.NewReader(`{
			"account": {
				"currency": "USD",
				"alias": "Primary",
				"marginRate": "0.02",
				"lastTransactionID": "5555",
				"balance": "50000.00",
				"openTradeCount": 0,
				"openPositionCount": 0,
				"pendingOrderCount": 0,
				"financing": "-419.3940",
				"commission": "0.0000",
				"dividendAdjustment": "0",
				"guaranteedExecutionFees": "0.0000",
				"orders": [],
				"positions": [],
				"trades": [],
				"unrealizedPL": "5000.0000",
				"NAV": "55000.00",
				"marginUsed": "0.0000",
				"marginAvailable": "55000.00",
				"positionValue": "0.0000",
				"marginCloseoutUnrealizedPL": "0.0000",
				"marginCloseoutNAV": "55000.00",
				"marginCloseoutMarginUsed": "0.0000",
				"marginCloseoutPositionValue": "0.0000",
				"marginCloseoutPercent": "0.00000",
				"withdrawalLimit": "55000.00",
				"marginCallMarginUsed": "0.0000",
				"marginCallPercent": "0.00000"
			},
			"lastTransactionID": "5555"
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	}

	tc.lock.Unlock()
	return res, nil
}

var getAvailableMarginUnitsTests = []struct {
	instrument    instruments.Instrument
	expectedUnits int
}{
	{
		instrument:    instruments.EurUsd,
		expectedUnits: 2543376,
	},
}

// func TestGetAvailableMarginUnits(t *testing.T) {
// 	for idx, tt := range getAvailableMarginUnitsTests {
// 		actualUnits, actualErr := util.GetAvailableMarginUnits(tt.instrument, &testEurUsdClient{})
// 		if actualErr != nil {
// 			t.Errorf("TestGetAvailableMarginUnits(%d): expected %v, actual %v", idx, nil, actualErr)
// 		}
// 		if actualUnits != tt.expectedUnits {
// 			t.Errorf("TestGetAvailableMarginUnits(%d): expected %v, actual %v", idx, tt.expectedUnits, actualUnits)
// 		}
// 	}
// }

var getTotalMarginUnitsTests = []struct {
	instrument    instruments.Instrument
	expectedUnits int
}{
	{
		instrument:    instruments.EurUsd,
		expectedUnits: 2312160,
	},
}

// func TestGetTotalMarginUnits(t *testing.T) {
// 	for idx, tt := range getTotalMarginUnitsTests {
// 		actualUnits, actualErr := util.GetTotalMarginUnits(tt.instrument, &testEurUsdClient{})
// 		if actualErr != nil {
// 			t.Errorf("TestGetTotalMarginUnits(%d): expected %v, actual %v", idx, nil, actualErr)
// 		}
// 		if actualUnits != tt.expectedUnits {
// 			t.Errorf("TestGetTotalMarginUnits(%d): expected %v, actual %v", idx, tt.expectedUnits, actualUnits)
// 		}
// 	}
// }

type testUsdJpyClient struct {
}

func (tc *testUsdJpyClient) Do(req *http.Request) (*http.Response, error) {
	res := &http.Response{}
	switch {
	case strings.Contains(req.URL.String(), "/instruments"):
		resWriter := strings.NewReader(`{
			"instruments": [
				{
					"name": "USD_JPY",
					"type": "CURRENCY",
					"displayName": "USD/JPY",
					"pipLocation": -2,
					"displayPrecision": 3,
					"tradeUnitsPrecision": 0,
					"minimumTradeSize": "1",
					"maximumTrailingStopDistance": "100.000",
					"minimumTrailingStopDistance": "0.050",
					"maximumPositionSize": "0",
					"maximumOrderUnits": "100000000",
					"marginRate": "0.04",
					"guaranteedStopLossOrderMode": "DISABLED",
					"tags": [
						{
							"type": "ASSET_CLASS",
							"name": "CURRENCY"
						}
					],
					"financing": {
						"longRate": "-0.0076",
						"shortRate": "-0.0137",
						"financingDaysOfWeek": [
							{
								"dayOfWeek": "MONDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "TUESDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "WEDNESDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "THURSDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "FRIDAY",
								"daysCharged": 1
							},
							{
								"dayOfWeek": "SATURDAY",
								"daysCharged": 0
							},
							{
								"dayOfWeek": "SUNDAY",
								"daysCharged": 0
							}
						]
					}
				}
			],
			"lastTransactionID": "5173"	
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	case strings.Contains(req.URL.String(), "/pricing"):
		resWriter := strings.NewReader(`{
			"time": "2020-05-12T18:25:46.386534182Z",
			"prices": [
				{
					"type": "PRICE",
					"time": "2020-05-12T18:25:46.092668982Z",
					"bids": [
						{
							"price": "107.281",
							"liquidity": 10000000
						}
					],
					"asks": [
						{
							"price": "107.294",
							"liquidity": 10000000
						}
					],
					"closeoutBid": "107.281",
					"closeoutAsk": "107.294",
					"status": "tradeable",
					"tradeable": true,
					"unitsAvailable": {
						"default": {
							"long": "1222539",
							"short": "1222539"
						},
						"openOnly": {
							"long": "1222539",
							"short": "1222539"
						},
						"reduceFirst": {
							"long": "1222539",
							"short": "1222539"
						},
						"reduceOnly": {
							"long": "0",
							"short": "0"
						}
					},
					"instrument": "USD_JPY"
				}
			],
			"homeConversions": [
				{
					"currency": "USD",
					"accountGain": "1",
					"accountLoss": "1",
					"positionValue": "1"
				},
				{
					"currency": "JPY",
					"accountGain": "0.009320185658",
					"accountLoss": "0.009321315051",
					"positionValue": "0.009320750355"
				}
			]
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	}

	return res, nil
}

var getPipPLTests = []struct {
	units      int
	instrument instruments.Instrument
	expectedPL float64
}{
	{
		units:      10000,
		instrument: instruments.UsdJpy,
		expectedPL: 0.932019,
	},
}

func TestGetPipGain(t *testing.T) {
	for idx, tt := range getPipPLTests {
		actualPL, actualErr := util.GetPipGain(tt.units, tt.instrument, &testUsdJpyClient{})
		if actualErr != nil {
			t.Errorf("TestGetPipPL(%d): expected %v, actual %v", idx, nil, actualErr)
		}
		if actualPL != tt.expectedPL {
			t.Errorf("TestGetPipPL(%d): expected %v, actual %v", idx, tt.expectedPL, actualPL)
		}
	}
}
