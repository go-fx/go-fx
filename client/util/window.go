package util

import (
	// "fmt"
	"time"

	"gofx/env"
)

//GetCurrentMarketHoursWindow returns the market hours window times (open/close) and whether current time is within that window
func GetCurrentMarketHoursWindow(current time.Time) (time.Time, time.Time, bool) {
	refTime := current.In(env.GetMarketHours().MarketHoursLocation)

	//prev open
	prevOpen := refTime.AddDate(0, 0, int(env.GetMarketHours().MarketHoursOpenDay-refTime.Weekday())).
		Add(time.Hour * time.Duration(env.GetMarketHours().MarketHoursOpenHour-refTime.Hour())).
		Add(time.Minute * time.Duration(-refTime.Minute()))
	prevOpen = time.Date(prevOpen.Year(), prevOpen.Month(), prevOpen.Day(), env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation)

	//next open
	nextOpen := refTime.AddDate(0, 0, 7+int(env.GetMarketHours().MarketHoursOpenDay-refTime.Weekday())).
		Add(time.Hour * time.Duration(env.GetMarketHours().MarketHoursOpenHour-refTime.Hour())).
		Add(time.Minute * time.Duration(-refTime.Minute()))
	nextOpen = time.Date(nextOpen.Year(), nextOpen.Month(), nextOpen.Day(), env.GetMarketHours().MarketHoursOpenHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation)

	if prevOpen.After(refTime) {
		prevOpen = prevOpen.AddDate(0, 0, -7)
		nextOpen = nextOpen.AddDate(0, 0, -7)
	}

	//prev close
	prevClose := refTime.AddDate(0, 0, int(env.GetMarketHours().MarketHoursCloseDay-refTime.Weekday())-7).
		Add(time.Hour * time.Duration(env.GetMarketHours().MarketHoursCloseHour-refTime.Hour())).
		Add(time.Minute * time.Duration(-refTime.Minute()))
	prevClose = time.Date(prevClose.Year(), prevClose.Month(), prevClose.Day(), env.GetMarketHours().MarketHoursCloseHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation).Add(-time.Second)

	nextClose := refTime.AddDate(0, 0, int(env.GetMarketHours().MarketHoursCloseDay-refTime.Weekday())).
		Add(time.Hour * time.Duration(env.GetMarketHours().MarketHoursCloseHour-refTime.Hour())).
		Add(time.Minute * time.Duration(-refTime.Minute()))
	nextClose = time.Date(nextClose.Year(), nextClose.Month(), nextClose.Day(), env.GetMarketHours().MarketHoursCloseHour, 0, 0, 0, env.GetMarketHours().MarketHoursLocation).Add(-time.Second)

	if nextClose.Before(refTime) {
		prevClose = prevClose.AddDate(0, 0, 7)
		nextClose = nextClose.AddDate(0, 0, 7)
	}

	if prevOpen.After(prevClose) && nextOpen.After(nextClose) {
		return prevOpen.In(current.Location()), nextClose.In(current.Location()), true
	}

	return nextOpen.In(current.Location()), nextClose.In(current.Location()), false
}

//GetCurrentTradingWindow returns the trading window times (open/close) and whether current time is within that window; based on config
func GetCurrentTradingWindow(current time.Time) (time.Time, time.Time, bool) {
	//if environment is above EnvDev
	if env.GetConfig().ServerEnv > env.EnvDev {
		return getTradingWindow(current, true)
	}
	return getTradingWindow(current, false)
}

//getTradingWindow returns the trading window times (open/close) and whether current time is within that window; based on config
func getTradingWindow(current time.Time, isNormalized bool) (time.Time, time.Time, bool) {
	//normalize to market hours if specified
	if isNormalized {
		return getCurrentNormalizedTradingWindow(current)
	}

	//reference time to preserve original param value
	refTime := current

	//prev open
	prevOpen := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowOpenDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowOpenHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowOpenMinute-current.Minute()))
	prevOpen = time.Date(prevOpen.Year(), prevOpen.Month(), prevOpen.Day(), prevOpen.Hour(), prevOpen.Minute(), 0, 0, current.Location())

	//next open
	nextOpen := refTime.AddDate(0, 0, 7+int(env.GetConfig().TradeWindowOpenDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowOpenHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowOpenMinute-current.Minute()))
	nextOpen = time.Date(nextOpen.Year(), nextOpen.Month(), nextOpen.Day(), nextOpen.Hour(), nextOpen.Minute(), 0, 0, current.Location())

	if prevOpen.After(current) {
		prevOpen = prevOpen.AddDate(0, 0, -7)
		nextOpen = nextOpen.AddDate(0, 0, -7)
	}

	//prev close
	prevClose := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowCloseDay-current.Weekday())-7).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowCloseHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowCloseMinute-current.Minute()))
	prevClose = time.Date(prevClose.Year(), prevClose.Month(), prevClose.Day(), prevClose.Hour(), prevClose.Minute(), 0, 0, current.Location()).Add(-time.Second)

	nextClose := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowCloseDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowCloseHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowCloseMinute-current.Minute()))
	nextClose = time.Date(nextClose.Year(), nextClose.Month(), nextClose.Day(), nextClose.Hour(), nextClose.Minute(), 0, 0, current.Location()).Add(-time.Second)

	if nextClose.Before(current) {
		prevClose = prevClose.AddDate(0, 0, 7)
		nextClose = nextClose.AddDate(0, 0, 7)
	}

	if prevOpen.After(prevClose) && nextOpen.After(nextClose) {
		return prevOpen, nextClose, true
	}

	return nextOpen, nextClose, false
}

//getCurrentNormalizedTradingWindow returns the trading window times (open/close) - normalized by market hours- and whether current time is within that window
func getCurrentNormalizedTradingWindow(current time.Time) (time.Time, time.Time, bool) {
	refTime := current

	//get market hours
	mOpen, mClose, mWithin := GetCurrentMarketHoursWindow(refTime)

	//prev open
	prevOpen := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowOpenDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowOpenHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowOpenMinute-current.Minute()))
	prevOpen = time.Date(prevOpen.Year(), prevOpen.Month(), prevOpen.Day(), prevOpen.Hour(), prevOpen.Minute(), 0, 0, current.Location())

	//normalize prev open
	if mWithin && prevOpen.Before(mOpen) {
		prevOpen = prevOpen.Add(mOpen.Sub(prevOpen))
	}

	//next open
	nextOpen := refTime.AddDate(0, 0, 7+int(env.GetConfig().TradeWindowOpenDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowOpenHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowOpenMinute-current.Minute()))
	nextOpen = time.Date(nextOpen.Year(), nextOpen.Month(), nextOpen.Day(), nextOpen.Hour(), nextOpen.Minute(), 0, 0, current.Location())

	//normalize open times
	if mWithin {
		if _, _, prevOpenWithin := GetCurrentMarketHoursWindow(prevOpen); !prevOpenWithin {
			prevOpen = mOpen
		}
		if nextOpen.Before(mOpen.AddDate(0, 0, 7)) {
			nextOpen = nextOpen.Add(mOpen.AddDate(0, 0, 7).Sub(nextOpen))
		}
	} else {
		if _, _, prevOpenWithin := GetCurrentMarketHoursWindow(prevOpen); !prevOpenWithin {
			prevOpen = mOpen.AddDate(0, 0, -7)
		} else {
			if prevOpen.Before(mOpen) {
				prevOpen = prevOpen.Add(time.Date(mOpen.Year(), mOpen.Month(), mOpen.Day(), prevOpen.Hour(), 0, 0, 0, mOpen.Location()).Sub(prevOpen))
				if prevOpen.Before(mOpen) {
					prevOpen = prevOpen.Add(mOpen.Sub(prevOpen))
				}
			}
			prevOpen = prevOpen.AddDate(0, 0, -7)
		}
		if _, _, nextOpenWithin := GetCurrentMarketHoursWindow(nextOpen); !nextOpenWithin {
			nextOpen = mOpen
		} else {
			if nextOpen.Before(mOpen.AddDate(0, 0, 7)) {
				nextMarketOpen := mOpen.AddDate(0, 0, 7)
				nextOpen = nextOpen.Add(time.Date(nextMarketOpen.Year(), nextMarketOpen.Month(), nextMarketOpen.Day(), nextOpen.Hour(), 0, 0, 0, nextMarketOpen.Location()).Sub(nextOpen))
				if nextOpen.Before(mOpen.AddDate(0, 0, 7)) {
					nextOpen = nextOpen.Add(mOpen.AddDate(0, 0, 7).Sub(nextOpen))
				}
			}
			nextOpen = nextOpen.AddDate(0, 0, -7)
		}
	}

	//prev close
	prevClose := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowCloseDay-current.Weekday())-7).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowCloseHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowCloseMinute-current.Minute()))
	prevClose = time.Date(prevClose.Year(), prevClose.Month(), prevClose.Day(), prevClose.Hour(), prevClose.Minute(), 0, 0, current.Location()).Add(-time.Second)

	//normalize prev close
	if mWithin && mClose.AddDate(0, 0, -7).Before(prevClose) {
		prevClose = prevClose.Add(mClose.AddDate(0, 0, -7).Sub(prevClose))
	}

	//next close
	nextClose := refTime.AddDate(0, 0, int(env.GetConfig().TradeWindowCloseDay-current.Weekday())).
		Add(time.Hour * time.Duration(env.GetConfig().TradeWindowCloseHour-current.Hour())).
		Add(time.Minute * time.Duration(env.GetConfig().TradeWindowCloseMinute-current.Minute()))
	nextClose = time.Date(nextClose.Year(), nextClose.Month(), nextClose.Day(), nextClose.Hour(), nextClose.Minute(), 0, 0, current.Location()).Add(-time.Second)

	//normalize close times
	if mWithin {
		if mClose.Before(nextClose) {
			nextClose = nextClose.Add(mClose.Sub(nextClose))
		}
	} else {
		if env.GetConfig().TradeWindowCloseDay <= refTime.Weekday() {
			prevClose = prevClose.AddDate(0, 0, 7)
			nextClose = nextClose.AddDate(0, 0, 7)
		}
		if mClose.AddDate(0, 0, -7).Before(prevClose) {
			prevClose = prevClose.Add(mClose.AddDate(0, 0, -7).Sub(prevClose))
		}
		if mClose.Before(nextClose) {
			nextClose = nextClose.Add(mClose.Sub(nextClose))
		}
	}

	//get trading hours
	_, wClose, withinWindow := getTradingWindow(current, false)

	//DEBUG
	// fmt.Println("\ncurrent:", current)

	//update open/close times if outside normal window but inside market hours
	if mWithin && !withinWindow {
		//DEBUG
		// fmt.Println("inside market hours, outside trading hours...")

		if current.Before(prevOpen) {
			prevOpen = prevOpen.AddDate(0, 0, -7)
			nextOpen = nextOpen.AddDate(0, 0, -7)
			prevClose = prevClose.AddDate(0, 0, -7)
			nextClose = nextClose.AddDate(0, 0, -7)
		}
		if wClose.AddDate(0, 0, -7).Before(mClose) {
			prevClose = prevClose.AddDate(0, 0, 7)
			nextClose = nextClose.AddDate(0, 0, 7)
		}
	}

	//DEBUG
	// fmt.Println("wOpen:", wOpen)
	// fmt.Println("wClose:", wClose)
	// fmt.Println("mOpen:", mOpen)
	// fmt.Println("mClose:", mClose)
	// fmt.Println("prevOpen:", prevOpen)
	// fmt.Println("prevClose:", prevClose)
	// fmt.Println("nextOpen:", nextOpen)
	// fmt.Println("nextClose:", nextClose)

	if prevOpen.After(prevClose) && nextOpen.After(nextClose) {
		return prevOpen, nextClose, true
	}

	return nextOpen, nextClose, false
}
