package client

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	gofxhttp "gofx/client/http"
	"gofx/client/util"
	"gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/loggers"
	"gofx/model"
	"gofx/strategy"
)

var (
	//ActiveStrategy is the active strategy for this excution of client
	ActiveStrategy strategy.Strategy

	//StartupTime is the time the client started
	StartupTime time.Time

	logger = loggers.GetLogger()

	tradingHoursTimeFmt, waitingTimeFmt string = "Monday, Jan 2 2006, 15:04 MST", "3:04PM on Monday, Jan 02"
)

//Run runs the client app
func Run() {
	//log strategy name
	logger.Printf("Strategy Name: '%s'\n\n", ActiveStrategy.Name())

	//display strategy title
	logger.Println(ActiveStrategy.Title())

	//set startup time
	StartupTime = time.Now()

	logger.Println("Current trading window...")

	//log window times
	open, close, isWithinWindow := util.GetCurrentTradingWindow(StartupTime)

	logger.Printf("Open  : %s", open.Format(tradingHoursTimeFmt))
	logger.Printf("Close : %s\n\n", close.Add(time.Second).Format(tradingHoursTimeFmt))

	//log whether current time is within trading hours
	withinWindowStatus := "OUTSIDE"
	if isWithinWindow {
		withinWindowStatus = "WITHIN"
	}
	logger.Printf("Current time is %s trading hours...\n\n", withinWindowStatus)

	//if not within window, enter sleep/reminder phase
	if !isWithinWindow {
		sleepReminderPhase(open)
	}

	//create account snapshot if not present
	if dbSnapshot, _ := db.GetAccountSnapshotByWindowOpen(open, nil); dbSnapshot == nil {
		resAcct, _ := gofxhttp.GetAccount(&http.Client{})
		resAcctTime := time.Now()
		newSnapshot := model.NewAccountSnapshot(open, resAcct)
		newSnapshot.CreatedAt = &resAcctTime
		if err := db.CreateAccountSnapshot(newSnapshot, nil); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not create account snapshot")
			panic(err)
		}
	}

	//run trading flow
	wg := &sync.WaitGroup{}
	wg.Add(1)
	runTradingFlow(ActiveStrategy, wg)

	//wait for client to exit
	wg.Wait()
}

//sleep until window opens, logging reminders and sending optional reminder alert email
func sleepReminderPhase(windowOpen time.Time) {
	//log waiting duration
	logger.Printf("...waiting %v for trade window to open @ %s\n\n", time.Until(windowOpen), windowOpen.Format(waitingTimeFmt))

	//send alert before window opens if env var is set
	var sendAlert <-chan time.Time
	if env.GetConfig().AlertOpenWindowInMinutes > 0 && time.Now().Add(time.Minute*time.Duration(env.GetConfig().AlertOpenWindowInMinutes)).Before(windowOpen) {
		sendAlert = time.After(windowOpen.Sub(time.Now().Add(time.Minute * time.Duration(env.GetConfig().AlertOpenWindowInMinutes))))
		go func(send <-chan time.Time) {
			<-sendAlert
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("sending alert that trading window will open in %v", time.Until(windowOpen).Round(time.Minute))

			//get account data
			if acct, err := gofxhttp.GetAccount(&http.Client{}); err == nil {
				//alert data
				alertData := email.WindowAlertTemplateData{}
				alertData.AccountTemplateData = *email.NewAccountTemplateData(acct)
				alertData.AppName = env.GetConfig().AppName
				alertData.CompanyLink = env.GetConfig().AppCompanyLink
				alertData.CompanyName = env.GetConfig().AppCompany
				alertData.CopyrightYear = time.Now().Year()
				alertData.StrategyName = ActiveStrategy.Name()
				alertData.TypeTitle = "Opening"
				alertData.TypeLower = "opening"
				alertData.WindowTimeRFC1123 = windowOpen.Format(time.RFC1123)
				alertData.DurationUntilWindowTime = fmt.Sprintf("%dm", env.GetConfig().AlertOpenWindowInMinutes)

				email.ActiveProvider.SendTXWindowAlert(env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, fmt.Sprintf("Trading Window %s Soon", alertData.TypeTitle), alertData, &http.Client{}, nil)
			} else {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("error sending alert that trading window will open")
			}
		}(sendAlert)
	}

	//heartbeat ticker for logged reminders until window opens
	var nextTickTime time.Time
	if time.Now().Round(time.Second * time.Duration(env.GetConfig().TradeTickerWaitingSeconds)).Before(time.Now()) {
		nextTickTime = time.Now().Add(time.Second * time.Duration(env.GetConfig().TradeTickerWaitingSeconds)).Round(time.Second * time.Duration(env.GetConfig().TradeTickerWaitingSeconds))
	} else {
		nextTickTime = time.Now().Round(time.Second * time.Duration(env.GetConfig().TradeTickerWaitingSeconds))
	}

	var heartbeat *time.Ticker
	doneBeating := make(chan bool, 1)
	if nextTickTime.Before(windowOpen) {
		go func(nextTick time.Time, beat *time.Ticker) {
			<-time.After(time.Until(nextTick))
			logger.Printf("...waiting %v for trade window to open @ %s\n\n", time.Until(windowOpen).Round(time.Second*time.Duration(env.GetConfig().TradeTickerWaitingSeconds)),
				windowOpen.Format(waitingTimeFmt))

			beat = time.NewTicker(time.Second * time.Duration(env.GetConfig().TradeTickerWaitingSeconds))
			for {
				select {
				case <-beat.C:
					logger.Printf("...waiting %v for trade window to open @ %s\n\n", time.Until(windowOpen).Round(time.Second*time.Duration(env.GetConfig().TradeTickerWaitingSeconds)),
						windowOpen.Format(waitingTimeFmt))

				case <-doneBeating:
					beat.Stop()
					// logger.Println("done beating..")
					return
				}
			}
		}(nextTickTime, heartbeat)
	}

	//wake up when window opens
	wakeup := time.After(time.Until(windowOpen) + time.Millisecond*time.Duration(100))
	<-wakeup
	doneBeating <- true
	logger.Printf("Trading window is now OPEN!\n\n")
}
