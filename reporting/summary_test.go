package reporting_test

/*
import (
	"fmt"
	"net/http"
	"testing"
	"time"

	// "gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/reporting"
)

func init() {
	email.ActiveProvider = email.SendGrid{
		ProviderEmailAuthHeader:     env.GetConfig().ProviderEmailAuthHeader,
		ProviderEmailEndpointSendTX: env.GetConfig().ProviderEmailEndpointSendTX,
	}
}

type testClient struct {
	isFailer bool
}

func (tc testClient) Do(r *http.Request) (*http.Response, error) {
	if tc.isFailer {
		return nil, fmt.Errorf("Generated testing error")
	}
	return &http.Response{StatusCode: http.StatusAccepted}, nil
}

func TestSendDailySummaryReportEmail(t *testing.T) {
	//db
	// schemaName := "gofxdb"
	// db.GetConnection(&schemaName)

	//template path
	templatePath := "../email/templates/report.email"

	//base data
	baseData := email.BaseTemplateData{}

	baseData.AppName = env.GetConfig().AppName
	baseData.CompanyLink = env.GetConfig().AppCompanyLink
	baseData.CompanyName = env.GetConfig().AppCompany
	baseData.CopyrightYear = time.Now().Year()

	reporting.SendDailySummaryReportEmail(time.Now(), "Dummy", "user@gofx.live", env.GetConfig().ProviderEmailFromAddress, baseData, &testClient{}, &templatePath)
	// actualErr := reporting.SendDailySummaryReportEmail(time.Now(), "Dummy", "user@gofx.live", env.GetConfig().ProviderEmailFromAddress, baseData, &testClient{}, &templatePath)
	// if actualErr != nil {
	// 	t.Errorf("TestSendDailySummaryReportEmail >> expected err: %v, actual: %v", nil, actualErr)
	// }
}

func TestSendWeeklySummaryReportEmail(t *testing.T) {
	//db
	// schemaName := "gofxdb"
	// db.GetConnection(&schemaName)

	//template path
	templatePath := "../email/templates/report.email"

	//base data
	baseData := email.BaseTemplateData{}

	baseData.AppName = env.GetConfig().AppName
	baseData.CompanyLink = env.GetConfig().AppCompanyLink
	baseData.CompanyName = env.GetConfig().AppCompany
	baseData.CopyrightYear = time.Now().Year()

	reporting.SendWeeklySummaryReportEmail(time.Now(), "Dummy", "user@gofx.live", env.GetConfig().ProviderEmailFromAddress, baseData, &testClient{}, &templatePath)
	// actualErr := reporting.SendDailySummaryReportEmail(time.Now(), "Dummy", "user@gofx.live", env.GetConfig().ProviderEmailFromAddress, baseData, &testClient{}, &templatePath)
	// if actualErr == nil {
	// 	t.Errorf("TestSendDailySummaryReportEmail >> expected err: %v, actual: %v", nil, actualErr)
	// }
}
*/
