package reporting

import (
	"time"

	"gofx/client/util"
	"gofx/db"
	"gofx/env"
	"gofx/loggers"
	"gofx/model"
)

//GetDailyClosedTrades retrieves details on all trades which closed for current day, based on trade day closing hour and minute
func GetDailyClosedTrades(nextDaySpotTime time.Time, strategyName string) (map[string]*model.SignalTrade, error) {
	// -- determine report begin/end times --
	var beginTime, endTime time.Time

	//EndTime
	if env.GetConfig().TradeDayClosingHour <= nextDaySpotTime.Hour() {
		endTime = time.Date(nextDaySpotTime.Year(), nextDaySpotTime.Month(), nextDaySpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local).Add(time.Second * -1)
	} else {
		endTime = time.Date(nextDaySpotTime.Year(), nextDaySpotTime.Month(), nextDaySpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local).Add(time.Hour * -24).Add(time.Second * -1)
	}

	//BeginTime
	beginTime = endTime.Add(time.Hour * -24).Add(time.Second * 1)

	//query db for signal trades over the period
	results, err := db.GetStrategyClosedTradesByTimeRange(strategyName, beginTime, endTime, nil)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get daily closed trades from db")
		return nil, err
	}

	return results, nil
}

//GetWeeklyClosedTrades retrieves details on all trades which closed for current week, based on weekly trading window
func GetWeeklyClosedTrades(nextWeekSpotTime time.Time, strategyName string) (map[string]*model.SignalTrade, error) {
	// -- determine report begin/end times --
	var beginTime, endTime time.Time

	windowStart, windowEnd, isWithinWindow := util.GetCurrentTradingWindow(nextWeekSpotTime)
	if isWithinWindow {
		beginTime = windowStart
		endTime = windowEnd
	} else {
		//if after window, subtract a week to get previously closed week's times
		beginTime = windowStart.Add(time.Hour * 24 * -7)
		endTime = windowEnd.Add(time.Hour * 24 * -7)
	}

	//query db for signal trades over the period
	results, err := db.GetStrategyClosedTradesByTimeRange(strategyName, beginTime, endTime, nil)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get weely closed trades from db")
		return nil, err
	}

	return results, nil
}
