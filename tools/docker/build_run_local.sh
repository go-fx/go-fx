# Optional first argument for environment file name within env directory, else defaults to dev.env
ENV_FILE=$1
if [ -z "$1" ]
then
    ENV_FILE="dev.env"
fi

# Optional second argument for dockerfile name within tools/docker directory, else defaults to release.dockerfile
DOCKER_FILE=$2
if [ -z "$2" ]
then
    DOCKER_FILE="release.dockerfile"
fi

docker build -t go-fx/go-fx:latest -f tools/docker/dockerfiles/$DOCKER_FILE . || exit 1

docker run -d --rm --env-file env/$ENV_FILE --name go-fx go-fx/go-fx:latest start

docker logs -f go-fx
