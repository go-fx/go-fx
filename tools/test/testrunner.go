package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

var (
	coverMode        = "atomic"
	covOutputDir     = "test_coverage"
	overallCovFile   = "coverage.cov"
	overallCovReport = "coverage.html"
	tempCovFile      = "tmp.cov"
	pkgIgnoreFile    = "./tools/test/testpkg.grepignore"
	pkgList          = []string{}
)

func main() {
	// Flag support
	argsFlag := flag.String("args", "", "String of arguments passed to 'go test'.  Packages appended unless 'np' flag provided.")
	gateCov := flag.Int("gate-cov", 0, "Activate gate that fails if overall mean coverage is below specified value (1 to 100).  Enables 'cov'.")
	isCov := flag.Bool("cov", false, "Perform test coverage, generate reporting, then exit.  Not needed if gate-cov > 0.")
	isNoPkgs := flag.Bool("np", false, "No packages.  Test with go test + args only; args can be packages.")
	isPkgsOnly := flag.Bool("pp", false, "Only print packages that would be included for testing, then exit.")
	isVerbose := flag.Bool("v", false, "Writes additional output if available.")
	isGateFiles := flag.Bool("gate-files", false, "Activate gate that fails if any included packages have no test files.")

	// Parse flags
	flag.Parse()

	// Unless 'no-pkgs' flag is set, get package list from all packages, minus those in .testignored
	if !*isNoPkgs || *isPkgsOnly {
		pkgList = getPkgList()
	}

	// Handle presence of 'pkgs-only' flag
	if *isPkgsOnly {
		if *isVerbose {
			fmt.Printf("\n>> Line-separated\n\n%s\n\n", strings.Join(pkgList, "\n"))
			fmt.Printf("\n>> Space-separated\n\n%s\n\n", strings.Join(pkgList, " "))
		} else {
			fmt.Print(strings.Join(pkgList, " "))
		}
		os.Exit(0)
	}

	// If no cov, perform 'go test' command with args+pkgList only
	if !*isCov && *gateCov == 0 {

		// Run test
		testScript := "go test "
		if len(*argsFlag) > 0 {
			testScript += fmt.Sprintf("%s ", *argsFlag)
		}

		// Unless 'no-pkgs' is set or pkgList is empty, append pkgList to testScript
		if !*isNoPkgs && len(pkgList) > 0 {
			testScript += strings.Join(pkgList, " ")
		}

		// Print test script
		fmt.Println(testScript)

		// Execute test script
		testCmd := exec.Command("bash", "-c", testScript)
		testResults, err := testCmd.Output()
		if err != nil {
			log.Fatalf("go test command failed with %s\n", err)
		}
		fmt.Println(string(testResults))

		// Check isGateFiles; fail if any included packages have no test files
		if *isGateFiles {
			err := runisGateFiles(string(testResults), pkgList)
			fmt.Print("\nGate for test files is ACTIVE.  Checking for packages with no test files...")
			if err != nil {
				fmt.Println(err)
				os.Exit(2)
			}
			fmt.Print("gate passed!\n\n>>  All included packages have at least 1 test file\n\n")
		}

		// exit
		os.Exit(0)
	}

	// Code Coverage - 'gateCov' > 0 || isCov

	// Create the coverage files directory
	os.MkdirAll(covOutputDir, 0755)
	os.Chdir(covOutputDir)

	// track goToolCoverResults for overall coverage calculation
	overallTestResults, overallCoverageResults := "", ""

	for idx, pkg := range pkgList {
		// print current pkg name
		fmt.Println(pkg)

		// Run test coverage
		goTestCoverScript := fmt.Sprintf("go test -covermode=%s -coverprofile %s %s", coverMode, tempCovFile, pkg)
		goTestCmd := exec.Command("bash", "-c", goTestCoverScript)
		testResults, err := goTestCmd.Output()
		if err != nil {
			log.Fatalf("go test command failed with %s\n", err)
		}
		overallTestResults += string(testResults)
		fmt.Println(string(testResults))

		// Run cover tool
		goToolCoverScript := fmt.Sprintf("go tool cover -func=%s", tempCovFile)
		goToolCoverCmd := exec.Command("bash", "-c", goToolCoverScript)
		goToolCoverResults, err := goToolCoverCmd.Output()
		if err != nil {
			log.Fatalf("go tool cover command failed with %s\n", err)
		}
		overallCoverageResults += string(goToolCoverResults)
		fmt.Println(string(goToolCoverResults))

		// append header of overall cov file, if first
		if idx == 0 {
			overallCovFileScript := fmt.Sprintf("head -n 1 %s > %s", tempCovFile, overallCovFile)
			overallCovFileCmd := exec.Command("bash", "-c", overallCovFileScript)
			_, err := overallCovFileCmd.Output()
			if err != nil {
				log.Fatalf("append to temp coverage file failed with %s\n", err)
			}
		}

		// Append rest of output to overall cov file
		appendOverallCovScript := fmt.Sprintf("tail -n +2 '%s' >> %s", tempCovFile, overallCovFile)
		appendOverallCovCmd := exec.Command("bash", "-c", appendOverallCovScript)
		_, err = appendOverallCovCmd.Output()
		if err != nil {
			log.Fatalf("append to overall cov file failed with %s\n", err)
		}

		// Remove temporary coverage file
		_, err = exec.Command("rm", tempCovFile).Output()
		if err != nil {
			log.Fatalf("removing temporary coverage file failed with %s\n", err)
		}
	}

	// Produce an HTML report with code coverage from overall cov file
	owCovReportScript := `echo "" > ` + overallCovReport
	_, err := exec.Command("bash", "-c", owCovReportScript).Output()
	if err != nil {
		log.Fatalf("overwrite overall cov report file failed with %s\n", err)
	}

	// Generate go coverage HTML report
	htmlCovReportScript := fmt.Sprintf("go tool cover -html=%s -o %s || true ;", overallCovFile, overallCovReport)
	_, err = exec.Command("bash", "-c", htmlCovReportScript).Output()
	if err != nil {
		log.Fatalf("generate html coverage report failed with %s\n", err)
	}

	// Calculate overall coverage percentage based on total goToolCoverResults
	statementCount, percentTotal := 0, 0.
	regx, _ := regexp.Compile(`^.+:\d+:\s.+\s(\d+\.\d+)%$`)
	overallCoverageResultLines := strings.Split(overallCoverageResults, "\n")
	for i := 0; i < len(overallCoverageResultLines); i++ {
		if isMatch := regx.MatchString(overallCoverageResultLines[i]); isMatch {
			percentMatches := regx.FindStringSubmatch(overallCoverageResultLines[i])
			percent, _ := strconv.ParseFloat(percentMatches[1], 64)
			percentTotal += percent
			statementCount++
		}
	}

	// Prevent divide by zero
	if statementCount == 0 {
		statementCount = 1
	}

	// Output overall (mean) coverage percentage
	meanCov := int(math.Round(percentTotal / float64(statementCount)))
	fmt.Println(fmt.Sprintf(`
	Overall Test Coverage Results
	=============================
	total average coverage: %d%%
	`, meanCov))

	// Check gateCov; fail if mean coverage below provided gate value
	if *gateCov > 0 {
		fmt.Printf("\nGate for mean coverage is ACTIVE.  Gating mean coverage @ %d%%...", *gateCov)
		err := runGateMCov(meanCov, *gateCov)
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
		fmt.Printf("gate passed!\n\n>>  Actual mean coverage %d%%, gate: %d%%\n\n", meanCov, *gateCov)
	}

	// Check isGateFiles; fail if any included packages have no test files
	if *isGateFiles {
		fmt.Print("\nGate for test files is ACTIVE.  Checking for packages with no test files...")
		err := runisGateFiles(overallTestResults, pkgList)
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
		fmt.Print("gate passed!\n\n>>  All included packages have at least 1 test file\n\n")
	}
}

func getPkgList() []string {
	// Get pkg list, grepping test ignored pkgs
	ignoredPkgLines, err := ioutil.ReadFile(pkgIgnoreFile)
	if err != nil {
		// could not read pkgIgnoreFile
		log.Printf("WARNING: Could not find/read file '%s'", pkgIgnoreFile)
		log.Printf("..including all pkgs for test coverage")
	}
	ignoredPkgsInput := strings.Split(string(ignoredPkgLines), "\n")
	ignoredPkgs := []string{}
	ignoreCounter := 0
	for _, grep := range ignoredPkgsInput {
		// skip lines that are blank/whitespace or start with '#' (comment)
		if grepTrimmed := strings.TrimSpace(grep); grepTrimmed == "" || grepTrimmed[:1] == "#" {
			continue
		}
		// increment ignoreCounter
		ignoreCounter++

		// append non-empty grep command string
		ignoredPkgs = append(ignoredPkgs, fmt.Sprintf("-e %s ", strings.TrimSpace(grep)))
	}

	covPkgCmd := "go list ./..."
	if len(ignoredPkgs) > 0 {
		covPkgCmd = fmt.Sprintf("%s | grep -v %s", covPkgCmd, strings.Join(ignoredPkgs, ""))
	}

	pkgCmd := exec.Command("bash", "-c", covPkgCmd)
	includedPkgLines, err := pkgCmd.Output()
	if err != nil {
		log.Fatalf("go list grep command failed with %s\n", err)
	}
	//log.Printf("go list output:\n%s\n", string(includedPkgLines))

	// split to slice and exclude final (blank) element
	includedPkgs := strings.Split(string(includedPkgLines), "\n")

	return includedPkgs[:len(includedPkgs)-1]
}

func runGateMCov(meanCoverage, gateCoverage int) error {
	errTemplate := "gate failed!\n\n>>  Actual mean coverage %d%%, gate: %d%%\n"

	// Fail if gated and coverage is below gate
	switch {
	// Valid cases
	case gateCoverage >= 0 && gateCoverage <= 100:
		if meanCoverage < gateCoverage {
			return fmt.Errorf(errTemplate, meanCoverage, gateCoverage)
		}
	// Invalid cases
	default:
		log.Fatal("error: coverage gate must be from 1 to 100 (%)")
	}

	// gate passed
	return nil
}

func runisGateFiles(testResults string, pkgList []string) error {
	// err vars
	errTemplate := "gate failed!\n\n>>  Packages found without test files:\n\n%s\n"
	noTestFilesRegx, _ := regexp.Compile(`(\[no test files\])`)
	noFilePkgs := []string{}

	// split lines from testResults into slice
	testResultLines := strings.Split(testResults, "\n")

	// search for no test files in results
	for lineNo, idxPkgList := 0, 0; lineNo < len(testResultLines) && idxPkgList < len(pkgList); lineNo++ {
		// if 'no test files' pattern is found, add current package to noFilePkgs
		testPkgRegx, _ := regexp.Compile(fmt.Sprintf(`^(\?|ok)\s+%s\s+.+`, pkgList[idxPkgList]))
		if testPkgRegx.MatchString(testResultLines[lineNo]) {
			// actual check for 'no test files' message in output
			if noTestFilesRegx.MatchString(testResultLines[lineNo]) {
				noFilePkgs = append(noFilePkgs, pkgList[idxPkgList])
			}

			// increment to look for next included package
			idxPkgList++
		}
	}

	// if any noFilePkgs, return error with the list
	if len(noFilePkgs) > 0 {
		return fmt.Errorf(errTemplate, strings.Join(noFilePkgs, "\n"))
	}

	// gate passed, no err found
	return nil
}
