module gofx

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/sirupsen/logrus v1.5.0
)
