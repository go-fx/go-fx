package env

import (
	"time"
)

const (
	//MarketHoursOpenDay is the DayOfWeek on which FX market opens
	MarketHoursOpenDay time.Weekday = time.Sunday

	//MarketHoursOpenHour is the Hour (24-hour scale) on which FX market opens
	MarketHoursOpenHour int = 17

	//MarketHoursCloseDay is the DayOfWeek on which FX market closes
	MarketHoursCloseDay time.Weekday = time.Friday

	//MarketHoursCloseHour is the Hour (24-hour scale) on which FX market closes
	MarketHoursCloseHour int = 17

	//MarketHoursLocationName is the name of the location in which MarketHours values should be considered
	MarketHoursLocationName string = "America/New_York"
)

//MarketHours contain FX market hours of operation
type MarketHours struct {
	MarketHoursOpenDay   time.Weekday
	MarketHoursOpenHour  int
	MarketHoursCloseDay  time.Weekday
	MarketHoursCloseHour int
	MarketHoursLocation  *time.Location
}

//GetMarketHours returns FX market hours of operation
func GetMarketHours() MarketHours {
	hours := MarketHours{
		MarketHoursOpenDay:   MarketHoursOpenDay,
		MarketHoursOpenHour:  MarketHoursOpenHour,
		MarketHoursCloseDay:  MarketHoursCloseDay,
		MarketHoursCloseHour: MarketHoursCloseHour,
	}
	hours.MarketHoursLocation, _ = time.LoadLocation(MarketHoursLocationName)

	return hours
}
