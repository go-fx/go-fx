package env

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"gofx/model/instruments"
)

var config *Config

//Env represents the current configured environment of the server
type Env int

//Name returns the name of the environment
func (e Env) Name() string {
	switch e {
	case EnvDev:
		return "DEV"
	case EnvStage:
		return "STAGE"
	case EnvProd:
		return "PROD"
	default:
		return ""
	}
}

const (
	//EnvDev indicates the server is configured for the dev environment
	EnvDev Env = iota + 1

	//EnvStage indicates the server is configured for the stage environment
	EnvStage

	//EnvProd indicates the server is configured for the prod environment
	EnvProd
)

//Config is the app config
type Config struct {
	AppHostProtocol                   string
	AppHostName                       string
	AppName                           string
	AppCompany                        string
	AppCompanyLink                    string
	ServerEnv                         Env
	ServerPort                        string
	ServerLogLevel                    logrus.Level
	DBHost                            string
	DBPort                            string
	DBUsername                        string
	DBPassword                        string
	DBSchema                          string
	DBScripts                         string
	StrategyName                      string
	StrategyAlias                     string
	StrategyMaxTrades                 int
	StrategyMarginUtilPerTrade        float64
	StrategyMinSignallProbability     float64
	StrategyMinTradeDurationHours     int
	StrategyMaxTradeDurationHours     int
	StrategyMaxTradesPerSignal        int
	StrategyInstruments               []instruments.Instrument
	StrategyIntervalMinutes           int
	StrategyWindowEarningsGoal        float64
	StrategyTradeEarningsThreshold    float64
	TradeWindowOpenDay                time.Weekday
	TradeWindowOpenHour               int
	TradeWindowOpenMinute             int
	TradeWindowCloseDay               time.Weekday
	TradeWindowCloseHour              int
	TradeWindowCloseMinute            int
	TradeDayClosingHour               int
	TradeDayClosingMinute             int
	TradeTickerWaitingSeconds         int
	TradeAPIAccountID                 string
	TradeAPIAuthHeader                string
	TradeAPIEndpointGetSignals        string
	TradeAPIEndpointGetAccount        string
	TradeAPIEndpointCreateOrder       string
	TradeAPIEndpointGetOrders         string
	TradeAPIEndpointUpdateOrder       string
	TradeAPIEndpointCancelOrder       string
	TradeAPIEndpointGetOpenTrades     string
	TradeAPIEndpointGetTradeByID      string
	TradeAPIEndpointUpdateTradeOrders string
	TradeAPIEndpointCloseTrade        string
	TradeAPIEndpointGetOpenPositions  string
	TradeAPIEndpointClosePosition     string
	TradeAPIEndpointGetPricing        string
	TradeAPIEndpointGetInstruments    string
	AlertOpenWindowInMinutes          int
	AlertCloseWindowInMinutes         int
	ProviderEmailName                 string
	ProviderEmailAuthHeader           string
	ProviderEmailFromAddress          string
	ProviderEmailToAddress            string
	ProviderEmailEndpointSendTX       string
}

//Configure sets up the global configuration for the server
func Configure() {
	//APP_HOST_PROTOCOL
	if val, isPresent := os.LookupEnv("APP_HOST_PROTOCOL"); isPresent {
		config.AppHostProtocol = val
	}

	//APP_HOST_NAME
	if val, isPresent := os.LookupEnv("APP_HOST_NAME"); isPresent {
		config.AppHostName = val
	}

	//APP_NAME
	if val, isPresent := os.LookupEnv("APP_NAME"); isPresent {
		config.AppName = val
	}

	//APP_COMPANY
	if val, isPresent := os.LookupEnv("APP_COMPANY"); isPresent {
		config.AppCompany = val
	}

	//APP_COMPANY_LINK
	if val, isPresent := os.LookupEnv("APP_COMPANY_LINK"); isPresent {
		config.AppCompanyLink = val
	}

	//SERVER_ENV
	if val, isPresent := os.LookupEnv("SERVER_ENV"); isPresent {
		switch strings.ToLower(val) {
		case "dev":
			config.ServerEnv = EnvDev
		case "stage":
			config.ServerEnv = EnvStage
		case "prod":
			config.ServerEnv = EnvProd
		default:
			panic("invalid SERVER_ENV detected: " + val)
		}
	} else {
		config.ServerEnv = EnvDev
	}

	//SERVER_PORT
	if val, isPresent := os.LookupEnv("SERVER_PORT"); isPresent {
		config.ServerPort = val
	} else {
		config.ServerPort = "8080"
	}

	//SERVER_LOG_LEVEL
	if val, isPresent := os.LookupEnv("SERVER_LOG_LEVEL"); isPresent {
		switch strings.ToLower(val) {
		case "trace":
			config.ServerLogLevel = logrus.TraceLevel
		case "debug":
			config.ServerLogLevel = logrus.DebugLevel
		case "info":
			config.ServerLogLevel = logrus.InfoLevel
		case "warn":
			config.ServerLogLevel = logrus.WarnLevel
		case "error":
			config.ServerLogLevel = logrus.ErrorLevel
		case "fatal":
			config.ServerLogLevel = logrus.FatalLevel
		case "panic":
			config.ServerLogLevel = logrus.PanicLevel
		default:
			panic("invalid SERVER_LOG_LEVEL detected: " + val)
		}
	} else {
		config.ServerLogLevel = logrus.TraceLevel
	}

	//DB_HOST
	if val, isPresent := os.LookupEnv("DB_HOST"); isPresent {
		config.DBHost = val
	}
	//DB_PORT
	if val, isPresent := os.LookupEnv("DB_PORT"); isPresent {
		config.DBPort = val
	}
	//DB_USERNAME
	if val, isPresent := os.LookupEnv("DB_USERNAME"); isPresent {
		config.DBUsername = val
	}
	//DB_PASSWORD
	if val, isPresent := os.LookupEnv("DB_PASSWORD"); isPresent {
		config.DBPassword = val
	}
	//DB_SCHEMA
	if val, isPresent := os.LookupEnv("DB_SCHEMA"); isPresent {
		config.DBSchema = val
	}

	//DB_SCRIPTS
	if val, isPresent := os.LookupEnv("DB_SCRIPTS"); isPresent {
		config.DBScripts = val
	}

	//STRATEGY_NAME
	if val, isPresent := os.LookupEnv("STRATEGY_NAME"); isPresent {
		config.StrategyName = val
	}

	//STRATEGY_ALIAS
	if val, isPresent := os.LookupEnv("STRATEGY_ALIAS"); isPresent {
		config.StrategyAlias = val
	}

	//STRATEGY_MAX_TRADES
	if val, isPresent := os.LookupEnv("STRATEGY_MAX_TRADES"); isPresent {
		if maxTrades, err := strconv.Atoi(val); err == nil && maxTrades >= 0 {
			config.StrategyMaxTrades = maxTrades
		} else {
			panic(fmt.Errorf("invalid strategy max trades value %s", val))
		}
	}

	//STRATEGY_MARGIN_UTIL_PER_TRADE
	if val, isPresent := os.LookupEnv("STRATEGY_MARGIN_UTIL_PER_TRADE"); isPresent {
		if marginUtil, err := strconv.ParseFloat(val, 64); err == nil && marginUtil > 0. && marginUtil < 1. {
			config.StrategyMarginUtilPerTrade = marginUtil
		} else {
			panic(fmt.Errorf("invalid strategy margin util per trade value %s", val))
		}
	}

	//STRATEGY_MIN_SIGNAL_PROBABILITY
	if val, isPresent := os.LookupEnv("STRATEGY_MIN_SIGNAL_PROBABILITY"); isPresent {
		if minProbability, err := strconv.ParseFloat(val, 64); err == nil && minProbability > 0. && minProbability < 1. {
			config.StrategyMinSignallProbability = minProbability
		} else {
			panic(fmt.Errorf("invalid strategy min signal probability value %s", val))
		}
	}

	//STRATEGY_MIN_TRADE_DURATION_HOURS
	if val, isPresent := os.LookupEnv("STRATEGY_MIN_TRADE_DURATION_HOURS"); isPresent {
		if minHours, err := strconv.Atoi(val); err == nil && minHours >= 0 {
			config.StrategyMinTradeDurationHours = minHours
		} else {
			panic(fmt.Errorf("invalid strategy min trade duration value %s", val))
		}
	}

	//STRATEGY_MAX_TRADE_DURATION_HOURS
	if val, isPresent := os.LookupEnv("STRATEGY_MAX_TRADE_DURATION_HOURS"); isPresent {
		if maxHours, err := strconv.Atoi(val); err == nil && maxHours > 0 {
			config.StrategyMaxTradeDurationHours = maxHours
		} else {
			panic(fmt.Errorf("invalid strategy max trade duration value %s", val))
		}
	}

	//STRATEGY_MAX_TRADES_PER_SIGNAL
	if val, isPresent := os.LookupEnv("STRATEGY_MAX_TRADES_PER_SIGNAL"); isPresent {
		if maxTrades, err := strconv.Atoi(val); err == nil && maxTrades > 0 {
			config.StrategyMaxTradesPerSignal = maxTrades
		} else {
			panic(fmt.Errorf("invalid strategy max trades per signal value %s", val))
		}
	}

	//STRATEGY_INSTRUMENTS
	if val, isPresent := os.LookupEnv("STRATEGY_INSTRUMENTS"); isPresent {
		//parse strategy instrument list
		instNames := regexp.MustCompile(`,\s*`).Split(val, -1)
		if len(instNames) < 1 {
			panic("no instrument names received for strategy!")
		}

		config.StrategyInstruments = make([]instruments.Instrument, 0)
		for _, name := range instNames {
			if inst := instruments.ParseInstrument(name); inst != 0 {
				config.StrategyInstruments = append(config.StrategyInstruments, inst)
			} else {
				panic(fmt.Errorf("invalid instrument found in config: %s", name))
			}
		}
	}

	//STRATEGY_INTERVAL_MINUTES
	if val, isPresent := os.LookupEnv("STRATEGY_INTERVAL_MINUTES"); isPresent {
		if intervalMins, err := strconv.Atoi(val); err == nil && intervalMins > 0 {
			config.StrategyIntervalMinutes = intervalMins
		} else {
			panic(fmt.Errorf("invalid strategy interval minutes value %s", val))
		}
	}

	//STRATEGY_WINDOW_EARNINGS_GOAL
	if val, isPresent := os.LookupEnv("STRATEGY_WINDOW_EARNINGS_GOAL"); isPresent {
		if goal, err := strconv.ParseFloat(val, 64); err == nil && goal > 0. {
			config.StrategyWindowEarningsGoal = goal
		} else {
			panic(fmt.Errorf("invalid strategy window earnings goal %s", val))
		}
	}

	//STRATEGY_TRADE_EARNINGS_THRESHOLD
	if val, isPresent := os.LookupEnv("STRATEGY_TRADE_EARNINGS_THRESHOLD"); isPresent {
		if threshold, err := strconv.ParseFloat(val, 64); err == nil && threshold >= 0. && threshold < 1. {
			config.StrategyTradeEarningsThreshold = threshold
		} else {
			panic(fmt.Errorf("invalid strategy trade earnings threshold %s", val))
		}
	}

	//TRADE_WINDOW_OPEN_DAY
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_OPEN_DAY"); isPresent {
		if day, err := strconv.Atoi(val); err == nil && time.Weekday(day) >= time.Sunday && time.Weekday(day) <= time.Saturday {
			config.TradeWindowOpenDay = time.Weekday(day)
		} else {
			panic(fmt.Errorf("invalid trade window open day value %s, %w", val, err))
		}
	}

	//TRADE_WINDOW_OPEN_HOUR
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_OPEN_HOUR"); isPresent {
		if hour, err := strconv.Atoi(val); err == nil && hour >= 0 && hour <= 23 {
			config.TradeWindowOpenHour = hour
		} else {
			panic(fmt.Errorf("invalid trade window open hour %s, %w", val, err))
		}
	}

	//TRADE_WINDOW_OPEN_MINUTE
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_OPEN_MINUTE"); isPresent {
		if min, err := strconv.Atoi(val); err == nil && min >= 0 && min <= 59 {
			config.TradeWindowOpenMinute = min
		} else {
			panic(fmt.Errorf("invalid trade window open minute %s, %w", val, err))
		}
	}

	//TRADE_WINDOW_CLOSE_DAY
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_CLOSE_DAY"); isPresent {
		if day, err := strconv.Atoi(val); err == nil && time.Weekday(day) >= time.Sunday && time.Weekday(day) <= time.Saturday {
			config.TradeWindowCloseDay = time.Weekday(day)
		} else {
			panic(fmt.Errorf("invalid trade window close day value %s, %w", val, err))
		}
	}

	//TRADE_WINDOW_CLOSE_HOUR
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_CLOSE_HOUR"); isPresent {
		if hour, err := strconv.Atoi(val); err == nil && hour >= 0 && hour <= 23 {
			config.TradeWindowCloseHour = hour
		} else {
			panic(fmt.Errorf("invalid trade window close hour %d, %w", hour, err))
		}
	}

	//TRADE_WINDOW_CLOSE_MINUTE
	if val, isPresent := os.LookupEnv("TRADE_WINDOW_CLOSE_MINUTE"); isPresent {
		if min, err := strconv.Atoi(val); err == nil && min >= 0 && min <= 59 {
			config.TradeWindowCloseMinute = min
		} else {
			panic(fmt.Errorf("invalid trade window close minute %d, %w", min, err))
		}
	}

	//TRADE_DAY_CLOSING_HOUR
	if val, isPresent := os.LookupEnv("TRADE_DAY_CLOSING_HOUR"); isPresent {
		if min, err := strconv.Atoi(val); err == nil && min >= 0 && min <= 59 {
			config.TradeDayClosingHour = min
		} else {
			panic(fmt.Errorf("invalid trade day closing hour %d, %w", min, err))
		}
	}

	//TRADE_DAY_CLOSING_MINUTE
	if val, isPresent := os.LookupEnv("TRADE_DAY_CLOSING_MINUTE"); isPresent {
		if min, err := strconv.Atoi(val); err == nil && min >= 0 && min <= 59 {
			config.TradeDayClosingMinute = min
		} else {
			panic(fmt.Errorf("invalid trade trade day closing minute %d, %w", min, err))
		}
	}

	//TRADE_TICKER_WAITING_SECONDS
	if val, isPresent := os.LookupEnv("TRADE_TICKER_WAITING_SECONDS"); isPresent {
		if sec, err := strconv.Atoi(val); err == nil && sec > 0 {
			config.TradeTickerWaitingSeconds = sec
		} else {
			panic(fmt.Errorf("invalid trade ticker waiting seconds %d, %w", sec, err))
		}
	}

	//TRADE_API_ACCOUNT_ID
	if val, isPresent := os.LookupEnv("TRADE_API_ACCOUNT_ID"); isPresent {
		config.TradeAPIAccountID = val
	}

	//TRADE_API_AUTH_HEADER
	if val, isPresent := os.LookupEnv("TRADE_API_AUTH_HEADER"); isPresent {
		config.TradeAPIAuthHeader = val
	}

	//TRADE_API_ENDPOINT_GET_SIGNALS
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_SIGNALS"); isPresent {
		config.TradeAPIEndpointGetSignals = val
	}

	//TRADE_API_ENDPOINT_GET_ACCOUNT
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_ACCOUNT"); isPresent {
		config.TradeAPIEndpointGetAccount = val
	}

	//TRADE_API_ENDPOINT_CREATE_ORDER
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_CREATE_ORDER"); isPresent {
		config.TradeAPIEndpointCreateOrder = val
	}

	//TRADE_API_ENDPOINT_GET_ORDERS
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_ORDERS"); isPresent {
		config.TradeAPIEndpointCreateOrder = val
	}

	//TRADE_API_ENDPOINT_UPDATE_ORDER
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_UPDATE_ORDER"); isPresent {
		config.TradeAPIEndpointUpdateOrder = val
	}

	//TRADE_API_ENDPOINT_CANCEL_ORDER
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_CANCEL_ORDER"); isPresent {
		config.TradeAPIEndpointCancelOrder = val
	}

	//TRADE_API_ENDPOINT_GET_OPEN_TRADES
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_OPEN_TRADES"); isPresent {
		config.TradeAPIEndpointGetOpenTrades = val
	}

	//TRADE_API_ENDPOINT_GET_TRADE_BY_ID
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_TRADE_BY_ID"); isPresent {
		config.TradeAPIEndpointGetTradeByID = val
	}

	//TRADE_API_ENDPOINT_UPDATE_TRADE_ORDERS
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_UPDATE_TRADE_ORDERS"); isPresent {
		config.TradeAPIEndpointUpdateTradeOrders = val
	}

	//TRADE_API_ENDPOINT_CLOSE_TRADE
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_CLOSE_TRADE"); isPresent {
		config.TradeAPIEndpointCloseTrade = val
	}

	//TRADE_API_ENDPOINT_GET_OPEN_POSITIONS
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_OPEN_POSITIONS"); isPresent {
		config.TradeAPIEndpointGetOpenPositions = val
	}

	//TRADE_API_ENDPOINT_CLOSE_POSITION
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_CLOSE_POSITION"); isPresent {
		config.TradeAPIEndpointClosePosition = val
	}

	//TRADE_API_ENDPOINT_GET_PRICING
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_PRICING"); isPresent {
		config.TradeAPIEndpointGetPricing = val
	}

	//TRADE_API_ENDPOINT_GET_INSTRUMENTS
	if val, isPresent := os.LookupEnv("TRADE_API_ENDPOINT_GET_INSTRUMENTS"); isPresent {
		config.TradeAPIEndpointGetInstruments = val
	}

	//ALERT_OPEN_WINDOW_IN_MINUTES
	if val, isPresent := os.LookupEnv("ALERT_OPEN_WINDOW_IN_MINUTES"); isPresent {
		if min, err := strconv.Atoi(val); err != nil {
			panic(fmt.Errorf("invalid alert open window in minutes %d, %w", min, err))
		} else {
			config.AlertOpenWindowInMinutes = min
		}
	}

	//ALERT_CLOSE_WINDOW_IN_MINUTES
	if val, isPresent := os.LookupEnv("ALERT_CLOSE_WINDOW_IN_MINUTES"); isPresent {
		if min, err := strconv.Atoi(val); err != nil {
			panic(fmt.Errorf("invalid alert close window in minutes %d, %w", min, err))
		} else {
			config.AlertCloseWindowInMinutes = min
		}
	}

	//PROVIDER_EMAIL_NAME
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_NAME"); isPresent {
		config.ProviderEmailName = val
	}

	//PROVIDER_EMAIL_AUTH_HEADER
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_AUTH_HEADER"); isPresent {
		config.ProviderEmailAuthHeader = val
	}

	//PROVIDER_EMAIL_FROM_ADDRESS
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_FROM_ADDRESS"); isPresent {
		config.ProviderEmailFromAddress = val
	}

	//PROVIDER_EMAIL_TO_ADDRESS
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_TO_ADDRESS"); isPresent {
		config.ProviderEmailToAddress = val
	}

	//PROVIDER_EMAIL_ENDPOINT_SEND_TX
	if val, isPresent := os.LookupEnv("PROVIDER_EMAIL_ENDPOINT_SEND_TX"); isPresent {
		config.ProviderEmailEndpointSendTX = val
	}
}

//GetConfig returns a copy of the configured environment, returning a copy makes it read-only towards original config
func GetConfig() *Config {
	if config == nil {
		config = DefaultConfig
		Configure()
	}

	//return read-only copy
	envCopy := *config

	//copy pointer/reference values
	envCopy.StrategyInstruments = make([]instruments.Instrument, 0)
	if config.StrategyInstruments != nil {
		for _, inst := range config.StrategyInstruments {
			envCopy.StrategyInstruments = append(envCopy.StrategyInstruments, inst)
		}
	}

	return &envCopy
}
