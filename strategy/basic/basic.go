package basic

import (
	"fmt"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"time"

	gofxhttp "gofx/client/http"
	"gofx/client/util"
	"gofx/db"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model"
	"gofx/model/instruments"
	"gofx/model/oanda"
	"gofx/strategy"

	"github.com/sirupsen/logrus"
)

const defaultName string = "Basic"

//Basic is a basic strategy included with the project
type Basic struct {
	Instruments []instruments.Instrument

	//maximum number of trades this strategy should open
	maxTrades int

	//target utilization (% out of 1) per trade
	utilizationPerTrade float64

	//minimum probability (% out of 1) to accept when considering signals
	minSignalProbability float64

	//name is the dynamic strategy name
	name string
}

//Init initializes the strategy
func (b *Basic) Init() {
	//set name as default if unset
	if len(b.name) == 0 {
		b.name = defaultName
	}

	//instrument list for strategy
	b.Instruments = env.GetConfig().StrategyInstruments

	//maximum number of trades this strategy should open
	b.maxTrades = env.GetConfig().StrategyMaxTrades

	//target utilization (% out of 1) per trade
	b.utilizationPerTrade = env.GetConfig().StrategyMarginUtilPerTrade

	//minimum probability (% out of 1) to accept when considering signals
	b.minSignalProbability = env.GetConfig().StrategyMinSignallProbability
}

//Name returns a display name for this strategy
func (b *Basic) Name() string {
	return b.name
}

//SetName sets a display name for this strategy
func (b *Basic) SetName(name string) bool {
	if len(name) == 0 {
		return false
	}

	b.name = name

	return true
}

//Title returns a display title for this strategy
func (b *Basic) Title() string {
	return `

	██████╗      █████╗      ██████╗    ██╗     ██████╗
	██╔══██╗    ██╔══██╗    ██╔════╝    ██║    ██╔════╝
	██████╔╝    ███████║    ███████╗    ██║    ██║     
	██╔══██╗    ██╔══██║    ╚════██║    ██║    ██║     
	██████╔╝    ██║  ██║    ███████║    ██║    ╚██████╗
	╚═════╝     ╚═╝  ╚═╝    ╚══════╝    ╚═╝     ╚═════╝
		
        Strategy v1.4.1

	`
}

//Interval returns the interval of time between action decisions
func (b *Basic) Interval() time.Duration {
	return time.Minute * time.Duration(env.GetConfig().StrategyIntervalMinutes)
}

//Run is the interface method that executes a strategy for a time interval
func (b *Basic) Run(signals []*oanda.AutochartistSignal) []strategy.Action {
	//actions to perform this interval
	actions := make([]strategy.Action, 0)

	//1. Get open trades from broker
	openTrades, err := db.GetOpenStrategySignalTrades(b.Name(), nil)
	ts := loggers.GetTimestamp()
	if err != nil {
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("Could not retrieve open trades.  Aborting...")
		return nil
	}
	loggers.GetJSONLogger().WithField("ts", ts).WithField("openTrades", len(openTrades)).Trace("Retrieved open trades successfully")

	//2. Close trades below min earnings threshold
	if len(openTrades) > 0 {
		actions = append(actions, b.closeOpenTradesBelowEarningsThreshold(openTrades, &http.Client{})...)
	}

	//3. Check if there is room for any more open trades (after closing any trades from #2)
	tradesRemainingOpen := len(openTrades) - len(actions)
	if tradesRemainingOpen >= b.maxTrades {
		loggers.GetJSONLogger().WithField("ts", ts).WithField("tradesRemainingOpen", tradesRemainingOpen).Trace("Max trades reached...")
		return actions
	}
	loggers.GetJSONLogger().WithField("ts", ts).WithField("tradesRemainingOpen", tradesRemainingOpen).Trace("Able to open more trades...")

	//4. Create set of instrumets involved in open signals
	openInstruments := make(map[string]struct{})
	for _, trade := range openTrades {
		openInstruments[trade.Instrument.Name()] = struct{}{}
	}

	//5. Filter signals by min probability and complete
	rankedSignals := make(SignalsByRanking, 0)
	for _, sig := range signals {
		if _, isOpen := openInstruments[sig.Instrument]; !isOpen && sig.Meta.Completed == 1 &&
			sig.Meta.Probability >= b.minSignalProbability*100 && sig.Meta.HistoricalStats.Symbol.Percent >= b.minSignalProbability*100 &&
			sig.Meta.HistoricalStats.Pattern.Percent >= b.minSignalProbability*100 && sig.Meta.HistoricalStats.HourOfDay.Percent >= b.minSignalProbability*100 {

			//convert broker signal to model signal
			basicSig := model.NewAutochartistSignal(sig)

			//check avg scores also meet minimum probability
			var qualityScore float64
			if sig.Meta.Scores != nil {
				qualityScore = getWeightedMeanQualityScore(*basicSig.Meta.Scores)
			}

			//filter if quality score is below minimum based on min probability (out of 50)
			if qualityScore >= b.minSignalProbability*50 {
				rankedSignals = append(rankedSignals, &struct {
					signal       *model.AutochartistSignal
					qualityScore float64
				}{
					signal:       basicSig,
					qualityScore: qualityScore,
				})
			}
		}
	}

	//6. Sort ranked signals by ranking
	sort.Sort(rankedSignals)

	//log sorted/ranked signals if TRACE level logging
	if loggers.GetJSONLogger().Level == logrus.TraceLevel {
		ts := loggers.GetTimestamp()
		for idx, signal := range rankedSignals {
			loggers.GetJSONLogger().WithField("ts", ts).WithField("signalID", signal.signal.ID).WithField("ranking", idx+1).
				WithField("qualityScore", signal.qualityScore).WithField("weightedProbability", signal.signal.Meta.Probability/2).
				WithField("totalRankingScore", signal.qualityScore+signal.signal.Meta.Probability/2).Trace("Ranked signal..")
		}
	}

	//7. Filter out duplicates
	filteredSignals := make([]*model.AutochartistSignal, 0)
	seen := make(map[string]struct{})
	for _, sig := range rankedSignals {
		if _, ok := seen[sig.signal.InstrumentName]; !ok {
			filteredSignals = append(filteredSignals, sig.signal)
			seen[sig.signal.InstrumentName] = struct{}{}
		}
	}

	//8. Create trade actions using broker/signal data
	actions = append(actions, b.processOpenTradeSignals(filteredSignals, openTrades, &http.Client{})...)

	return actions
}

func (b *Basic) closeOpenTradesBelowEarningsThreshold(openTrades map[string]*model.SignalTrade, client interfaces.HTTPClient) []strategy.Action {
	//min percent of original earnings before trade closeout
	earningsThreshold := env.GetConfig().StrategyTradeEarningsThreshold

	actions := make([]strategy.Action, 0)
	for _, trade := range openTrades {
		//earnings per pip for trade instrument based on units
		pipEarnings, err := util.GetPipGain(int(math.Abs(float64(trade.Units))), trade.Instrument, &http.Client{})
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not calculate pip gain for earnings, inst: %s", trade.Instrument.Name())
			continue
		}

		//get trade instrument display precision
		resInst, err := gofxhttp.GetInstruments([]instruments.Instrument{trade.Instrument}, client)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not get instrument %s", trade.Instrument.Name())
			continue
		}

		//calculate original trade potential hourly earnings
		originalTradePotentialEarnings := pipEarnings * (math.Abs(*trade.TakePrice-trade.EntryPrice) / math.Pow10(resInst.Instruments[0].PipLocation))
		originalTradePotentialHourlyEarnings := originalTradePotentialEarnings / trade.Deadline.Sub(trade.EntryTime).Hours()

		// -- calculate remaining trade potential hourly earnings --

		//get current market pricing
		resPricing, err := gofxhttp.GetPricing([]instruments.Instrument{trade.Instrument}, false, nil, client)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not get instrument pricing for %s", trade.Instrument.Name())
			continue
		}
		if !resPricing.Prices[0].Tradeable {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("issue evaluating remaining earnings, instrument %s not tradeable", trade.Instrument.Name())
			continue
		}

		//get appropriate market price
		var marketPrice float64
		if trade.Units > 0 {
			marketPrice, _ = strconv.ParseFloat(resPricing.Prices[0].Bids[0].Price, 64)
		} else {
			marketPrice, _ = strconv.ParseFloat(resPricing.Prices[0].Asks[0].Price, 64)
		}

		remainingTradePotentialEarnings := pipEarnings * (math.Abs(*trade.TakePrice-marketPrice) / math.Pow10(resInst.Instruments[0].PipLocation))
		remainingTradePotentialHourlyEarnings := remainingTradePotentialEarnings / trade.Deadline.Sub(time.Now()).Hours()

		//calculate remaining trade earnings ratio to original
		remainingEarningsRatio := remainingTradePotentialHourlyEarnings / originalTradePotentialHourlyEarnings

		//log
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithField("tradeID", trade.TradeID).WithField("instrument", trade.Instrument.Name()).
			WithField("remainingEarningsRatio", fmt.Sprintf("%.6f", remainingEarningsRatio)).WithField("minThreshold", earningsThreshold).
			WithField("remainingTradePotentialHourlyEarnings", fmt.Sprintf("%.6f", remainingTradePotentialHourlyEarnings)).
			WithField("originalTradePotentialHourlyEarnings", fmt.Sprintf("%.6f", originalTradePotentialHourlyEarnings)).
			Info("ratio of remaining to original hourly trade earnings potential")

		//close trade if below threshold
		if remainingEarningsRatio < earningsThreshold {
			actions = append(actions, &strategy.CloseTradeUnits{
				TradeSpecifier: trade.TradeID,
				Units:          "ALL",
			})
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithField("tradeID", trade.TradeID).Info("closing trade below min earnings threshold")
		}
	}

	return actions
}

func (b *Basic) processOpenTradeSignals(filteredSignals []*model.AutochartistSignal, openTrades map[string]*model.SignalTrade, client interfaces.HTTPClient) []strategy.Action {
	actions := make([]strategy.Action, 0)
	var windowOpen, windowClose *time.Time
	var acctBalance *float64
	for s, t := 0, 0; t < b.maxTrades-len(openTrades) && s < len(filteredSignals); s++ {
		//get window info
		if windowOpen == nil {
			open, close, _ := util.GetCurrentTradingWindow(time.Now())
			windowOpen, windowClose = &open, &close
		}

		marketTrade := &model.SignalTrade{}
		marketTrade.Signal = filteredSignals[s]
		marketTrade.Instrument = filteredSignals[s].Instrument

		//opt out of signal if instrument already in open trade
		isTradingInst := false
		for _, openTrade := range openTrades {
			if openTrade.Instrument == marketTrade.Signal.Instrument {
				isTradingInst = true
				break
			}
		}
		if isTradingInst {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).
				Infof("opted out of %s signal %d, instrument already in open trade", marketTrade.Signal.Instrument.Name(), marketTrade.Signal.ID)
			continue
		}

		//opt out of signal if already reached max trade limit for this signal
		signalTrades, err := db.GetSignalTradesBySignalID(marketTrade.Signal.ID, nil)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not get trades for signal %d", marketTrade.Signal.ID)
			continue
		}
		if len(signalTrades) >= env.GetConfig().StrategyMaxTradesPerSignal {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).
				Infof("opted out of signal %d, max of %d trades reached", marketTrade.Signal.ID, env.GetConfig().StrategyMaxTradesPerSignal)
			continue
		}

		//opt out of signal is instrument not tradeable
		resPricing, err := gofxhttp.GetPricing([]instruments.Instrument{marketTrade.Instrument}, false, nil, client)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not get instrument pricing for %s", marketTrade.Instrument.Name())
			continue
		}
		if !resPricing.Prices[0].Tradeable {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("opted out of signal %d, instrument %s not tradeable", marketTrade.Signal.ID, marketTrade.Instrument.Name())
			continue
		}

		//get instrument display precision
		resInst, err := gofxhttp.GetInstruments([]instruments.Instrument{marketTrade.Instrument}, client)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not get instrument %s", marketTrade.Instrument.Name())
			continue
		}
		instDisplayPrecision := resInst.Instruments[0].DisplayPrecision

		//assign units based on total units available (from balance not current available units)
		availUnits, err := util.GetTotalMarginUnits(marketTrade.Instrument, client)
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get total margin units")
			continue
		}
		marketTrade.Units = int(float64(availUnits) * b.utilizationPerTrade)

		//set trade boundaries
		var take, stop, priceGain, marketPrice float64
		if marketTrade.Signal.Meta.Direction == 1 {
			marketPrice, _ = strconv.ParseFloat(resPricing.Prices[0].Bids[0].Price, 64)
			if marketTrade.Signal.Data.Prediction != nil && marketTrade.Signal.Data.Prediction.TimeToUnix > 0 {
				//chartpattern
				take = math.Floor(((*marketTrade.Signal.Data.Prediction.PriceHigh+*marketTrade.Signal.Data.Prediction.PriceLow)/2)*math.Pow10(instDisplayPrecision)) / math.Pow10(instDisplayPrecision)
				marketTrade.TakePrice = &take
				marketTrade.Deadline = marketTrade.Signal.Data.Prediction.TimeTo
			} else {
				//keylevel
				take = marketTrade.Signal.Data.Price
				marketTrade.TakePrice = &take
				deadline := time.Now().Add(marketTrade.Signal.Data.PatternEndTime.Sub(marketTrade.Signal.Data.Points.KeyTime[1]))
				marketTrade.Deadline = &deadline
			}
			priceGain = *marketTrade.TakePrice - marketPrice

			//opt out if trade is not profitable
			if priceGain <= 0 {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).
					Infof("opted out of long signal %d, not profitable :: market price: %v, target price: %v", marketTrade.Signal.ID, marketPrice, *marketTrade.TakePrice)
				continue
			}

			stop = math.Floor((marketPrice-(take-marketPrice))*math.Pow10(instDisplayPrecision)) / math.Pow10(instDisplayPrecision)
			marketTrade.StopPrice = &stop
		} else {
			//flip units to negative
			marketTrade.Units = -marketTrade.Units

			marketPrice, _ = strconv.ParseFloat(resPricing.Prices[0].Asks[0].Price, 64)
			if marketTrade.Signal.Data.Prediction != nil && marketTrade.Signal.Data.Prediction.TimeToUnix > 0 {
				//chartpattern
				take = math.Floor(((*marketTrade.Signal.Data.Prediction.PriceHigh+*marketTrade.Signal.Data.Prediction.PriceLow)/2)*math.Pow10(instDisplayPrecision)) / math.Pow10(instDisplayPrecision)
				marketTrade.TakePrice = &take
				marketTrade.Deadline = marketTrade.Signal.Data.Prediction.TimeTo
			} else {
				//keylevel
				take = marketTrade.Signal.Data.Price
				marketTrade.TakePrice = &take
				deadline := time.Now().Add(marketTrade.Signal.Data.PatternEndTime.Sub(marketTrade.Signal.Data.Points.KeyTime[1]))
				marketTrade.Deadline = &deadline
			}
			priceGain = marketPrice - *marketTrade.TakePrice

			//opt out if trade is not profitable
			if priceGain <= 0 {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).
					Infof("opted out of short signal %d, not profitable :: market price: %v, target price: %v", marketTrade.Signal.ID, marketPrice, *marketTrade.TakePrice)
				continue
			}

			stop = math.Floor((marketPrice+(marketPrice-take))*math.Pow10(instDisplayPrecision)) / math.Pow10(instDisplayPrecision)
			marketTrade.StopPrice = &stop
		}

		// -- deadline analysis --

		//opt out of this signal if the deadline is after start of next trading window (prevents holding trades over weekend)
		if !marketTrade.Deadline.Before(windowOpen.AddDate(0, 0, 7)) {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("opted out of signal %d, deadline too late: %s", marketTrade.Signal.ID, marketTrade.Deadline.Format(time.RFC1123))
			continue
		}

		//trim deadline if occurs outside of trading window
		if windowClose.Before(*marketTrade.Deadline) {
			trimmedDeadline := windowClose
			marketTrade.Deadline = trimmedDeadline
		}

		//opt out if trade duration below min hours
		if marketTrade.Deadline.Sub(time.Now()).Hours() < float64(env.GetConfig().StrategyMinTradeDurationHours) {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("opted out of signal %d, trade duration below min of %d hours; deadline: %s",
				marketTrade.Signal.ID, env.GetConfig().StrategyMinTradeDurationHours, marketTrade.Deadline.Format(time.RFC1123))
			continue
		}

		//opt out if trade duration above max hours
		if marketTrade.Deadline.Sub(time.Now()).Hours() > float64(env.GetConfig().StrategyMaxTradeDurationHours) {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("opted out of signal %d, trade duration above max of %d hours; deadline: %s",
				marketTrade.Signal.ID, env.GetConfig().StrategyMaxTradeDurationHours, marketTrade.Deadline.Format(time.RFC1123))
			continue
		}

		// -- opt out if below earnings goal --

		//calculate potential trade earnings
		pipEarnings, err := util.GetPipGain(int(math.Abs(float64(marketTrade.Units))), marketTrade.Instrument, &http.Client{})
		if err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not calculate pip gain for earnings, inst: %s", marketTrade.Instrument.Name())
			continue
		}
		potentialTradeEarnings := pipEarnings * (math.Abs(*marketTrade.TakePrice-marketPrice) / math.Pow10(resInst.Instruments[0].PipLocation))

		// -- calculate hourly trade earnings goal based on window duration ang opening balance --

		//get available margin from account -- retrieve from broker only if not yet retrieved this interval
		if acctBalance == nil {
			if acct, err := gofxhttp.GetAccount(&http.Client{}); err == nil {
				bal, _ := strconv.ParseFloat(acct.Balance, 64)
				acctBalance = &bal
			} else {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("error getting available margin from account for earnings goal calculation")
			}
		}

		//hourly earnings calculation
		hourlyWindowEarningsGoal := (*acctBalance * env.GetConfig().StrategyWindowEarningsGoal) / windowClose.Sub(*windowOpen).Hours()
		if potentialHourlyEarnings := potentialTradeEarnings / marketTrade.Deadline.Sub(time.Now()).Hours(); potentialHourlyEarnings < hourlyWindowEarningsGoal {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).
				Infof("opted out of signal %d, potential hourly earnings: %.3f less than goal of %.3f", marketTrade.Signal.ID, potentialHourlyEarnings, hourlyWindowEarningsGoal)
			continue
		}

		//add action
		actions = append(actions, &strategy.CreateMarketTrade{
			Trade: marketTrade,
		})
		t++
	}

	//complete
	return actions
}

//SignalReqParams returns signal request params, each represents a separate request made at each interval
func (b *Basic) SignalReqParams() []url.Values {
	params := make([]url.Values, 0)
	for _, inst := range b.Instruments {
		vals := url.Values{}
		vals.Set("instrument", inst.Name())
		params = append(params, vals)
	}
	return params
}

//SignalsByRanking implements sort methods to sort signals by ranking
type SignalsByRanking []*struct {
	signal       *model.AutochartistSignal
	qualityScore float64
}

//Len from sort interface
func (s SignalsByRanking) Len() int {
	return len(s)
}

//Less from sort interface
func (s SignalsByRanking) Less(i, j int) bool {
	if s[i].signal.Type == "chartpattern" && s[j].signal.Type != "chartpattern" {
		return true
	}
	if s[j].signal.Type == "chartpattern" && s[i].signal.Type != "chartpattern" {
		return false
	}

	return s.ranking(i) > s.ranking(j)
}

//Swap from sort interface
func (s SignalsByRanking) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

//calculate signal ranking (max 100)
func (s SignalsByRanking) ranking(idx int) float64 {
	var qualityScore float64
	if s[idx].signal.Meta.Scores != nil {
		qualityScore = getWeightedMeanQualityScore(*s[idx].signal.Meta.Scores)
	}
	return qualityScore + s[idx].signal.Meta.Probability/2
}

//weighted mean quality score (max 50)
func getWeightedMeanQualityScore(scores model.MetaScores) float64 {
	//calculate unweighted mean of provided score(s)
	var scoreTotal float64
	var scoreCount int

	if scores.Breakout != nil {
		scoreCount++
		scoreTotal += float64(*scores.Breakout)
	}
	if scores.Clarity != nil {
		scoreCount++
		scoreTotal += float64(*scores.Clarity)
	}
	if scores.InitialTrend != nil {
		scoreCount++
		scoreTotal += float64(*scores.InitialTrend)
	}
	if scores.Quality != nil {
		scoreCount++
		scoreTotal += float64(*scores.Quality)
	}
	if scores.Uniformity != nil {
		scoreCount++
		scoreTotal += float64(*scores.Uniformity)
	}

	//return score of zero if no scores provided
	if scoreCount == 0 {
		return 0
	}

	providedScoreMean := scoreTotal / float64(scoreCount)

	//copy slice to hold remaining values
	missingScores := make([]float64, 5-scoreCount)

	//add lesser weighted values to scoreTotal for missing scores
	for i, slot := scoreCount, 1; i < 5; i, slot = i+1, slot+1 {
		//if providedScoreMean > 1, use descending values
		if providedScoreMean > 1 {
			//for first empty slot subtract from initial mean, else use previous slot
			if slot == 1 {
				//add score
				missingScores[slot-1] = providedScoreMean - float64(slot)
			} else {
				//add score
				missingScores[slot-1] = missingScores[slot-2] - float64(slot)
			}

			//set score to minimum of 1
			if missingScores[slot-1] < 1 {
				missingScores[slot-1] = 1
			}
		} else {
			//if providedScoreMean is 1, make remaining scores zero
			missingScores[slot-1] = 0
		}

		scoreTotal += missingScores[slot-1]
	}

	return scoreTotal
}
