package basic_test

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"

	"gofx/model"
	"gofx/model/oanda"
	// "gofx/strategy"
	"gofx/strategy/basic"
)

var weightedMeanQualityScoreTestCases = []struct {
	scores   model.MetaScores
	expected float64
}{
	{
		scores:   model.MetaScores{Breakout: toPtr(10)},
		expected: 31.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(10), Clarity: toPtr(10), InitialTrend: toPtr(10), Quality: toPtr(10), Uniformity: toPtr(10)},
		expected: 50.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(10), Clarity: toPtr(10), InitialTrend: toPtr(10), Quality: toPtr(10)},
		expected: 49.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(10), Clarity: toPtr(10), InitialTrend: toPtr(10), Quality: toPtr(10), Uniformity: toPtr(9)},
		expected: 49.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(10), Clarity: toPtr(10), InitialTrend: toPtr(10), Quality: toPtr(10), Uniformity: toPtr(8)},
		expected: 48.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(8), Clarity: toPtr(8)},
		expected: 30.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(7), Clarity: toPtr(7), InitialTrend: toPtr(7)},
		expected: 31.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(6), Clarity: toPtr(7), InitialTrend: toPtr(8)},
		expected: 31.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(6), Clarity: toPtr(6), InitialTrend: toPtr(6), Quality: toPtr(6), Uniformity: toPtr(6)},
		expected: 30.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(10), Clarity: toPtr(10)},
		expected: 40.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(8), Clarity: toPtr(9)},
		expected: 32.5,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(1)},
		expected: 1.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(3)},
		expected: 8.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(1), Clarity: toPtr(1)},
		expected: 2.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(1), Clarity: toPtr(1), InitialTrend: toPtr(1), Quality: toPtr(1), Uniformity: toPtr(1)},
		expected: 5.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(2)},
		expected: 6.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(2), Clarity: toPtr(2)},
		expected: 7.,
	},
	{
		scores:   model.MetaScores{Breakout: toPtr(6), Clarity: toPtr(10), InitialTrend: toPtr(3), Quality: toPtr(6), Uniformity: toPtr(7)},
		expected: 32.,
	},
	{
		scores:   model.MetaScores{},
		expected: 0.,
	},
}

func TestGetWeightedMeanQualityScore(t *testing.T) {
	for idx, testCase := range weightedMeanQualityScoreTestCases {
		actualValue, expectedValue := basic.GetWeightedMeanQualityScore(testCase.scores), testCase.expected
		if actualValue != expectedValue {
			t.Errorf("TestGetWeightedMeanQualityScore(%d): expected %.2f, actual %.2f", idx, expectedValue, actualValue)
		}
	}
}

func toPtr(val int) *int {
	return &val
}

type testProcessSignalClient struct {
}

func (tc *testProcessSignalClient) Do(req *http.Request) (*http.Response, error) {
	res := &http.Response{}
	switch {
	case strings.Contains(req.URL.String(), "/pricing"):
		resWriter := strings.NewReader(`{
			"time": "2020-05-14T03:05:38.730982830Z",
			"prices": [
				{
					"type": "PRICE",
					"time": "2020-05-14T03:05:21.930363759Z",
					"bids": [
						{
							"price": "0.97194",
							"liquidity": 10000000
						}
					],
					"asks": [
						{
							"price": "0.97194",
							"liquidity": 10000000
						}
					],
					"closeoutBid": "0.97194",
					"closeoutAsk": "0.97194",
					"status": "tradeable",
					"tradeable": true,
					"unitsAvailable": {
						"default": {
							"long": "2261199",
							"short": "2261199"
						},
						"openOnly": {
							"long": "2261199",
							"short": "2261199"
						},
						"reduceFirst": {
							"long": "2261199",
							"short": "2261199"
						},
						"reduceOnly": {
							"long": "0",
							"short": "0"
						}
					},
					"instrument": "USD_CHF"
				}
			],
			"homeConversions": [
				{
					"currency": "USD",
					"accountGain": "1",
					"accountLoss": "1",
					"positionValue": "1"
				},
				{
					"currency": "CHF",
					"accountGain": "1.08124",
					"accountLoss": "1.08139",
					"positionValue": "1.08132"
				}
			]
		}`)
		res.Body = ioutil.NopCloser(resWriter)
	}
	return res, nil
}

func TestProcessSignalsOpenTrade(t *testing.T) {
	testBrokerSignalData := `{
        "id": 36758177,
        "type": "keylevel",
        "instrument": "USD_CHF",
        "data": {
            "prediction": {
                "pricelow": 0.962163,
                "pricehigh": 0.967758,
                "timefrom": 1589544000,
                "timeto": 0,
                "timebars": 555
            },
            "points": {
                "keytime": {
                    "1": 1589490000,
                    "2": 1589443200,
                    "3": 1589241600,
                    "4": 1588870800,
                    "5": 1588770000,
                    "6": 1588744800,
                    "7": 0,
                    "8": 0,
                    "9": 0,
                    "10": 0
                }
            },
            "patternendtime": 1589547600,
            "price": 0.97224
        },
        "meta": {
            "scores": {
                "quality": 6
            },
            "patterntype": "Breakout",
            "pattern": "Support",
            "probability": 74.07,
            "interval": 60,
            "direction": -1,
            "length": 174,
            "historicalstats": {
                "symbol": {
                    "correct": 227,
                    "percent": 72.06,
                    "total": 315
                },
                "pattern": {
                    "correct": 7320,
                    "percent": 74.1,
                    "total": 9879
                },
                "hourofday": {
                    "correct": 802,
                    "percent": 75.59,
                    "total": 1061
                }
            },
            "completed": 1
        }
	}`
	testBrokerSignal := &oanda.AutochartistSignal{}
	if err := json.Unmarshal([]byte(testBrokerSignalData), testBrokerSignal); err != nil {
		panic(err)
	}

	model.NewAutochartistSignal(testBrokerSignal)
	// testSignal := model.NewAutochartistSignal(testBrokerSignal)
	// signals := []*model.AutochartistSignal{
	// 	testSignal,
	// }

	// strategyAction := basic.ProcessOpenTradeSignals(2, 0, 0.2, signals, &http.Client{})[0]
	// createTradeAction, typeOK := strategyAction.(*strategy.CreateMarketTrade)
	// if !typeOK {
	// 	t.Error("TestProcessSignalsOpenTrade: expected action type *strategy.CreateMarketTrade")
	// }

	// if createTradeAction.Trade.TakePrice == nil {
	// 	t.Errorf("TestProcessSignalsOpenTrade: expectedTake price %.5f, actual nil", testSignal.Data.Price)
	// } else {
	// 	if *createTradeAction.Trade.TakePrice != testSignal.Data.Price {
	// 		t.Errorf("TestProcessSignalsOpenTrade: expectedTake price %.5f, actual %.5f", testSignal.Data.Price, *createTradeAction.Trade.TakePrice)
	// 	}
	// }
}
