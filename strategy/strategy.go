package strategy

import (
	"net/url"
	"time"

	"gofx/model"
	"gofx/model/oanda"
)

//Strategy is the main interface that all strategies must implement
type Strategy interface {
	Init()
	Name() string
	SetName(string) bool
	Title() string
	Interval() time.Duration
	Run([]*oanda.AutochartistSignal) []Action
	SignalReqParams() []url.Values
}

//Action is the interface for action commands returned to client for execution
type Action interface {
	Description() string
}

//CreateMarketTrade is an Action to create a trade at market price
type CreateMarketTrade struct {
	Trade *model.SignalTrade
}

//Description returns a description of this action
func (c *CreateMarketTrade) Description() string {
	return "Create trade from market order"
}

//CreateUpdateDeleteTradeOrders is an Action to modify trade orders
type CreateUpdateDeleteTradeOrders struct {
	Trade *model.SignalTrade
}

//Description returns a description of this action
func (c *CreateUpdateDeleteTradeOrders) Description() string {
	return "Create/update/delete trade order(s)"
}

//CloseTradeUnits is an Action to close units of an active trade
type CloseTradeUnits struct {
	TradeSpecifier string
	Units          string //can be int or ALL
}

//Description returns a description of this action
func (c *CloseTradeUnits) Description() string {
	return "Close trade units"
}

//CreateLimitOrder is an Action to create a limit order
type CreateLimitOrder struct {
	Order *model.SignalOrder
}

//Description returns a description of this action
func (c *CreateLimitOrder) Description() string {
	return "Create limit order"
}

//CreateStopOrder is an Action to create a stop order
type CreateStopOrder struct {
	Order *model.SignalOrder
}

//Description returns a description of this action
func (c *CreateStopOrder) Description() string {
	return "Create stop order"
}
